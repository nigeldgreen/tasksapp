<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Catamaran|Open+Sans:300,400" rel="stylesheet">
    <style>
        h1, h2, h3, h4 {
            font-family: "Catamaran", sans-serif;
        }
        p, a, ul, li {
            font-family: "Open sans", sans-serif;
        }
        p {
            padding-bottom: 9px;
        }
        li {
            padding-bottom: 3px;
        }
    </style>
</head>
<body>
    <h1>Hello {{ $firstname }}</h1>
    <p>Welcome to tasksapp!</p>

    <p>Thanks for registering, please click this link to <a href="{{ $url }}">validate your account</a> and get started!</p>

    <p>Here's three things to help you get started:</p>

    <ul>
        <li>Start adding actions by clicking the '+' icon on the inbox.</li>
        <li>Create some projects and clients to start organising, and try the agenda view to see all your flagged and due actions.</li>
        <li>The help pages should cover most things, but drop me a line if you get stuck and I'll help you get going.</li>
    </ul>

    <p>Hope you enjoy using tasksapp - I'm very keen to know how you like it, so why not hit me up a message and tell me what you like and what I can do better?</p>

    <p>Cheers,<br />
        Nigel</p>
</body>
</html>
