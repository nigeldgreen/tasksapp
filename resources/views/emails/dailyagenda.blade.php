<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Catamaran|Open+Sans:300,400" rel="stylesheet">
    <style>
        h1, h2, h3, h4 {
            font-family: "Catamaran", sans-serif;
        }
        p, a, ul, li {
            font-family: "Open sans", sans-serif;
        }
        p {
            padding-bottom: 9px;
        }
        li {
            padding-bottom: 3px;
        }
    </style>
</head>
<body>
<p>Hello {{ $user->getFirstName() }}, you have important stuff in your tasksapp!</p>
<p>The actions below are due or overdue:</p>
<ul>
    @foreach($due as $action)<li>{{ $action->title }} (due on {{ $action->due }})</li>@endforeach
</ul>

<p>The actions below are flagged for completion:</p>
<ul>
    @foreach($flagged as $action)<li>{{ $action->title }}</li>@endforeach
</ul>

<p>Click the link to <a href="{{ config('app.url') }}/remotelogin?daily={{ $user->api_token }}">log in</a> and get tasking!</p>

<p>Cheers,<br />
    Nigel</p>
</body>
</html>