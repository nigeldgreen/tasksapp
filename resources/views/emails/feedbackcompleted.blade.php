<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Catamaran|Open+Sans:300,400" rel="stylesheet">
    <style>
        h1, h2, h3, h4 {
            font-family: "Catamaran", sans-serif;
        }
        p, a, ul, li {
            font-family: "Open sans", sans-serif;
        }
        p {
            padding-bottom: 9px;
        }
        li {
            padding-bottom: 3px;
        }
    </style>
</head>
<body>
    <h1>Someone has fed back on tasksapp!</h1>

    <p>{{ $feedback->user->name }} said:</p>

    <p>{{ $feedback->message }}</p>

</body>
</html>