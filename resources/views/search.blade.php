@extends('templates/main')

@section('content')
    <div class="client-container">
        <div class="client-row">
            <span class="client-title">Search results for '{{ $searchterm }}'</span>
        </div>

        <div class="project-container">
            <div class="project-row">
                <span class="project-title">Live actions</span>
            </div>
            <div class="action-container">
                @if($liveactions->count() == 0)
                    There are no live actions containing "{{ $searchterm }}"
                @else
                    @foreach($liveactions as $action)
                        @include('partials.agendaAndSearchView')
                    @endforeach
                @endif
            </div>
        </div>

        <div class="project-container">
            <div class="project-row">
                <span class="project-title">Completed actions</span>
            </div>
            <div class="action-container">
                @if($liveactions->count() == 0)
                    There are no completed actions containing "{{ $searchterm }}"
                @else
                    @foreach($completedactions as $action)
                        @include('partials.agendaAndSearchView')
                    @endforeach
                @endif
            </div>
        </div>
    </div>

@endsection
