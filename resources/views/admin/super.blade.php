@extends('templates/main')

@section('content')
<h2>Settings</h2>
<p>
    Show debug info: <input id="debug_view" type="checkbox" class="form-control" name="debug_view" @if(auth()->user()->debug_view)checked @endif>
</p>
<p>
    <button id="reindex">Reindex all</button>
</p>

<h2>User stats</h2>
<table>
    <tr>
        <th>User (ID)</th>
        <th>Registered on</th>
        <th>Last logged in</th>
        <th>Total logins</th>
        <th>Last active</th>
        <th>Clients</th>
        <th>Projects</th>
        <th>Actions</th>
    </tr>
    @foreach ($users as $user)
    <tr>
        <td>{{ $user->name }}</td>
        <td>{{ $user->created_at }}</td>
        <td>{{ $user->lastLoggedInOn }}</td>
        <td>{{ $user->totalLogins }}</td>
        <td>{{ $user->lastActive }}</td>
        <td>{{ $user->clients->count() }}</td>
        <td>{{ $user->projects->count() }}</td>
        <td>{{ $user->actions->count() }}</td>
    </tr>
    @endforeach
</table>
<h2>User feedback</h2>
<table>
    <tr>
        <th>User</th>
        <th>Email</th>
        <th>Message</th>
        <th>Left on</th>
    </tr>
    @foreach($feedback as $message)
        <tr>
            <td>{{ $message->user->name }}</td>
            <td>{{ $message->user->email }}</td>
            <td>{{ $message->message }}</td>
            <td>{{ $message->created_at }}</td>
        </tr>
    @endforeach
</table>

@endsection
