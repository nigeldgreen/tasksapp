@extends('templates/main')

@section('content')

<h2>My profile</h2>
<p>
    <span class="fixed-width-title">Name:</span>{{ auth()->user()->name }}
</p>
<p>
    <span class="fixed-width-title">Email:</span>{{ auth()->user()->email }}
</p>
<p>
    <span class="fixed-width-title">Password:</span>Click the button below to request a password reset
    <form role="form" action="{{ route('password.email') }}" method="POST" class="admin-reset-password">
        {{ csrf_field() }}
        <input id="email" type="hidden" class="form-control" name="email" value="{{ auth()->user()->email }}" required />
        <div class="form-group">
            <label for="form-button"></label>
            <button type="submit" class="form-button">Send Password Reset Link</button>
        </div>
    </form>
</p>


<hr>
<h2>Communication preferences</h2>
<p>Tick the box below if you would like to receive a daily email when you have flagged or due tasks:</p>
<form action="{{ route('admin.agenda.optin') }}" method="POST" class="admin-agenda-opt-in">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="agenda_opt_in">Opt-in?</label>
        <input id="agenda_opt_in" type="checkbox" class="form-control" name="agenda_opt_in" @if(auth()->user()->agenda_opt_in) checked @endif>
    </div>
    <div class="form-group">
        <label for="agenda_opt_in"></label>
        <button type="submit" class="form-button">Update preference</button>
    </div>
</form>

<hr>

<h2>My stats</h2>
<table>
    <tr>
        <th colspan=2>Last 7 days</th>
    </tr>
    <tr>
        <td>Actions added</td><td class="numbers">{{ $this_week['actions_added'] }}</td>
    </tr>
    <tr>
        <td>Actions completed</td><td class="numbers">{{ $this_week['actions_deleted'] }}</td>
    </tr>

    <tr>
        <td>Projects added</td><td class="numbers">{{ $this_week['projects_added'] }}</td>
    </tr>
    <tr>
        <td>Projects completed</td><td class="numbers">{{ $this_week['projects_deleted'] }}</td>
    </tr>

    <tr>
        <td>Clients added</td><td class="numbers">{{ $this_week['clients_added'] }}</td>
    </tr>
    <tr>
        <td>Clients removed</td><td class="numbers">{{ $this_week['clients_deleted'] }}</td>
    </tr>
</table>

<table>
    <tr>
        <th colspan=2>All time</th>
    </tr>
    <tr>
        <td>Actions added</td><td class="numbers">{{ $all_time['actions_added'] }}</td>
    </tr>
    <tr>
        <td>Actions completed</td><td class="numbers">{{ $all_time['actions_deleted'] }}</td>
    </tr>

    <tr>
        <td>Projects added</td><td class="numbers">{{ $all_time['projects_added'] }}</td>
    </tr>
    <tr>
        <td>Projects completed</td><td class="numbers">{{ $all_time['projects_deleted'] }}</td>
    </tr>

    <tr>
        <td>Clients added</td><td class="numbers">{{ $all_time['clients_added'] }}</td>
    </tr>
    <tr>
        <td>Clients removed</td><td class="numbers">{{ $all_time['clients_deleted'] }}</td>
    </tr>
</table>

<hr>

<h2>Manage clients</h2>

{!! Form::open(['route' => ['admin.client.save'], 'method' => 'post', 'class' => 'admin-order-clients']) !!}

@foreach ($clients as $client)

<div class="form-group">
    <input class="form-control" name="client_{{ $client->id }}" id="client_{{ $client->id }}" value="{{ $client->order }}" type="text">
    <label for="client_{{ $client->id }}">{{ $client->title }}</label>
</div>

@endforeach

<div class="form-group">
    <input class="form-button" value="Re-order clients" type="submit">
</div>



@endsection
