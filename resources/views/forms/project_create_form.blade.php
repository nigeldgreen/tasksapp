{!! Form::open(['route' => 'project.store', 'id' => 'project-form']) !!}

<a href="#" class="close-dynamic-form visible-link">close</a>
<h3>Create new project</h3>
<!-- Project title -->
<div class="form-group">
    {!! Form::label('title', 'Title') !!}
    {!! Form::text('title', null, ['id' => 'project-form-title', 'class' => 'form-control', 'autofocus']) !!}
</div>

<!-- Project notes -->
<div class="form-group">
    {!! Form::label('notes', 'Notes') !!}
    {!! Form::textarea('notes', null, ['id' => 'project-form-notes', 'class' => 'form-control', 'size' => '50x8']) !!}
</div>

<!-- Related client -->
<div class="form-group">
    <label for="client_id">Client</label>
    <select class="form-control" id="project-form-client_id" name="client_id">
        <option value=0>
            -- no client --
        </option>
        @foreach($active_clients as $client)
            <option value="{{ $client->id }}" @if($client->id == $client_id)selected @endif>{{ $client->title }}</option>
        @endforeach
    </select>
</div>

<!-- Add Client Button -->
<div class="form-group">
    {!! Form::label('submit_button',' ') !!}
    {!! Form::submit('Store project', ['class' => 'form-button', 'id' => 'project-form-button']) !!}
</div>

<div class="form-group">
    <label for="addAnotherProject">Add another</label>
    <input type="checkbox" id="project-form-addanother" name="addAnotherProject" class="create-checkbox add-another">
</div>

{!! Form::close() !!}
