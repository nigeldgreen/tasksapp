{!! Form::model($client, ['route' => ['client.update', $client], 'method' => 'patch', 'id' => 'client-detail-form']) !!}

<a href="#" class="close-dynamic-form visible-link">close</a>
<h3>Update client</h3>

<!-- Client id -->
{{ Form::hidden('id', null, ['id' => 'client-form-id']) }}

<!-- Client title -->
<div class="form-group">
    {!! Form::label('title', 'Title') !!}
    {!! Form::text('title', null, ['id' => 'client-form-title', 'class' => 'form-control', 'autofocus', 'required']) !!}
</div>

<!-- Client notes -->
<div class="form-group">
    {!! Form::label('notes', 'Notes') !!}
    {!! Form::textarea('notes', null, ['id' => 'client-form-notes', 'class' => 'form-control']) !!}
</div>

<!-- Add Client Button -->
<div class="form-group">
    {!! Form::label('submit_button',' ') !!}
    {!! Form::submit('Update client', ['class' => 'form-button', 'id' => 'client-form-button']) !!}
</div>

{!! Form::close() !!}