@extends('templates/main')
@section('content')
<h1>Login</h1>
<form class="login main-page-form" method="POST" action="{{ route('login') }}">
    @csrf

    <div class="form-group">
        <label for="email">E-Mail</label>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
    </div>

    <div class="form-group">
        <label for="password">Password</label>
            <input id="password" type="password" class="form-control" name="password" required>
    </div>

    <div class="form-group">
        <label for="remember"></label>
        <input type="checkbox" name="remember" id="remember" class="checkbox"><span>Remember Me</span>
    </div>

    <div class="form-group">
        <label for="submit"></label>
        <button type="submit" class="form-button">Login</button>
    </div>
</form>
@endsection
