@extends('templates/main')
@section('content')
    <h1>Account not validated</h1>
    <p>Please check your email and follow the link to validate your account.</p>
@endsection
