@extends('templates/main')
@section('content')
<h1>Register</h1>
    <p>It's free to register for a tasksapp account. Just fill in the details in the form below and we'll
    create an account for you and log you straight in.</p>
    <form method="POST" action="{{ url('/register') }}" class="register main-page-form">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="name">Name</label>
            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <input id="password" type="password" class="form-control" name="password" required>
        </div>

        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <label for="password-confirm">Confirm Password</label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
        </div>

{{--        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">--}}
{{--            <label for="h-captcha">Confirm humanity</label>--}}
{{--            <span id="h-captcha" class="h-captcha" data-sitekey="e743f7db-3646-4699-a270-731dead18cb4"></span>--}}
{{--            <script src="https://hcaptcha.com/1/api.js" async defer></script>--}}
{{--        </div>--}}

        <div class="form-group">
            <label for="register-form-submit"></label>
            <button type="submit" class="form-button" id="register-form-submit">
                Register
            </button>
        </div>
    </form>
@endsection