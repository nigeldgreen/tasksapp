@extends('templates/main')

<!-- Main Content -->
@section('content')
<h1>Reset Password</h1>
<p>Enter your email address below and we'll send you a mail to let you know how to reset your password.</p>
<form role="form" method="POST" action="{{ url('/password/email') }}" class="main-page-form">
    {{ csrf_field() }}

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email" class="col-md-4 control-label">Email</label>
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>

    <div class="form-group">
        <label for="register-form-submit" class="col-md-4 control-label"></label>
        <button type="submit" class="form-button" id="register-form-submit">
            Register
        </button>
    </div>
</form>
@endsection
