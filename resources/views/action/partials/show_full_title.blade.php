<span class="action-title-text">
    {{ $action->title }}
</span>

@isset ($action->url)
<a class="icon-action-has-link" href="{{ $action->url }}" target="_blank" title="Open the URL for the action">
    <svg viewBox="0 0 8 8" class="icon">
        <use xlink:href="/images/open-iconic.svg#link-intact"></use>
    </svg>
</a>
@endisset

@if (isset($action->notes) && $action->notes != "")
<a class="icon-action-has-notes" href="{{ $action->url }}" target="_blank" title="Show the notes panel for the action">
    <svg viewBox="0 0 8 8" class="icon">
        <use xlink:href="/images/open-iconic.svg#copywriting"></use>
    </svg>
</a>
@endif