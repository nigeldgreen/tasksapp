<div class="action-row action-id-{{ $action->id }}" action_id="{{ $action->id }}">
    <div class="action-icons">
        <a class="icon-complete-action" href="{{ route('completedaction.store', $action->id) }}" title="Mark action as complete">
            <svg viewBox="0 0 8 8" class="icon">
                <use xlink:href="/images/open-iconic.svg#check"></use>
            </svg>
        </a>
        <a class="icon-edit-action" href="{{ route('action.edit', $action->id }}" title="Edit this action">
            <svg viewBox="0 0 8 8" class="icon icon-edit-action">
                <use xlink:href="/images/open-iconic.svg#pencil"></use>
            </svg>
        </a>
    </div>
    <div class="action-title" action_id="50">
        <span class="action-title-text">{{ $action->title }}</span>
    </div>
    <div class="action-flags">
        <span class="date-flag date-due">{{ $action->due or "--" }}</span>
        <img class="action-priority-$action->priority" action_id="$action->id" src="/images/s.png" title="Change the priority for this action" />
    </div>
</div><!-- action-row -->