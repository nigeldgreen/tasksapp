<!DOCTYPE html public '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @if(auth()->user())
    <meta name="_utoken" content="{{ auth()->user()->api_token }}">
    <meta name="_token" content="{{ csrf_token() }}">
    @endif
    <title>{{ config('app.name') }}</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Fira+Mono&family=Raleway&display=swap" rel="stylesheet">
    <link rel='stylesheet' type='text/css' href='{{ mix('/css/app.css') }}' />
    <script src="{{ mix('/js/app.js') }}" type="text/javascript" ></script>
</head>
<body>
    <div id="pageCover"></div>
    <div id="form-display"></div>
    @include('partials.all-actions-block')
    <div id="mainpage"@if (auth()->guest())>@else user_id="{{ auth()->user()->id }}">@endif

    <div class="main-content">
        <div class="container">
            <div class="left-column">
                <span class='logo'>
                    <a class="tasksapp-logo" href="{{ config('app.url') }}">tasksapp</a>
                </span>
                @if(auth()->guest())
                    @include('partials.leftnav-loggedout')
                @else
                    @include('partials.leftnav')
                @endif
            </div> <!-- left-column -->
            <div  @if(Route::currentRouteName() != 'note.index') class="main-column" @else class="main-column main-column-notes" @endif>
                @if (count($errors))
                    <div class="message-error">
                        @foreach ($errors->all() as $error)
                                {{ $error }}<br />
                        @endforeach
                    </div>
                @endif
                @if (session('status'))
                    <div class="message-status">{{ session('status') }}</div>
                @endif
                @yield('content')
            </div> <!-- main-column -->
            @if(Route::currentRouteName() == 'note.index')
                <div class="right-column right-column-notes">
                    <div class="right-column-content"></div>
                </div>
            @endif
        </div> <!-- container -->
    </div> <!-- main-content -->
</div> <!-- #mainpage -->

</body>
</html>
