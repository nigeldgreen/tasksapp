@extends('templates/main')

@section('content')
<div id="project-detail">
    <div class="project-row">
        <span class="project-title is-open">{{ $project->title }}</span>
        <span class="project-icons">
            <a class="icon-add-action-to-project" href="{{ route('action.create', $project->id) }}" title="Create an action for this project">
                <svg viewBox="0 0 8 8" class="icon">
                    <use xlink:href="/images/open-iconic.svg#plus"></use>
                </svg>
            </a>
            <a class="icon-edit-project" href="{{ route('project.edit', $project->id) }}" title="Edit this project">
                <svg viewBox="0 0 8 8" class="icon">
                    <use xlink:href="/images/open-iconic.svg#pencil"></use>
                </svg>
            </a>
            <a class="icon-complete-project" href="{{ route('project.destroy', $project->id) }}" title="Mark project as complete">
                <svg viewBox="0 0 8 8" class="icon">
                    <use xlink:href="/images/open-iconic.svg#check"></use>
                </svg>
            </a>
        </span>
    </div>

    <h3>Notes</h3>
    <p>{{ $project->notes }}</p>

    <h3>Actions</h3>
    <div class="action-container action-container-client-{{ $project->client_id or 0 }}">
        @foreach ($project->actions()->orderBy('project_order','asc')->get() as $action)
            @include('partials.show_action')
        @endforeach
    </div>
</div>

@endsection
