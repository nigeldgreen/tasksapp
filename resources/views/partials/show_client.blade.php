<div class="client-container" client_id="{{ $client->id }}">
    <div class="client-row">
        <span class="client-title">{{ $client->title }}    </span>
        <a href="" class="client-disclose is-open" title="Show or hide projects and actions for this client">
            <svg viewBox="0 0 8 8" class="icon">
                <use xlink:href="/images/open-iconic.svg#eye"></use>
            </svg>
        </a>
        <div class="client-icons">
            <a href="{{ route('project.create', $client->id) }}" title="Add a project to this client" class="icon-add-project-to-client">
                <svg viewBox="0 0 8 8" class="icon">
                    <use xlink:href="/images/open-iconic.svg#plus"></use>
                </svg>
            </a>
            <a href="{{ route('client.edit', $client->id) }}" title="Edit this client" class="icon-edit-client">
                <svg viewBox="0 0 8 8" class="icon">
                    <use xlink:href="/images/open-iconic.svg#pencil"></use>
                </svg>
            </a>
            <a href="{{ route('client.destroy', $client->id) }}" title="Mark this client as complete" class="icon-complete-client">
                <svg viewBox="0 0 8 8" class="icon">
                    <use xlink:href="/images/open-iconic.svg#check"></use>
                </svg>
            </a>
        </div>
    </div>
    <div class="action-container action-container-client-{{ $client->id or 0 }}">
        @foreach ($client->actions as $action)
            @include('partials.show_action')
        @endforeach
    </div>
    @foreach ($client->projects as $project)
        @include('partials.show_project')
    @endforeach
</div>
