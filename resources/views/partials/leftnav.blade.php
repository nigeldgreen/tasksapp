<form METHOD="POST" action="{{ route('client.switch', 0) }}" id="top-nav-client-select">
    <div class="form-group">
        <select class="form-control" id="nav-form-client_id" name="client_id">
            <option value=0> -- show all -- </option>
            @if($active_clients)
                @foreach($active_clients as $client)
                    <option value="{{ $client->id }}" @if($client->id == session('client_id')) selected @endif>{{ $client->title }}</option>
                @endforeach
            @endif
        </select>
    </div>
</form>
<ul class="nav-list">
    <div @if(Route::currentRouteName() == 'view.agenda') class="left-nav-menu-item selected" @else class="left-nav-menu-item" @endif>
        <a href="{{ route('view.agenda') }}" class="show-the-agenda">
            <li>agenda</li>
            <div class="menu-highlight"></div>
        </a>
    </div>
    <div @if(Route::currentRouteName() == 'client.index') class="left-nav-menu-item selected" @else class="left-nav-menu-item" @endif>
        <a href="{{ route('client.index') }}">
            <li>actions</li>
            <div class="menu-highlight"></div>
        </a>
    </div>
    <div @if(Route::currentRouteName() == 'view.completedactions') class="left-nav-menu-item selected" @else class="left-nav-menu-item" @endif>
        <a href="{{ route('view.completedactions') }}">
            <li>completed</li>
            <div class="menu-highlight"></div>
        </a>
    </div>
    <div @if(Route::currentRouteName() == 'note.index') class="left-nav-menu-item selected" @else class="left-nav-menu-item" @endif>
        <a href="{{ route('note.index') }}">
            <li>notes</li>
            <div class="menu-highlight"></div>
        </a>
    </div>
    <div class="left-nav-menu-item">
        <a href="" id="collapse-tree">
            <li>show/hide actions</li>
            <div class="menu-highlight"></div>
        </a>
    </div>
    <div class="left-nav-menu-item">
        <a href="" id="toggle-notes">
            <li>show/hide notes</li>
            <div class="menu-highlight"></div>
        </a>
    </div>
</ul>
<div id='search'>
    <div class="search-field">
        <form action="{{ route('search') }}" method="POST" id='search-form'>
            {{ csrf_field() }}
            <label for="search"></label>
            <input class="form-control" id="search" name="searchterm" type="text" placeholder="search">
        </form>
    </div>
</div>
<hr>
<ul class="nav-list">
    <div @if(Route::currentRouteName() == 'admin.index') class="left-nav-menu-item selected" @else class="left-nav-menu-item" @endif>
        <a href="{{ route('admin.index') }}">
            <li>Admin page</li>
            <div class="menu-highlight"></div>
        </a>
    </div>
    @can ('view-all-users', auth()->user())
        <div @if(Route::currentRouteName() == 'admin.super') class="left-nav-menu-item selected" @else class="left-nav-menu-item" @endif>
            <a href="{{ route('admin.super') }}">
                <li>Super admin page</li>
                <div class="menu-highlight"></div>
            </a>
        </div>
    @endcan
    <div @if(Route::currentRouteName() == 'help') class="left-nav-menu-item selected" @else class="left-nav-menu-item" @endif>
        <a href="/help">
            <li>Help page</li>
            <div class="menu-highlight"></div>
        </a>
    </div>
    <div class="left-nav-menu-item">
        <a href="{{ route('logout') }}">
            <li>Log out</li>
            <div class="menu-highlight"></div>
        </a>
    </div>
</ul>
