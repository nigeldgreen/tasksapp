<div class="project-container project-container-client-{{ $project->client_id or 0 }}"
     project_id="{{ $project->id }}"
     client_order="{{ $project->client_order }}">
    <div class="project-row">
        <div class="project-drag-handle">&nbsp;</div>
        <span class="project-title is-open">{{ $project->title }} @if(Auth::user()->debug_view)[{{ $project->client_order }}] @endif</span>
        <a class="project-disclose is-open" href="{{ $project->url }}" target="_blank" title="Hide or show the actions in the project">
            <svg viewBox="0 0 8 8" class="icon">
                <use xlink:href="/images/open-iconic.svg#eye"></use>
            </svg>
        </a>
        @if (isset($project->notes) && $project->notes != "")
            <a class="icon-project-has-notes is-closed" href="{{ $project->url }}" target="_blank" title="Show the notes panel for the action">
                <svg viewBox="0 0 8 8" class="icon">
                    <use xlink:href="/images/open-iconic.svg#copywriting"></use>
                </svg>
            </a>
        @endif

        <div class="project-icons">
            <a class="icon-add-action-to-project" href="{{ route('action.create', $project->id) }}" title="Create an action for this project">
                <svg viewBox="0 0 8 8" class="icon">
                    <use xlink:href="/images/open-iconic.svg#plus"></use>
                </svg>
            </a>
            <a class="icon-edit-project" href="{{ route('project.edit', $project->id) }}" title="Edit this project">
                <svg viewBox="0 0 8 8" class="icon">
                    <use xlink:href="/images/open-iconic.svg#pencil"></use>
                </svg>
            </a>
            <a class="icon-complete-project" href="{{ route('project.destroy', $project->id) }}" title="Mark project as complete">
                <svg viewBox="0 0 8 8" class="icon">
                    <use xlink:href="/images/open-iconic.svg#check"></use>
                </svg>
            </a>
        </div>
    </div>
    <div class="project-notes project-notes-{{ $project->id }}">
        <div class="project-notes-text">
            {!! parsedown($project->notes) !!}
        </div>
    </div>
    <div class="action-container action-container-client-{{ $project->client_id or 0 }}">
        @foreach ($project->actions as $action)
            @include('partials.show_action')
        @endforeach
    </div>
</div>
