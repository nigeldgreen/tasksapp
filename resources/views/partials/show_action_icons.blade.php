<a class="icon-complete-action" href="{{ route('completedaction.store', $action->id) }}" title="Mark action as complete">
    <svg viewBox="0 0 8 8" class="icon">
        <use xlink:href="/images/open-iconic.svg#check"></use>
    </svg>
</a>
<a class="icon-edit-action" href="{{ route('action.edit', $action->id) }}" title="Edit this action">
    <svg viewBox="0 0 8 8" class="icon icon-edit-action">
        <use xlink:href="/images/open-iconic.svg#pencil"></use>
    </svg>
</a>
<a class="icon-is-flagged" href="#" title="Flag action to do next" flagged="{{ $action->flagged }}">
    <svg viewBox="0 0 8 8"
        <?php $flagged = $action->flagged ? "set" : "unset"; ?>
        class="icon flag-icon-{{ $flagged }}">
        <use xlink:href="/images/open-iconic.svg#flag"></use>
    </svg>
</a>
