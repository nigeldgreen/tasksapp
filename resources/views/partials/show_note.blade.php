<div class="note-row" note_id="{{ $note->id }}">
    <div class="note-title">{{ $note->title }}</div>
    <div class="note-overview">{{ $note->overview }}</div>
    <div class="note-row-highlight"></div>
</div>
