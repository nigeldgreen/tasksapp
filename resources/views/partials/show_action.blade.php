<div class="action-display action-id-{{ $action->id }}"
     action_id="{{ $action->id }}"
     project_order="{{ $action->project_order }}">
    <div class="action-row">
        <div class="action-drag-handle">&nbsp;</div>
        <div class="action-title" action_id="{{$action->id}}">
            <span class="action-title-text">
                {{ $action->title }} @if(Auth::user()->debug_view)[{{ $action->project_order }}] @endif
            </span>
            @isset ($action->url)
                <a class="icon-action-has-link" href="{{ $action->url }}" target="_blank" title="Open the URL for the action">
                    <svg viewBox="0 0 8 8" class="icon">
                        <use xlink:href="/images/open-iconic.svg#link-intact"></use>
                    </svg>
                </a>
            @endisset
            @if (isset($action->notes) && $action->notes != "")
                <a class="icon-action-has-notes is-closed" href="{{ $action->url }}" target="_blank" title="Show the notes panel for the action">
                    <svg viewBox="0 0 8 8" class="icon">
                        <use xlink:href="/images/open-iconic.svg#copywriting"></use>
                    </svg>
                </a>
            @endif
        </div>
        <div class="action-flags">
            @if ($action->deleted_at)
                <span class="date-flag date-deleted">{{$action->deleted_at->format('Y-m-d')}}</span>
            @else
                <input type="text"
                       id="date-due-{{ $action->id }}"
                       class="date-flag date-due dateinput"
                       value="{{ $action->due ? $action->due : "  --  " }}">
            @endif
        </div>
        <div class="action-icons">
            @if ($action->deleted_at)
                <a class="icon-action-restore"
                   href=""
                   title="Re-open this action">
                    <svg viewBox="0 0 8 8" class="icon">
                        <use xlink:href="/images/open-iconic.svg#action-undo"></use>
                    </svg>
                </a>
            @else
               @include('partials.show_action_icons')
            @endif
        </div>
    </div>
    <div class="action-notes action-notes-{{ $action->id }}">
        <div class="action-notes-text">
            {!! parsedown($action->notes) !!}
        </div>
    </div>
</div>
