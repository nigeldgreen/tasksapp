@extends('templates/main')

@section('content')
<h1>Helpful text</h1>
<p>This area is where you will see the detail for any actions, project and
    clients you select in the list over to the left. Just click one of them now
    and this message will disappear to show you the interesting stuff.</p>

<h2>Helpful shortcuts</h2>
<p>To help you get around quickly, you can use the following shortcuts:</p>
<h3>Adding new stuff</h3>
<p>You can quick-add new stuff to tasksapp by using the menu at the top, or
    typing the shortcuts:</p>
<ul>
    <li><strong>c c</strong> - create client</li>
    <li><strong>c p</strong> - create project</li>
    <li><strong>c a</strong> - create action</li>
</ul>
<h3>Viewing your tasks</h3>
<p>There are different ways to view your various clients, projects and
    actions in tasksapp:</p>
<ul>
    <li><strong>a</strong> - view <strong>a</strong>ll projects and actions for the active client</li>
    <li><strong>m</strong> - view <strong>m</strong>y day, an agenda view of your most important shizz (your 'daily driver' view)</li>
</ul>
<h3>Other useful stuff to know</h3>
<p>You can use the 'clients' menu to set an active client (this will be on the
    left or in the top bar depending on the size of device you're using).
    Do this if you need to focus on a particular client for a while, or just
    leave it set to 'all' to see everything you need to do.</p>

@endsection