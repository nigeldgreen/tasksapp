@extends('templates/main')

@section('content')

<div id="note-container">
    <div id='note-search'>
        <div class="search-field">
            <form action="{{ route('note.index') }}" method="POST" id='search-notes-form'>
                {{ csrf_field() }}
                <label for="searchterm"></label>
                <input class="form-control" id="note-search-input" name="searchterm" type="text" placeholder="search"value="{{ isset($search_term) ? $search_term : "" }}">
                @if(isset($search_term))
                    <a href="{{ route('note.index') }}" class="clear-search">clear</a>
                @endif
            </form>
        </div>
    </div>
    @foreach ($notes as $note)
        @include('partials.show_note')
    @endforeach
</div>

@endsection
