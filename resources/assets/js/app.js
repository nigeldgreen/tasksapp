window.$ = window.jQuery = require('jquery');

import 'jquery-ui/ui/widgets/datepicker.js';
import 'jquery-ui/ui/widgets/draggable.js';
import 'jquery-ui/ui/widgets/droppable.js';
import 'jquery-ui/ui/widgets/sortable.js';

require('mousetrap');
require('mousetrap-global-bind');

global.moment = require('moment');

require('./tasksapp');
