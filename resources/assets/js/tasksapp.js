$(document).ready(function () {
    // Define variables to avoid duplicating JQuery selectors
    const body = $('body');
    const allActionsBlock = $('#all-actions-block');
    const clientDetailForm = $('#client-form');
    const projectDetailForm = $('#project-form');
    const actionDetailForm = $('#action-form');
    const navBar = $('#navbar');
    const searchBar = $('#searchbar');
    const pageCover = $('#pageCover');
    let formDisplay = $('#form-display');

    // Helper function to refresh the CSRF token
    function refreshCSRFToken() {
        $.ajax({
            url: '/refreshcsrf',
            type: 'GET',
        }).done(function (new_token) {
            $('meta[name="_token"]').attr('content', new_token);
            return new_token;
        });
    }

    // Helper function to wrap all calls to the API for error catch
    // and graceful recovery
    function sendApiRequest(payload) {
        // Copy the data so that we can reference it later if we need to resend the request
        let data = payload;

        // Make sure we have a data attribute and add the CSRF token to it
        data.data = data.data === undefined ? {} : data.data;
        data.data._token = $('meta[name="_token"]').attr('content');

        $.ajax({
            url: data.url,
            type: data.type,
            data: data.data,
        }).done(function (response) {
            if (data.sFunc !== undefined) {
                data.sFunc(response);
            }
        }).fail(function (response) {
            if (response.status === 419) {
                $.ajax({
                    url: '/refreshcsrf',
                    type: 'GET',
                }).done(function () {
                    refreshCSRFToken();
                    sendApiRequest(data);
                });
            }
            if (data.eFunc !== undefined) {
                data.eFunc(response);
            }
        });
    }


    // Helper function to display error at the top of the viewport
    function showErrorMessage(errorMessage) {
        $(errorMessage).insertAfter($('.main-column')).css('top', $(document).scrollTop());
        setTimeout(function () {
            $('.message-error').fadeOut(350, function () {
                $(this).remove();
            });
        }, 1500);
    }


    //---------------------------------------------//
    // --  Client functions and event handlers  -- //
    //---------------------------------------------//
    function showClientCreateForm() {
        sendApiRequest({
            functionCall: "showClientCreateForm",
            url: "/client/create",
            type: "GET",
            sFunc: function success(data) {
                addFormToPage(data, '#client-form-title');
            }
        });
    }
    function showClientUpdateForm(client_id) {
        sendApiRequest({
            functionCall: "showClientUpdateForm",
            url: "/client/" + client_id + "/edit",
            type: "GET",sFunc: function success(data) {
                addFormToPage(data);
                $('#client-form-title').focus();
            }
        });
    }
    function checkClientFormData() {
        let titleField = $('#client-form-title');
        let errors = false;
        if ("" === titleField.val()) {
            titleField.addClass('form-error');
            $('<p class="form-field-error">You need a title here...</p>').insertAfter(titleField);
            errors = true;
        }
        return !errors;
    }
    function storeClient(data) {
        sendApiRequest({
            functionCall: "storeClient",
            url: "/client",
            type: "POST",
            data: data,
            sFunc: function success(client) {
                $(client).insertAfter($('.client-container').last());
                setUpProjectSortables();
                setUpActionSortables();
                updateClientList();
                if ($('#client-form-addanother').is(':checked')) {
                    $('#client-form-title').val("").focus();
                } else {
                    cleanUpUIElements();
                }
            }
        });
    }
    function updateClient(data) {
        let client_id = $('#client-form-id').val();
        sendApiRequest({
            functionCall: "updateClient",
            url: "/client/" + client_id,
            type: "PATCH",
            data: data,
            sFunc: function success(clientRow) {
                cleanUpUIElements();
                updateClientList();
                $('.client-container[client_id=' + client_id + ']').replaceWith($(clientRow));
            }
        });
    }
    function deleteClient(element) {
        const client_id = element.attr('href').split('/')[5];
        sendApiRequest({
            functionCall: "deleteClient",
            url: "/client/delete/" + client_id,
            type: "DELETE",
            eFunc: function () {
                showErrorMessage('<div class="message-error">Can\'t delete client with active projects...</div>')
            },
            sFunc: function () {
                element.closest('.client-container').fadeOut(250);
                updateClientList();
            }
        });
    }
    function hideClient(element) {
        let client_id = element.closest('.client-container').attr('client_id');

        $('.action-container-client-' + client_id)
            .slideUp(250);
        $('.project-container-client-' + client_id)
            .find('.project-disclose')
            .addClass('is-closed')
            .removeClass('is-open');
        element
            .addClass('is-closed')
            .removeClass('is-open');

        // Hide any individual actions
        element.closest('.client-row').siblings('.action-container').each(function() {
            $(this).slideUp(250);
        });

        // Hide the contained projects
        element.closest('.client-row').siblings('.project-container').each(function() {
            hideProject($(this).find('.project-disclose'));
        })
    }
    function showClient(element) {
        let client_id = element.closest('.client-container').attr('client_id');
        $('.action-container-client-' + client_id)
            .slideDown(250);
        $('.project-container-client-' + client_id)
            .find('.project-disclose')
            .addClass('is-open')
            .removeClass('is-closed');
        element
            .addClass('is-open')
            .removeClass('is-closed');

        // Show any individual actions
        element.closest('.client-row').siblings('.action-container').each(function() {
            $(this).slideDown(250);
        });

        // Show the contained projects
        element.closest('.client-row').siblings('.project-container').each(function() {
            showProject($(this).find('.project-disclose'));
        })
    }
    function toggleClientVisibility(element) {
        if (element.hasClass('is-open')) {
            hideClient(element);
        } else {
            showClient(element);
        }
    }
    function updateClientList() {
        sendApiRequest({
            functionCall: "updateClientList",
            url: "/clientlist",
            type: "GET",
            sFunc: function (client_list) {
                $('#nav-form-client_id').html(client_list);
            }
        });
    }

    //-----------------------------------------------//
    // --  Event handlers for client row buttons  -- //
    //-----------------------------------------------//
    $('a#navbar-create-client, a.add-new-client').click(function (e) {
        e.preventDefault();
        showClientCreateForm();
    });
    body.on('click', '#client-form-button', function (e) {
        e.preventDefault();
        let client_data = {
            title: $('#client-form-title').val(),
            notes: $('#client-form-notes').val(),
        };
        if (checkClientFormData()) {
            if (!$('#client-form-id').length) {
                storeClient(client_data);
            } else {
                updateClient(client_data);
            }
        }
    });
    body.on('click', 'a.icon-edit-client', function (e) {
        e.preventDefault();
        showClientUpdateForm($(this).closest('.client-container').attr('client_id'));
    });
    body.on('click', 'a.icon-complete-client', function (e) {
        e.preventDefault();
        deleteClient($(this));
    });
    body.on('click', '.client-disclose', function (e) {
        e.preventDefault();
        toggleClientVisibility($(this));
    });
    body.on('click', '.client-icons', function (e) {
        e.preventDefault();
        let menuEl = $(this).closest('.client-row').find('.full-menu');
        if (menuEl.hasClass('hidden')) {
            $('.full-menu').addClass('hidden');
            menuEl.removeClass('hidden');
        } else {
            menuEl.addClass('hidden');
        }
    });


    //----------------------------------------------//
    // --  Project functions and event handlers  -- //
    //----------------------------------------------//
    function showProjectCreateForm(client_id) {
        sendApiRequest({
            functionCall: "showProjectCreateForm",
            url: null == client_id ? "/project/create" : "/project/create/" + client_id,
            type: "GET",
            sFunc: function success(data) {
                addFormToPage(data, '#project-form-title');
            }
        });
    }
    function showProjectUpdateForm(project_id) {
        sendApiRequest({
            functionCall: "showProjectUpdateForm",
            url: "/project/" + project_id + "/edit",
            type: "GET",
            sFunc: function success(data) {
                addFormToPage(data, '#project-form-title');
            }
        });
    }
    function checkProjectFormData() {
        const formTitleField = $('#project-form-title');
        let errors = false;
        $('.form-error').removeClass('form-error');
        $('.project-form').find('p.form-field-error').remove();
        if ("" === formTitleField.val()) {
            formTitleField.addClass('form-error');
            $('<p class="form-field-error">You need a title here...</p>').insertAfter(formTitleField);
            errors = true;
        }
        return !errors;
    }
    function storeProject(data) {
        sendApiRequest({
            functionCall: "storeProject",
            url: "/project",
            type: "POST",
            data: data,
            sFunc: function success(data) {
                $('[client_id="' + data.client_id + '"]').append($(data.html));
                setUpActionSortables();
                $('#project-form-addanother').is(':checked')
                    ? $('#project-form-title').val("").focus()
                    : cleanUpUIElements();
            }
        });
    }
    function updateProject(data) {
        sendApiRequest({
            functionCall: "storeProject",
            url: "/project/" + $('#project-form-id').val(),
            type: "PATCH",
            data: data,
            sFunc: function success(project) {
                const thisProject = $('[project_id="' + project.id + '"]');
                thisProject.find('.project-title').text(project.title);

                const old_client_id = thisProject.attr('class').split('-').pop();
                const new_client_id = project.client_id;
                if (old_client_id !== new_client_id) {
                    refileProjectByClient(project);
                    // Update the helper classes for toggling visibility
                    thisProject.removeClass('project-container-client-' + old_client_id);
                    thisProject.addClass('project-container-client-' + new_client_id);
                    thisProject.find('.action-container').removeClass('action-container-client-' + old_client_id);
                    thisProject.find('.action-container').addClass('action-container-client-' + new_client_id);
                }
                cleanUpUIElements();
            }
        });
    }
    function refileProjectByClient(project) {
        const client_id = null == project.client_id ? 0 : project.client_id;
        const clientRow = $('[client_id="' + client_id + '"]');

        if (clientRow.length) {
            clientRow.append($('[project_id="' + project.id + '"]'));
        } else {
            $('[project_id="' + project.id + '"]').fadeOut(250, function () {
                $(this).remove();
            });
        }
        setUpProjectSortables();
    }
    function deleteProject(element) {
        const project_id = element.closest('.project-container').attr('project_id');
        sendApiRequest({
            functionCall: "deleteProject",
            url: "/project/" + project_id + '/delete',
            type: "DELETE",
            eFunc: function error() {
                cleanUpUIElements();
                showErrorMessage('<div class="message-error">You can\'t delete a project with active actions.</div>');
            },
            sFunc: function success() {
                element.closest('.project-container').fadeOut(250);
            }
        });
    }
    function hideProject(element) {
        element.closest('.project-container').find('.action-container').slideUp(250);
        element.addClass('is-closed').removeClass('is-open');
    }
    function showProject(element) {
        element.closest('.project-container').find('.action-container').slideDown(250);
        element.addClass('is-open').removeClass('is-closed');
    }
    function toggleProjectVisibility(element) {
        if (element.hasClass('is-open')) {
            hideProject(element);
        } else {
            showProject(element);
        }
    }

    //------------------------------------------//
    // --  Event handlers for project shizz  -- //
    //------------------------------------------//
    $('a#navbar-create-project').click(function (e) {
        e.preventDefault();
        showProjectCreateForm(null);
    });
    body.on('click', 'a.icon-add-project-to-client', function (e) {
        e.preventDefault();
        showProjectCreateForm($(this).closest('.client-container').attr('client_id'));
    });
    body.on('click', 'a.icon-add-action-to-client', function (e) {
        e.preventDefault();
        showActionForm($(this).closest('.client-container').attr('client_id'));
    });
    body.on('click', 'a.icon-edit-project', function (e) {
        e.preventDefault();
        showProjectUpdateForm($(this).closest('.project-container').attr('project_id'));
    });
    body.on('click', '#project-form-button', function (e) {
        e.preventDefault();
        let formData = {
            title: $('#project-form-title').val(),
            notes: $('#project-form-notes').val(),
            client_id: $('#project-form-client_id').val(),
            contentType: "application/json",
        };
        if (checkProjectFormData()) {
            if (!$('#project-form-id').length) {
                storeProject(formData);
            } else {
                updateProject(formData);
            }
        }
    });
    body.on('click', 'a.icon-complete-project', function (e) {
        e.preventDefault();
        deleteProject($(this));
    });
    body.on('click', '.icon-project-has-notes', function (e) {
        e.preventDefault();
        let project_id = $(this).closest('.project-container').attr('project_id');
        $('.project-notes-' + project_id).slideToggle(350);
        $(this).toggleClass('is-open').toggleClass('is-closed');
    });
    body.on('click', '.project-disclose', function (e) {
        e.preventDefault();
        toggleProjectVisibility($(this));
    });
    body.on('click', '.project-icons', function (e) {
        e.preventDefault();
        const menuEl = $(this).closest('.project-row').find('.full-menu');
        if (menuEl.hasClass('hidden')) {
            $('.full-menu').addClass('hidden');
            menuEl.removeClass('hidden');
        } else {
            menuEl.addClass('hidden');
        }
    });

    //---------------------------------------------//
    // --  Sort out the project sortable setup  -- //
    //---------------------------------------------//
    function setUpProjectSortables() {
        $('.client-container').sortable({
            items: ".project-container",
            connectWith: ".client-container",
            placeholder: "ui-state-highlight",
            handle: ".project-drag-handle",
            cursor: "move",
            opacity: 0.6,
            tolerance: "intersect",
            axis: "y",
            start: function start(event, ui) {
                $(ui.item[0]).nextAll('.project-container').each(function () {
                    const newOrder = parseInt($(this).attr('client_order'), 10) - 1;
                    $(this).attr('client_order', newOrder);
                    $(this).find('.project-client-order').text("(" + newOrder + ")");
                });
            },
            stop: function stop(event, ui) {
                let project_id = ui.item[0].attributes['project_id'].value;
                let old_client_id = $(event.target).closest('.client-container').attr('client_id') || 0;
                let new_client_id = $(ui.item[0]).closest('.client-container').attr('client_id') || 0;
                let new_client_order = parseInt($(ui.item[0]).prev('.project-container').attr('client_order'), 10) + 1 || 1;
                sendApiRequest({
                    functionCall: "setUpProjectSortables",
                    url: '/project/' + project_id,
                    type: "PATCH",
                    data: {
                        project_id: project_id,
                        client_id: new_client_id,
                        client_order: new_client_order,
                    },
                    sFunc: function success() {
                        let client_detail = $(ui.item[0]);
                        // Update the client order for the dropped item
                        client_detail.attr('client_order', new_client_order);
                        // Update the subsequent projects in the client
                        client_detail.nextAll().each(function () {
                            $(this).attr('client_order', parseInt($(this).attr('client_order'), 10) + 1);
                        });
                        client_detail.removeClass('project-container-client-' + old_client_id);
                        client_detail.addClass('project-container-client-' + new_client_id);
                        client_detail.find('.action-container').removeClass('action-container-client-' + old_client_id);
                        client_detail.find('.action-container').addClass('action-container-client-' + new_client_id);
                    }
                });
            }
        });
    }
    setUpProjectSortables();


    //---------------------------------------------//
    // --  Action functions and event handlers  -- //
    //---------------------------------------------//
    function showActionForm(action_id, project_id) {
        let url = "/action";
        // Edit if we have an action ID
        if (action_id != null) {
            url += "/" + action_id + "/edit";
        }
        // Create within the project if we have a project ID
        if (project_id != null) {
            url += "/create/" + project_id;
        }
        sendApiRequest({
            functionCall: "showActionForm",
            url: url,
            type: "GET",
            sFunc: function success(data) {
                addFormToPage(data, '#action-form-title');
            }
        });
    }
    function checkActionFormData() {
        let formTitle = $('#action-form-title');
        let errors = false;
        $('.form-error').removeClass('form-error');
        actionDetailForm.find('p.form-field-error').remove();
        if ("" === formTitle.val()) {
            formTitle.addClass('form-error');
            $('<p class="form-field-error">You need a title here...</p>').insertAfter(formTitle);
            errors = true;
        }
        return !errors;
    }
    function storeAction(data) {
        sendApiRequest({
            functionCall: "storeAction",
            url: "/action",
            type: "POST",
            data: data,
            sFunc: function success(action) {
                addActionToPage(action);
                if ($('#action-form-addanother').is(':checked')) {
                    $('#action-form-title').val("").focus();
                    $('#action-form-notes').val("");
                    $('#action-form-due').val("");
                    $('#action-form-url').val("");
                } else {
                    cleanUpUIElements();
                }
            }
        });
    }
    function addActionToPage(action) {
        sendApiRequest({
            functionCall: "addActionToPage",
            url: "/action/" + action.id,
            type: "GET",
            data: {
                show_handles: showHandles()
            },
            sFunc: function success(actionRow) {
                refileActionByProject(action, actionRow);
            }
        });
    }
    function updateAction(data) {
        const action_id = data.action_id ? data.action_id : $('#action-form-id').val();
        sendApiRequest({
            functionCall: "updateAction",
            url: "/action/" + action_id,
            type: "PATCH",
            data: data,
            sFunc: function success(action) {
                sendApiRequest({
                    functionCall: "addActionToPage",
                    url: "/action/" + action.id,
                    type: "GET",
                    data: {
                        show_handles: showHandles()
                    },
                    sFunc: function success(actionRow) {
                        cleanUpUIElements();
                        const actionToUpdate = $('.action-id-' + action.id);
                        const client_id = actionToUpdate.closest('.client-container').attr('client_id');
                        const project_id = actionToUpdate.closest('.project-container').attr('project_id');
                        const existingClient = undefined == client_id ? 0 : client_id;
                        const existingProject = undefined == project_id ? 0 : project_id;

                        // If action is not moving then update in place, otherwise refile
                        if (window.location.pathname === '/search' || (action.project_id == existingProject && action.client_id == existingClient)) {
                            actionToUpdate.before(actionRow);
                            actionToUpdate.remove();
                        } else {
                            actionToUpdate.remove();
                            refileActionByProject(action, actionRow);
                        }
                    }
                });
            }
        });
    }
    function refileActionByProject(action, actionRow) {
        const projectRow = $('[project_id="' + action.project_id + '"]');
        const today = moment();

        if (window.location.pathname == '/myday') {
            if (action.due != null) {
                if (moment(action.due).isAfter(today, 'day')) {
                    $('.future-actions').append($(actionRow));
                } else {
                    $('.due-actions').append($(actionRow));
                }
            } else if (action.flagged == "1") {
                $('.flagged-actions').append($(actionRow));
            }
        } else {
            if (0 != action.project_id) {
                if (projectRow.length) {
                    const lastProjectRow = projectRow.find('.action-display').last();
                    if (lastProjectRow.length) {
                        $(actionRow).insertAfter(lastProjectRow);
                    } else {
                        projectRow.find('.action-container').append($(actionRow));
                    }
                } else {
                    $(actionRow).fadeOut(250, function () {
                        $(this).remove();
                    });
                }
            } else {
                if (0 == action.client_id) {
                    const inboxContainer = $('#inbox-container');
                    if (inboxContainer.length) {
                        inboxContainer.append($(actionRow));
                    } else {
                        $(actionRow).fadeOut(250, function () {
                            $(this).remove();
                        });
                    }
                } else {
                    const clientRow = $('[client_id="' + action.client_id + '"]');
                    if (clientRow.find('.action-container').first().length) {
                        clientRow.find('.action-container').first().append(actionRow);
                    } else {
                        $(actionRow).fadeOut(250, function () {
                            $(this).remove();
                        });
                    }
                }
            }
        }
    }
    function setUpActionFormDropdowns(element) {
        const client_id = element.val();
        sendApiRequest({
            functionCall: "setUpActionFormDropdowns",
            url: "/client/getProjects/" + client_id,
            type: "GET",
            sFunc: function success(projectList) {
                const actionForm = $('#action-form-project_id');
                actionForm.find("option").remove();
                actionForm.append(projectList);
            }
        });
    }
    function deleteAction(element) {
        const action_id = element.closest('.action-display').attr('action_id');
        $('.action-id-' + action_id).nextAll().each(function () {
            const order = parseInt($(this).attr('project_order'), 10) - 1;
            $(this).attr('project_order', order);
        });
        sendApiRequest({
            functionCall: "deleteAction",
            url: "/completeaction/" + action_id,
            type: "POST",
            sFunc: function success() {
                $('.action-id-' + action_id).remove();
            }
        });
    }
    function flagAction(element) {
        const flagged = element.attr('flagged') == 0 ? 1 : 0;
        const action_id = element.closest('.action-display').attr('action_id');
        const project_id = element.closest('.project-container').attr('project_id');
        const client_id = element.closest('.client-container').attr('client_id');
        const parent = element.closest('.full-menu');
        sendApiRequest({
            functionCall: "flagAction",
            url: "/action/" + action_id,
            type: "PATCH",
            data: {
                flagged: flagged,
                client_id: client_id,
                project_id: project_id,
            },
            sFunc: function success(action) {
                parent.prev().find('svg').toggleClass('flag-icon-set').toggleClass('flag-icon-unset');
                element.find('svg').toggleClass('flag-icon-set').toggleClass('flag-icon-unset');
                if (flagged === 0) {
                    element.attr('flagged', flagged);
                    parent.addClass('hidden');
                    if (window.location.pathname === '/myday' && element.closest('.project-container').attr('type') === "flagged") {
                        element.closest('.action-display').fadeOut(250);
                    }
                }
                cleanUpUIElements();
            }
        });
    }
    function restoreAction(element) {
        let action_id = element.closest('.action-display').attr('action_id');
        let url = "/restoreaction/" + action_id;

        sendApiRequest({
            functionCall: "restoreAction",
            url: url,
            type: "DELETE",
            sFunc: function success() {
                $('.action-id-' + action_id).fadeOut(200);
            },
            eFunc: function error(data) {
                // console.log(data);
            }
        });
    }
    function editActionTitleField(element) {
        const parent = element.closest('.action-title');
        const action_id = parent.attr('action_id');
        sendApiRequest({
            functionCall: "editActionTitleField",
            url: "/actiontitle/" + action_id + "/edit",
            type: "GET",
            sFunc: function success(html) {
                element.closest('.action-title').html(html);
                parent.find('#dynamic-title-field').select();
            }
        });
    }
    function updateActionTitleField(element) {
        const action_id = element.attr('action_id');
        sendApiRequest({
            functionCall: "updateActionTitleField",
            url: "/actiontitle/" + action_id,
            type: "POST",
            data: {
                title: $('#dynamic-title-field').val(),
                show_handles: showHandles()
            },
            sFunc: function success(html) {
                let element = $('.action-id-' + action_id);
                element.hide();
                element.replaceWith(html);
                element.fadeIn(300);
            }
        });
    }
    function showActionTitle() {
        const action_id = $('#dynamic-title-field').closest('form').attr('action_id');
        sendApiRequest({
            functionCall: "showActionTitle",
            url: "/action/" + action_id,
            type: "GET",
            data: {
                action_id: action_id,
                show_handles: showHandles()
            },
            sFunc: function success(html) {
                let element = $('.action-id-' + action_id);
                element.hide();
                element.replaceWith(html);
                element.fadeIn(300);
            }
        });
    }

    //-----------------------------------------------//
    // --  Event handlers for action row buttons  -- //
    //-----------------------------------------------//
    $('#icon-add-action-to-inbox, a#navbar-create-action').click(function (e) {
        e.preventDefault();
        showActionForm(null, null);
    });
    body.on('click', 'a.icon-add-action-to-project', function (e) {
        e.preventDefault();
        showActionForm(null, $(this).closest('.project-container').attr('project_id'));
    });
    body.on('click', 'a.icon-edit-action', function (e) {
        e.preventDefault();
        $('.action-display-active').removeClass('action-display-active');
        $(this).closest('.action-display').addClass('action-display-active');
        showActionForm($(this).closest('.action-display').attr('action_id'), null);
    });
    body.on('click', '#action-form-button', function (e) {
        e.preventDefault();
        let action_data = {
            title: $('#action-form-title').val(),
            notes: $('#action-form-notes').val(),
            flagged: $('#action-form #flagged').is(':checked') ? 1 : 0,
            due: $('#action-form-due').val(),
            url: $('#action-form-url').val(),
            client_id: $('#action-form-client_id').val(),
            project_id: $('#action-form-project_id').val(),
        };
        if (checkActionFormData()) {
            if (!$('#action-form-id').length) {
                storeAction(action_data);
            } else {
                updateAction(action_data);
            }
        }
    });
    body.on('click', 'a.icon-complete-action', function (e) {
        e.preventDefault();
        deleteAction($(this));
    });
    body.on('click', 'a.icon-is-flagged', function (e) {
        e.preventDefault();
        flagAction($(this));
    });
    body.on('click', 'a.icon-action-restore', function (e) {
        e.preventDefault();
        restoreAction($(this));
    });
    body.on('change', '#action-form-client_id', function () {
        setUpActionFormDropdowns($(this));
    });
    body.on('change', 'input.date-due', function () {
        const data = {
            action_id: $(this).closest('.action-display').attr('action_id'),
            due: $(this).val(),
        };
        updateAction(data);
    });
    body.on('click', '.icon-action-has-notes', function (e) {
        e.preventDefault();
        const action_id = $(this).closest('.action-display').attr('action_id');
        $('.action-notes-' + action_id).slideToggle(350);
        $(this).toggleClass('is-open').toggleClass('is-closed');
    });
    body.on('click', 'span.action-title-text', function (e) {
        e.preventDefault();
        editActionTitleField($(this));
    });
    body.on('submit', 'form#dynamic-action-title', function (e) {
        e.preventDefault();
        updateActionTitleField($(this));
    });
    body.on('blur', '#dynamic-title-field', function () {
        showActionTitle();
    });
    body.on('click', 'span.hamburger', function (e) {
        e.preventDefault();
        const menuEl = $(this).closest('.action-row').find('.full-menu');
        if (menuEl.hasClass('hidden')) {
            $('.full-menu').addClass('hidden');
            menuEl.removeClass('hidden');
        } else {
            menuEl.addClass('hidden');
        }
    });

    function setUpActionSortables() {
        $('.action-container').sortable({
            items: ".action-display",
            connectWith: ".action-container",
            placeholder: "ui-state-highlight",
            handle: ".action-drag-handle",
            cursor: "move",
            opacity: 0.6,
            revert: false,
            axis: "y",
            tolerance: "intersect",
            // Update subsequent actions in original project when we take the action out
            start: function start(event, ui) {
                $(ui.item[0]).nextAll('.action-display').each(function () {
                    const newOrder = parseInt($(this).attr('project_order'), 10) - 1;
                    $(this).attr('project_order', newOrder);
                });
            },
            stop: function stop(event, ui) {
                const action_id = ui.item[0].attributes['action_id'].value;
                const new_project_id = $(ui.item[0]).closest('.project-container').attr('project_id') || 0;
                const new_client_id = $(ui.item[0]).closest('.client-container').attr('client_id') || 0;
                const new_project_order = parseInt($(ui.item[0]).prev('.action-display').attr('project_order'), 10) + 1 || 1;

                // Update the UI components for the dropped action
                $('.action-id-' + action_id).attr('project_order', parseInt(new_project_order, 10));
                $(ui.item[0]).nextAll().each(function () {
                    $(this).attr('project_order', parseInt($(this).attr('project_order'), 10) + 1);
                });

                sendApiRequest({
                    functionCall: "storeMovedAction",
                    url: '/action/' + action_id,
                    type: "PATCH",
                    data: {
                        project_id: new_project_id,
                        client_id: new_client_id,
                        project_order: new_project_order,
                    }
                });
            }
        });
    }
    setUpActionSortables();



    //
    // Note functions and event handlers
    //
    function addNotesFormToPage(data) {
        $('.right-column .right-column-content').html(data);
    }
    function showNoteDetail(note_id) {
        sendApiRequest({
            functionCall: "showNote",
            url: "/note/" + note_id,
            type: "GET",
            sFunc: function success(data) {
                addNotesFormToPage(data);
            }
        });
    }
    function showNoteCreateForm() {
        sendApiRequest({
            functionCall: "showNoteForm",
            url: "/note/create",
            type: "GET",
            sFunc: function success(data) {
                if ('/notes' === window.location.pathname) {
                    addNotesFormToPage(data);
                } else {
                    addFormToPage(data);
                }
                $('#note-form-title').focus();
            }
        });
    }
    function showNoteUpdateForm(note_id) {
        sendApiRequest({
            functionCall: "showNoteUpdateForm",
            url: "/note/" + note_id + "/edit",
            type: "GET",
            sFunc: function success(data) {
                addNotesFormToPage(data);
                $('#note-form-title').focus();
            }
        });
    }
    function getNoteFormData() {
        return {
            title: $('#note-form-title').val(),
            body: $('#note-form-body').val(),
            tags: $('#note-form-tags').val(),
            client_id: $('#note-form-client_id').val(),
        };
    }
    function validateFormData(data) {
        let formDataIsClean = true;

        if ("" === data.title) {
            let noteTitleField = $('#note-form-title');
            noteTitleField.addClass('form-error');
            $('<p class="form-field-error">You need a title here...</p>').insertAfter(noteTitleField);
            formDataIsClean = false;
        }

        if ("" === data.body) {
            let noteBodyField = $('#note-form-body');
            noteBodyField.addClass('form-error');
            $('<p class="form-field-error">You need some text here...</p>').insertAfter(noteBodyField);
            formDataIsClean = false;
        }

        return formDataIsClean;
    }
    function saveNote(action) {
        // Remove any old screen messages
        $('.form-error').removeClass('form-error');
        $('#note-form').find('p.form-field-error').remove();

        // Deal with the form if all is OK
        let note_data = getNoteFormData();
        if (validateFormData(note_data)) {
            storeNote(note_data, action);
        }
    }
    function storeNote(data, action) {
        const note_id = $('#note-form-id').val();
        let url = "/note";
        let type = "POST";
        if (note_id > 0) {
            url = url + '/' + note_id;
            type = "PATCH";
        }

        sendApiRequest({
            functionCall: "storeNote" + type,
            url: url,
            type: type,
            data: data,
            sFunc: function success(data) {
                $('#note-form-id').val(data.note.id);

                addNoteToTopOfList(data.html, data.note.id);

                if (action === 'note-save-and-stay') {
                    UpdateSaveButtonText();
                } else {
                    showNoteDetail($('.note-row').first().attr('note_id'));
                }
            }
        });
    }
    function deleteNote(note_id) {
        sendApiRequest({
            functionCall: "deleteNote",
            url: "/note/" + note_id,
            type: "DELETE",
            sFunc: function success() {
                $('.note-row[note_id=' + note_id + ']').remove();
                previewFirstNote();
            }
        });
    }
    function addNoteToTopOfList(note_row, note_id) {
        if (note_id > 0) {
            $(".note-row[note_id=" + $('#note-form-id').val() + "]").remove();
        }
        $('#note-search').after(note_row);
        $('.note-row-active').removeClass('note-row-active');
        $('.note-row').first().addClass('note-row-active');
    }
    function previewFirstNote() {
        let active_note = $('.note-row').first();
        active_note.addClass('note-row-active');
        showNoteDetail(active_note.attr('note_id'));
    }
    function UpdateSaveButtonText() {
        let button = $('#note-save-and-stay');

        button.val('Saved!');
        setTimeout(function () {
            button.val('Save and keep editing');
        }, 1250);
    }

    body.on('click', '.note-row', function () {
        showNoteDetail($(this).attr('note_id'));
        $('.note-row-active').removeClass('note-row-active');
        $(this).addClass('note-row-active');
    });
    $('a#navbar-create-note').click(function (e) {
        e.preventDefault();
        showNoteCreateForm();
    });
    body.on('click', '.note-form-button', function (e) {
        e.preventDefault();
        saveNote($(this).attr('id'));
    });
    body.on('click', 'a.edit-note-link', function (e) {
        e.preventDefault();
        showNoteUpdateForm($(this).attr('note_id'));
    });
    body.on('click', 'a.delete-note-link', function (e) {
        e.preventDefault();
        deleteNote($(this).attr('note_id'));
    });

    // Highlight first note on page load
    if ((window.location.pathname === '/notes') && ($('.note-row').first().length > 0)) {
        previewFirstNote();
    }



    // Change the client on using subnavbar select
    $('#nav-form-client_id').change(function (e) {
        e.preventDefault();
        window.location.assign("/client/switch/" + $(this).val());
    });

    // Global expand/collapse of actions/projects
    $('#collapse-tree').click(function(e) {
        e.preventDefault();
        if ($('.client-disclose.is-open').length > 0) {
            $('.client-disclose').each(function () {
                hideClient($(this));
            });
        } else {
            $('.client-disclose').each(function () {
                showClient($(this));
            });
        }
    });

    // Global show/hide for notes associated with actions
    $('#toggle-notes').click(function(e) {
        e.preventDefault();
        if ($('.action-notes:visible').length === 0) {
            $('.action-notes').each(function() {
                if ($.trim($(this).children(':first')[0].innerHTML) !== "") {
                    $(this).slideDown(350);
                }
                $(this).closest('.action-display').find('.icon-action-has-notes').addClass('is-open').removeClass('is-closed')
            });
        } else {
            $('.action-notes').each(function() {
                $(this).slideUp(350);
                $(this).closest('.action-display').find('.icon-action-has-notes').removeClass('is-open').addClass('is-closed')
            });
        }

    });

    // Utility functions
    function goToNextNote(current, newNote) {
        if (newNote.length > 0) {
            current.toggleClass('note-row-active');
            newNote.toggleClass('note-row-active');
            showNoteDetail(newNote.attr('note_id'));
        }
    }
    function goToPreviousNote(current, newNote) {
        if (newNote.length > 0) {
            current.toggleClass('note-row-active');
            newNote.toggleClass('note-row-active');
            showNoteDetail(newNote.attr('note_id'));
        }
    }
    function showHandles() {
        return window.location.pathname === '/myday' || window.location.pathname === '/search' ? 0 : 1;
    }

    // Add elements dynamically to actions page
    function addFormToPage(data, focusElement) {
        pageCover.show();
        formDisplay.css('top', $(document).scrollTop());
        formDisplay.append(data);
        if (! formDisplay.is(':visible')) {
            formDisplay.show();
        }
        $(focusElement).focus();
    }

    // Set the format for the datepicker
    body.on('focus', '.dateinput', function() {
        $(this).datepicker({ dateFormat: "yy-mm-dd" });
        $(this).select();
    });

    // Use JQuery to manage the hover of the drop-down user menu
    $('li:hover ul, li ul:hover').css("display", "none");
    $('.menu').click(function() {
        $(this).find('ul').first().stop(true,true).fadeToggle('fast');
    });

    // Show/hide the reset password form on the admin page
    $('a.show-reset-form').click(function(e) {
        e.preventDefault();
        $('form.admin-reset-password').slideToggle(250);
    });

    // Manage the state of the debug view button on the superadmin page
    $('#debug_view').click(function(e) {
        const debug = $(this).prop('checked') ? 1 : null;

        sendApiRequest({
            functionCall: "toggleDebug",
            url: "/user",
            type: "PUT",
            data: {
                'debug_view': debug
            },
            sFunc: function success(data) {
            }
        });
    });

    // Reindex the actions and projects if the superadmin button is clicked
    $('#reindex').click(function(e) {
        sendApiRequest({
            functionCall: "reindexAll",
            url: "/reindex",
            type: "GET",
            sFunc: function success(action) {
            }
        });
    });

    // Update the CSRF token when search box is focused
    $('input#search').focus(function(e) {
        e.preventDefault();
        console.log("refresh token");
        refreshCSRFToken();
    });

    // Clear up dynamic forms if close button clicked
    formDisplay.on('click', 'a.close-dynamic-form', function(e) {
        e.preventDefault();
        cleanUpUIElements();
    });

    // Clear up dynamic forms if close button clicked
    $('.right-column-notes').on('click', 'a.close-dynamic-form', function(e) {
        e.preventDefault();
        showNoteDetail($('.note-row-active').attr('note_id'));
    });

    // Show/hide the popover help page
    allActionsBlock.find('.close-link a').click(function(e) {
        e.preventDefault();
        allActionsBlock.fadeOut(250);
    });

    // Show/hide the popover help page
    allActionsBlock.click(function() {
        $(this).fadeOut(250);
    });

    // Clean up all dynamic forms
    function cleanUpUIElements() {
        if(formDisplay.is(':visible')) {
            formDisplay.fadeOut(250);
            formDisplay.contents().remove();
        }
        if(allActionsBlock.length) {
            allActionsBlock.fadeOut(350);
        }
        if(clientDetailForm.length) {
            clientDetailForm.fadeOut(250, function() {
                $(this).remove();
            });
        }
        if(projectDetailForm.length) {
            projectDetailForm.fadeOut(250, function() {
                $(this).remove();
            });
        }
        if(actionDetailForm.length) {
            actionDetailForm.fadeOut(250, function() {
                $(this).remove();
            });
        }
        if(navBar.find('.menu li ul').is(':visible')) {
            navBar.find('.menu li ul').fadeOut(250);
        }
        if(searchBar.is(':visible')) {
            searchBar.fadeOut(250);
        }
        if(pageCover.is(':visible')) {
            pageCover.fadeOut(250);
        }
        $('.action-display-active').removeClass('action-display-active');
        $('.full-menu').addClass('hidden');
        if ($('#note-form').is(':visible')) {
            showNoteDetail($('.note-row-active').attr('note_id'));
        }
        if ($('#dynamic-title-field').is(':visible')) {
            showActionTitle();
        }
    }



    //
    // Define the keyboard shortcuts
    //
    Mousetrap.bind('s s', function () {
        notefield = $('#note-search-input').length == 0 ? $('input#search') : $('#note-search-input');
        notefield.val("");
        notefield.focus();
    });
    Mousetrap.bind('c c', function () {
        showClientCreateForm();
    });
    Mousetrap.bind('c p', function () {
        showProjectCreateForm(null);
    });
    Mousetrap.bind('c a', function () {
        showActionForm(null, null);
    });
    Mousetrap.bind('c n', function () {
        showNoteCreateForm();
    });
    Mousetrap.bind('v a', function () {
        window.open("/client", "_self");
    });
    Mousetrap.bind('v m', function () {
        window.open("/myday", "_self");
    });
    Mousetrap.bind('v j', function () {
        window.open("/completedactions", "_self");
    });
    Mousetrap.bind('v n', function () {
        window.open("/notes", "_self");
    });
    Mousetrap.bind('u a', function () {
        window.open("/admin", "_self");
    });
    Mousetrap.bind('u h', function () {
        window.open("/help", "_self");
    });
    Mousetrap.bind('u l', function () {
        window.open("/logout", "_self");
    });
    Mousetrap.bind('j', function () {
        if (window.location.pathname == '/notes') {
            var current = $('.note-row-active');
            var newNote = current.next();
            goToNextNote(current, newNote);
        } else if (window.location.pathname == '/client') {
            goToNextAction();
        }
    });
    Mousetrap.bind('k', function () {
        if (window.location.pathname == '/notes') {
            var current = $('.note-row-active');
            var newNote = current.prev();
            goToPreviousNote(current, newNote);
        } else if (window.location.pathname == '/client') {
            goToPreviousAction();
        }
    });
    Mousetrap.bind('e', function() {
        var note = $('.note-row-active');
        if (note.length > 0) {
            showNoteUpdateForm(note.attr('note_id'));
        }
    });
    Mousetrap.bind('.', function () {
        $('#quick-entry-form').find('#title').focus();
    }, 'keyup');
    Mousetrap.bind('?', function () {
        allActionsBlock.fadeToggle(350);
    });
    Mousetrap.bindGlobal('esc', function () {
        cleanUpUIElements();
    });
    Mousetrap.bindGlobal('ctrl+s', function (e) {
        e.preventDefault();
        saveNote("note-save-and-stay");
    });
    Mousetrap.bindGlobal('shift+ctrl+s', function (e) {
        e.preventDefault();
        saveNote("note-save-and-leave");
    });
});
