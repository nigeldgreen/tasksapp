<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewUserRegisters extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $url;
    public $firstname;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $url)
    {
        $this->user = $user;
        $this->url = $url;
        $this->firstname = $user->getFirstName();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Welcome to tasksapp, ' . $this->firstname)
            ->view('emails.firstrun');
    }
}
