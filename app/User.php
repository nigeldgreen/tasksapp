<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'su',
        'api_token',
        'debug_view',
        'totalLogins',
        'lastLoggedInOn',
        'lastActive',
        'validated_at',
        'hide_due',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $dates = [
        'validated_at',
    ];


    // Relationships
    /**
     * Get all of the clients for the user.
     */
    public function clients()
    {
        return $this->hasMany(Client::class);
    }
    /**
     * Get all of the projects for the user.
     */
    public function projects()
    {
        return $this->hasMany(Project::class);
    }
    /**
     * Get all of the actions for the user.
     */
    public function actions()
    {
        return $this->hasMany(Action::class);
    }
    /**
     * Get all of the feedback for the user.
     */
    public function feedback()
    {
        return $this->hasMany(Feedback::class);
    }
    /**
     * Get all of the notes for the user
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes()
    {
        return $this->hasMany(Note::class);
    }


    // Utility methods for building the client list
    /**
     * Get just the inbox actions for this user
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function inboxActions()
    {
        return $this->actions()
            ->HideDueIfFlagged()
            ->where('project_id', 0)
            ->where('client_id', 0)
            ->orderBy('project_order');
    }
    /**
     * Get just the inbox projects for this user
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function inboxProjects()
    {
        return $this->projects()
            ->where('client_id', 0)
            ->orderBy('client_order');
    }


    // Enhanced getters
    /**
     * Get the users first name
     *
     * Created from the full name as we don't currently ask for the name to be
     * split out when a user registers.
     *
     * @return string
     */
    public function getFirstName()
    {
        return explode(' ', $this->name)[0];
    }

    /**
     * Return array of recent user data for display on admin screen
     * @return array
     */
    public function getThisWeekSummary()
    {
        return [
            "actions_added" => $this->actions()->withTrashed()->oneWeekOld()->count(),
            "actions_deleted" => $this->actions()->onlyTrashed()->oneWeekOld()->count(),
            "projects_added" => $this->projects()->withTrashed()->oneWeekOld()->count(),
            "projects_deleted" => $this->projects()->onlyTrashed()->oneWeekOld()->count(),
            "clients_added" => $this->clients()->withTrashed()->oneWeekOld()->count(),
            "clients_deleted" => $this->clients()->onlyTrashed()->oneWeekOld()->count(),
        ];
    }

    /**
     * Return array of all-time user data for display on admin screen
     * @return array
     */
    public function getAllTimeSummary()
    {
        return [
            "actions_added" => $this->actions()->withTrashed()->count(),
            "actions_deleted" => $this->actions()->onlyTrashed()->count(),
            "projects_added" => $this->projects()->withTrashed()->count(),
            "projects_deleted" => $this->projects()->onlyTrashed()->count(),
            "clients_added" => $this->clients()->withTrashed()->count(),
            "clients_deleted" => $this->clients()->onlyTrashed()->count(),
        ];
    }

    /**
     * Return array of all-time user data for display on admin screen
     * @return bool
     */
    public function isValidated(): bool
    {
        return $this->validated_at != null;
    }

    /**
     * Set user as validated, clean up the token and log the user in
     *
     * Quite a lot going on here, but it should all be seen as a single action so will
     * only split out if there is a use-case for that in the future.
     *
     * @param RegistrationToken $token
     */
    Public function updateAndDeleteToken(RegistrationToken $token): void {
        $this->update([
            'validated_at' => Carbon::now(),
        ]);

        $token->delete();

        Auth::login($this);
    }



    // Helpers to manage the db flags for the user
    public function hideDueActions() {
        $this->update([
            'hide_due' => true,
        ]);
    }
    public function shouldHideDueActions() {
        return $this->hide_due == 1;
    }


    /**
     * Log activity for this user
     */
    public function logActivity()
    {
        $this->lastActive = Carbon::now();
        $this->save();
    }
}
