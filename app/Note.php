<?php

namespace App;

use App\Events\NoteCreating;
use App\Events\NoteUpdated;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Note extends Model
{
    use SoftDeletes, HasFactory;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['title','body','client_id','tags'];
    protected $casts = [
        'client_id' => 'integer',
        'project_id' => 'integer',
        'action_id' => 'integer',
        'user_id' => 'integer',
    ];

    /**
     * Get the user that the note belongs to
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /**
     * Get the client that the note belongs to
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    /**
     * Get the project that the note belongs to
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
    /**
     * Get the action that the note belongs to
     */
    public function action()
    {
        return $this->belongsTo(Action::class);
    }
    /**
     * Convert the body of a note to HTML for display in a view
     */
    public function getBodyAsHTML()
    {
        return "<div class='display-note'>"
            . "<h1>$this->title"
            . "<span class='note-head-tools'><a href='#' class='edit-note-link' note_id='" . $this->id . "'>edit</a></span>"
            . "<span class='note-head-tools'><a href='#' class='delete-note-link' note_id='" . $this->id . "'>delete</a></span>"
            . "</h1>"
            . $this->getTagsAsHTML()
            . parsedown($this->body) . "</div>";
    }
    public function getTagsAsHTML()
    {
        $html = "<div class=\"tags-container\"><p>Tags: </p><ul class=\"tags-list\">";
        foreach(explode(',', $this->tags) as $tag) {
            $html .= "<li class='tags-list-item'>$tag</li>";
        }
        $html .= "</ul></div>";

        $empty = "<div class=\"tags-container\"><p>Tags: none defined</p></div>";

        return $this->tags == "" ? $empty : $html;    
    }
    public function makeOverview()
    {
        if (strlen($this->body) > 79) {
            $this->overview = substr($this->body, 0, 79) . "...";
        } else {
            $this->overview = $this->body;
        }
    }
    public function createTagCollection()
    {
        return collect(explode(',', $this->tags));
    }
}
