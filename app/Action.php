<?php
namespace App;

use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Action extends Model
{
    use SoftDeletes, Notifiable, HasFactory;

    protected $dates = [
        'due',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    protected $fillable = [
        'title',
        'url',
        'due',
        'notes',
        'user_id',
        'client_id',
        'project_id',
        'project_order',
        'flagged',
        'created_at',
        'deleted_at'
    ];
    protected $casts = [
        'project_id' => 'integer',
        'client_id' => 'integer',
        'user_id' => 'integer',
        'project_order' => 'integer',
    ];

    // Define relationships
    /**
     * Get the User that the action belongs to
     *
     * @return belongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    /**
     * Get the project that the action belongs to
     *
     * @return belongsTo
     */
    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }
    /**
     * Get the client that the action belongs to
     *
     * @return belongsTo
     */
    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class);
    }
    /**
     * Get all of the notes for the client.
     *
     * @return HasMany
     */
    public function notes(): HasMany
    {
        return $this->hasMany(Note::class);
    }



    // Extended getters and setters
    /**
     * Return the due date formatted for display
     *
     * @param string|null $value
     * @return string|null
     */
    public function getDueAttribute(string|null $value): ?string
    {
        if (null == $value) {
            return null;
        } else {
            return Carbon::parse($value)->format('Y-m-d');
        }
    }
    /**
     * Return the project or client title that the action belongs to
     *
     * @return string
     */
    public function getProjectTitle(): string
    {
        if ($this->project_id == 0) {
            if ($this->client_id == 0) {
                return "Inbox";
            } else {
                return Client::where('id', $this->client_id)->withTrashed()->first()->title;
            }
        } else {
            return Project::where('id', $this->project_id)->withTrashed()->first()->title;
        }
    }
    /**
     * Return text value for flagged status
     *
     * @return string
     */
    public function getFlaggedStatus(): string
    {
        return $this->flagged ? "set" : "unset";
    }
    /**
     * Return text value for flagged status
     *
     * @return string
     */
    public function getLocation(): string
    {
        $clientTitle = $this->client ? $this->client->title : '';
        $projectTitle = $this->project ? $this->project->title : '';

        if ($clientTitle . $projectTitle == '')
            return 'Inbox';

        return $clientTitle . ' / ' . $projectTitle;
    }



    // Helper methods for moving actions between clients/projects
    /**
     * Wrapper function to update request when moving an action between client/project
     *
     * @param Request $request
     * @return void
     */
    public function moveAction(Request $request) : void
    {
        $new_values = [
            'client_id' => intval($request->input('client_id', $this->client_id)),
            'project_id' => intval($request->input('project_id', $this->project_id)),
            'project_order' => intval($request->input('project_order', $this->project_order)),
        ];

        if ($this->shouldMoveAction($new_values)) {
            $this->removeFromOrder();
            $this->addToOrder($new_values['client_id'], $new_values['project_id'], $new_values['project_order']);

            // unset the values that were amended above to prevent propagation
            unset($request['client_id']);
            unset($request['project_id']);
            unset($request['project_order']);
        }
    }

    /**
     * Test new params to see if the action is moving within the project structure
     *
     * @param array $params
     * @return bool
     */
    public function shouldMoveAction(array $params) : bool
    {
       return $this->project_id !== $params['project_id']
           || $this->client_id !== $params['client_id']
           || $this->project_order !== $params['project_order'];
    }
    /**
     * Get the sibling actions from the current action's project
     *
     * @return Collection
     */
    protected function getSiblings()
    {
        if ($this->project_id > 0) {
            $actions = Action::where('project_id', $this->project_id)->get();
        } elseif ($this->client_id > 0) {
            $actions = Action::where('client_id', $this->client_id)->get();
        } else {
            $actions = $this->user->inboxActions()->get();
        }

        return $actions;
    }
    /**
     * Remove the current action from the project
     *
     * Take the current action out of the project and update the project_order
     * for the remaining actions
     *
     * @return bool
     */
    public function removeFromOrder()
    {
        DB::beginTransaction();
        try {
            foreach ($this->getSiblings() as $action) {
                if ($action->project_order > $this->project_order) {
                    $action->project_order--;
                    $action->save();
                }
            }
            $this->project_order = 0;
            $this->save();

            DB::commit();
            return true;
        } catch (\Exception) {
            DB::rollBack();
            return false;
        }
    }
    /**
     * Add an action to a new project
     *
     * Inserts the action in at the correct order and updates other
     * actions to preserve correct ordering
     *
     * @param $client_id
     * @param $project_id
     * @param $project_order
     * @return bool
     */
    public function addToOrder($client_id, $project_id, $project_order)
    {
        $this->client_id = $client_id;
        $this->project_id = $project_id;
        $this->save();

        if ($project_order == 0) {
            $this->project_order = $this->calculateProjectOrder();
        } else {
            foreach ($this->getSiblings() as $action) {
                if ($action !== $this && $action->project_order >= $project_order) {
                    $action->project_order++;
                    $action->save();
                }
            }
            $this->project_order = $project_order;
        }

        return $this->save();
    }
    /**
     * Remove the current action from the project
     *
     * Take the current action out of the project and update the project_order
     * for the remaining actions
     *
     * @return bool
     */
    public function deleteAndUpdateApplicationState()
    {
        DB::beginTransaction();
        try {
            $this->removeFromOrder();
            $this->flagged = 0;
            $this->save();
            $this->delete();

            DB::commit();
            return true;
        } catch (\Exception) {
            DB::rollBack();
            return false;
        }
    }



    // Other public methods
    /**
     * Return HTML to display an action row, with or without drag handles
     *
     * @param Request $request
     * @return View|null
     */
    public function buildActionRow(Request $request): null|View
    {
        if ($request->input('show_handles') == 1) {
            $view = 'partials.show_action';
        } else {
            $view='partials.agendaAndSearchView';
        }
        $data = view($view)->with('action', $this)
            ->with('client_id', $this->client_id)
            ->with('project_id', $this->project_id);

        return $data ?: null;
    }
    /**
     * Restore the action and correctly revert metadata
     */
    public function restoreAction(): void
    {
        $this->restore();

        // If client is deleted, set client_id to 0
        if ($this->client_id != 0 && $this->client == null) {
            $this->client_id = 0;
        }
        // If project is deleted, set project_id to 0, else set project_order
        if ($this->project_id != 0 && $this->project == null) {
            $this->project_id = 0;
        } else {
            $this->project_order = $this->calculateProjectOrder();
        }
        $this->save();
    }
    /**
     * Find the current highest project_order
     *
     * Depends on the destination project/client combination which logic
     * is used to fetch the index
     *
     * @return int
     */
    public function calculateProjectOrder(): int
    {
        $max = Action::where('client_id', $this->client_id)
            ->where('project_id', $this->project_id)
            ->where('user_id', auth()->user()->id)
            ->pluck('project_order')->max();

        return $max > 0 ? $max + 1 : 1;
    }



    // Query scopes
    /**
     * Define query scope to get all actions over a week old
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOneWeekOld(Builder $query): Builder
    {
        $aWeekAgo = Carbon::today()->subDays(7);
        return $query->where('created_at', '>', $aWeekAgo);
    }
    /**
     * Restrict results to only completed actions
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeCompleted(Builder $query): Builder
    {
        return $query->onlyTrashed();
    }
    /**
     * Restrict results to only actions completed today
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeDeletedToday(Builder $query): Builder
    {
        return $query
            ->where('deleted_at', '>', Carbon::today())
            ->orderBy('deleted_at', 'desc');
    }
    /**
     * Restrict results to only actions completed yesterday
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeDeletedYesterday(Builder $query): Builder
    {
        return $query
            ->where('deleted_at', '>', Carbon::yesterday())
            ->where('deleted_at', '<', Carbon::today())
            ->orderBy('deleted_at', 'desc');
    }
    /**
     * Restrict results to only actions completed in last week
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeDeletedLastWeek(Builder $query): Builder
    {
        return $query
            ->where('deleted_at', '>', Carbon::today()->subDays(7))
            ->where('deleted_at', '<', Carbon::yesterday())
            ->orderBy('deleted_at', 'desc');
    }
    /**
     * Restrict results to only actions completed today
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeDueToday(Builder $query): Builder
    {
        return $query->where('due', '<=', Carbon::today());
    }
    /**
     * Restrict results to only actions completed yesterday
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeDueInFuture(Builder $query): Builder
    {
        return $query
            ->where('due', '>', Carbon::today())
            ->where('due', '<', Carbon::today()->addDays(21))
            ->orderBy('due', 'asc');
    }
    /**
     * Restrict results to only actions completed in last week
     * @param Builder $query
     * @return Builder
     */
    public function scopeFlagged(Builder $query): Builder
    {
        return $query->where('flagged', 1);
    }
    /**
     * Restrict DB result set to only given client
     *
     * @param Builder $query
     * @param int|null $client_id
     * @return Builder
     */
    public function scopeWhereClientIs(Builder $query, int|null $client_id): Builder
    {
        return $query
            ->when($client_id, function ($query) use ($client_id) {
                $query = $query->where('client_id', $client_id);
            });
    }
    /**
     * Restrict DB results to those with no due date
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeNotDue(Builder $query): Builder
    {
        return $query->where('due', null);
    }
    /**
     * Only show tasks with no due date if flag is set
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeHideDueIfFlagged(Builder $query): Builder
    {
        return $query->when(Auth::user()->shouldHideDueActions(), function ($query) {
            $query->where('due', null);
        });
    }
    /**
     * Restrict DB results to only those matching the given search term
     *
     * @param Builder $query
     * @param string $searchTerm
     * @return Builder
     */
    public function scopeMatchingSearchTerm(
        Builder $query,
        string $searchTerm,
    ): Builder
    {
        return $query
            ->where(function ($query) use ($searchTerm) {
                $query->where('title', 'LIKE', "%$searchTerm%")
                    ->orWhere('notes', 'LIKE', "%$searchTerm%");
            });
    }
}
