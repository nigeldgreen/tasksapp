<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class ClientListViewComposer
{

    /**
     * Retrieve list of active clients for the logged-in user
     *
     * @param View $view
     */
    public function compose(View $view)
    {
        $active_clients = null;
        if (Auth::user()) {
            $active_clients = auth()->user()->clients()->orderBy('order')->get();
        }
        $view->with('active_clients', $active_clients);
    }
}
