<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use App\Project;

class ProjectListViewComposer
{
    /**
     * Retrieve list of projects for the logged-in user
     *
     * If we have a client set then use the client, otherwise retrieve all the
     * projects with no client set that are assigned to the logged-in user.
     *
     * @param View $view
     */
    public function compose(View $view)
    {
        $active_projects = Project::where('user_id', auth()->user()->id)->get();
        
        $view->with('active_projects', $active_projects);
    }
}
