<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAction extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'url' => 'string|nullable|between:1,255',
            'due' => 'date|nullable',
            'notes' => 'string|nullable',
            'user_id' => 'integer',
            'client_id' => 'integer|nullable',
            'project_id' => 'integer|nullable',
            'project_order' => 'integer',
            'flagged' => 'integer',
            'created_at' => 'date|nullable'
        ];
    }
}
