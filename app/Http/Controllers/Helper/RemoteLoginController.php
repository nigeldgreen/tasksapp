<?php

namespace App\Http\Controllers\Helper;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RemoteLoginController extends Controller
{
    /**
     * Log a user in from the link sent out in the daily email
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = User::where('api_token', $request['daily'])->first();
        Auth::login($user);

        return redirect()->route('home');
    }
}
