<?php

namespace App\Http\Controllers\Helper;

use App\Classes\IndexManager;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReindexActionsRequest;
use Illuminate\Http\Response;

class ReindexController extends Controller
{
    /**
     * Runs a migration to tidy up database records
     */
    public function index(ReindexActionsRequest $request): Response
    {
        $im = new IndexManager();
        $im->indexProjects();
        $im->indexActions();

        return response('', 204);
    }
}
