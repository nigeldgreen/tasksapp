<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CSRFRefreshTokenController extends Controller
{
    /**
     * Return the dynamic title input field for display
     *
     * @param Action $action
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function index()
    {
        $new_token = csrf_token();
        return response($new_token, 200);
    }
}
