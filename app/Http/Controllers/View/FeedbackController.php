<?php

namespace App\Http\Controllers\View;

use App\Feedback;
use App\Mail\FeedbackFormCompleted;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreFeedback;
use Illuminate\Support\Facades\Mail;

class FeedbackController extends Controller
{
    /**
     * Show feedback page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        return view('feedback')->with('user', auth()->user());
    }

    /**
     * Store new feedback in the database
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(StoreFeedback $request)
    {
        $feedback = new Feedback();
        $feedback->message = $request['message'];
        $feedback->user_id = auth()->user()->id;
        $feedback->save();

        Mail::to('hello@tasksapp.co.uk')->send(new FeedbackFormCompleted($feedback));

        return redirect()->route('feedback')
             ->with('status', "Thanks for your feedback!");
    }
}
