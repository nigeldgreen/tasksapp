<?php

namespace App\Http\Controllers\View;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    /**
     * Show the main application page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();
        if ($user) {
            if ($user->isValidated() != null) {
                return redirect()->route('client.index');
            } else {
                Auth::logout();
                return view('auth.not-validated');
            }
        }
        return redirect()->route('login');
    }
}
