<?php

namespace App\Http\Controllers\View;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    /**
     * Retrieve matching results and show the search page
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if (!null == $request->searchterm) {
            session(['searchterm' => $request->searchterm]);
        }
        $searchterm = session('searchterm');

        $liveactions = auth()->user()->actions()->matchingSearchTerm($searchterm)->get();
        $completedactions = auth()->user()->actions()->completed()->matchingSearchTerm($searchterm)
            ->orderBy('deleted_at', 'desc')
            ->get();

        return view('search')
            ->with('liveactions', $liveactions)
            ->with('completedactions', $completedactions)
            ->with('searchterm', $searchterm);
    }
}
