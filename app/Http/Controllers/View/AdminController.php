<?php

namespace App\Http\Controllers\View;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Get core data and show user their stats
     *
     * @return $this
     */
    public function index()
    {
        return view('admin.index')
            ->with('clients', auth()->user()->clients()->orderBy('order')->get())
            ->with('this_week', auth()->user()->getThisWeekSummary())
            ->with('all_time', auth()->user()->getAllTimeSummary());
    }

    /**
     * Update 'order' field for client list
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveclientorder(Request $request)
    {
        foreach (auth()->user()->clients as $client) {
            $client->order = $request->input("client_" . $client->id);
            $client->save();
        }
        return redirect()->route('admin.index');
    }

    /**
     * Update agenda_opt_in based on form checkbox
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateagendaoptin(Request $request)
    {
        auth()->user()->agenda_opt_in = $request->has('agenda_opt_in') ? 1 : 0;
        auth()->user()->save();

        return redirect()->back();
    }
}
