<?php

namespace App\Http\Controllers;

use App\Note;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ExportController extends Controller
{
    /**
     * Get core data and show user their stats
     *
     * @return $this
     */
    public function index()
    {
        foreach (Note::get() as $note) {
            $title = str_replace(' ', '_', $note->title);
            $title = str_replace('/', '_', $title);
            $body = $note->body;
            $handle = fopen("/text_files/" . $title . '.txt', "w+");
            fwrite($handle, $body);
            fclose($handle);
        }
    }
}
