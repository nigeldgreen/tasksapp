<?php namespace App\Http\Controllers\Client;

use App\Action;
use App\Client;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreClient;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ClientController extends Controller
{
    /**
     * Show inbox and projects/actions from all clients
     *
     * @return View
     */
    public function index()
    {
        return view('client.index')
            ->with('orphan_actions', Auth::user()->inboxActions()->get())
            ->with('orphan_projects', Auth::user()->inboxProjects())
            ->with('clients', Client::where('user_id', auth()->user()->id)->FullyStackedForMainView())
            ->with('client_id', session('client_id'));
    }

    /**
     * Show the form to create a new client
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function create(Request $request)
    {
        if ($request->isJson() || $request->ajax()) {
            return response(
                view('forms.client_create_form')->render(),
                200
            );
        } else {
            return view('client.create');
        }
    }

    /**
     * Take form data and use it to create a new client
     *
     * @param StoreClient $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreClient $request)
    {
        if ($request->isJson() || $request->ajax()) {
            $data = view('partials.show_client')->with('client', Client::create($request->all()));
            return response($data, 200);
        } else {
            $route = $request['addAnotherClient'] == "on" ? 'client.create' : 'client.index';
            return redirect()->route($route);
        }
    }

    /**
     * Show form to edit an existing client
     *
     * @param Request $request
     * @param Client $client
     * @return $this
     */
    public function edit(Request $request, Client $client)
    {
        if ($request->isJson() || $request->ajax()) {
            return response(
                view('forms.client_update_form')->with('client', $client),
                200
            );
        } else {
            return view('client.edit')->with('client', $client);
        }
    }

    /**
     * Take form data and use it to update an existing client
     *
     * @param Client $client
     * @param StoreClient $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Client $client, StoreClient $request)
    {
        $client->update($request->all());
        $client->save();

        if ($request->isJson() || $request->ajax()) {
            $data = view('partials.show_client')->with('client', $client);
            return response($data, 200);
        } else {
            return redirect()->route('client.index');
        }
    }

    /**
     * Mark an existing client as completed
     *
     * Will only run if client has no currently active projects
     *
     * @param Client $client
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Request $request, Client $client)
    {
        if ($client->hasBeenDeleted()) {
            if ($request->isJson() || $request->ajax()) {
                return response('success', 200);
            } else {
                return redirect()->route('client.index');
            }
        } else {
            if ($request->isJson() || $request->ajax()) {
                return response("Sorry, you can't delete a client if it has active projects", 403);
            } else {
                return redirect()->back()->with('status', "Can't delete while the client has active projects!");
            }
        }
    }
}
