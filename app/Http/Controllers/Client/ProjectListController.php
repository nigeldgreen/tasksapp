<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Client;
use App\Project;

class ProjectListController extends Controller
{
    /**
     * Get the list of projects for the specified client
     *
     * @param  int $client_id
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function index($client_id)
    {
        $client = Client::first();
        return response(view('partials.buildProjectDropdown')
            ->with('projects', auth()->user()->projects()->where('client_id', $client_id)->get())
            ->render(), 200);
    }
}
