<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;

class ListController extends Controller
{
    /**
     * Return a list of the Clients for the current user
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function index()
    {
        $html = view('partials.buildClientDropdown')
            ->with('clients', auth()->user()->clients)
            ->with('client_id', session('client_id', 0))
            ->render();

        return response($html, 200);
    }
}
