<?php
namespace App\Http\Controllers\Action;

use App\Action;
use App\Client;
use App\Project;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use phpDocumentor\Reflection\Types\Integer;

class CompletedController extends Controller
{
    /**
     * Show completed actions, grouped by day/week completed
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('completedaction.show', [
            'deletedtoday' => auth()->user()->actions()->completed()->deletedToday()->get(),
            'deletedyesterday' => auth()->user()->actions()->completed()->deletedYesterday()->get(),
            'deletedlastweek' => auth()->user()->actions()->completed()->deletedLastWeek()->get()
        ]);
    }

    /**
     * Mark the specified action as deleted
     *
     * @param Action $action
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function store(Action $action, Request $request)
    {
        if ($action->deleteAndUpdateApplicationState()) {
            if ($request->isJson() || $request->ajax()) {
                return response('success', 200);
            } else {
                return redirect()->back();
            }
        } else {
            if ($request->isJson() || $request->ajax()) {
                return response('failed', 409);
            } else {
                return redirect()->back()->withErrors();
            }
        }
    }

    /**
     * Restore a completed action to a client/project (i.e. 'delete' the completed action)
     *
     * Remove the deleted_at datestamp and restore an action to 'active'. If
     * the client and project have been deleted, restore to inbox
     *
     * @param int $action_id
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function delete(int $action_id, Request $request)
    {
        Action::withTrashed()->find($action_id)->restoreAction();

        if ($request->isJson() || $request->ajax()) {
            return response('success', 204);
        } else {
            return redirect()->back();
        }
    }
}
