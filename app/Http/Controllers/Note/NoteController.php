<?php

namespace App\Http\Controllers\Note;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Requests\StoreNote;
use App\Note;
use Illuminate\Http\Response;
use Illuminate\View\View;

class NoteController extends Controller
{
    /**
     * Show the main notes view with all notes visible
     *
     * @param Request $request
     * @return Factory|View
     */
    public function index(Request $request): Factory|View
    {
        $search_term = $request->input('searchterm');
        $notes = auth()->user()->notes()
            ->when($search_term != "", function($query) use ($search_term) {
                $searchterm = "%" . $search_term . "%";
                $query = $query->where('title', 'like', $searchterm)
                    ->orWhere('body', 'like', $searchterm);
            })->orderBy('updated_at', 'desc')->get();

        return view('note.index')->with([
            'notes' => $notes,
            'search_term'=> $search_term
        ]);
    }

    /**
     * Return the note as an object
     *
     * @param  Note  $note
     * @return Response
     */
    public function show(Note $note): Response
    {
        return response($note->getBodyAsHTML(), 200);
    }

    /**
     * Show the form to create a new note
     *
     * @param Request $request
     * @param int $client_id
     * @return Response|Factory|View
     */
    public function create(Request $request, $client_id = 0): Factory|Response|View
    {
        $client_id = $client_id != null ? $client_id : (session('client_id') > 0 ? session('client_id') : 0);

        if ($request->isJson() || $request->ajax()) {
            return response(view('forms.note_create_form')
                ->with('client_id', $client_id), 200);
        } else {
            return view('note.create')->with('client_id', $client_id);
        }
    }

    /**
     * Take form data and use it to create a new note
     *
     * @param StoreNote $request
     * @return RedirectResponse|Response
     */
    public function store(StoreNote $request): Response|RedirectResponse
    {
        $note = new Note($request->all());
        $note->save();

        if ($request->isJson() || $request->ajax()) {
            return response([
                'html' => view('partials.show_note')->with('note', $note)->render(),
                'note' => $note,
            ], 200);
        } else {
            return redirect()->route('note.index');
        }
    }

    /**
     * Show the form to edit an existing note
     *
     * @param Request $request
     * @param Note $note
     * @return Factory|Response|View
     */
    public function edit(Request $request, Note $note): Factory|Response|View
    {
        if ($request->isJson() || $request->ajax()) {
            return response(
                view('forms.note_update_form')
                    ->with('note', $note)
                    ->with('client_id', $note->client_id),
                200
            );
        } else {
            return view('note.edit')->with('note', $note)
                ->with('client_id', $note->client_id);
        }
    }

    /**
     * Take form data and use it to update an existing note
     *
     * @param Note $note
     * @param StoreNote $request
     *
     * @return Response|RedirectResponse
     */
    public function update(note $note, StoreNote $request): Response|RedirectResponse
    {
        $note->update($request->all());
        $note->makeOverview();
        $note->save();

        if ($request->isJson() || $request->ajax()) {
            return response([
                'html' => view('partials.show_note')->with('note', $note)->render(),
                'note' => $note,
            ], 200);
        } else {
            return redirect()->route('note.index');
        }
    }

    /**
     * Delete an existing note from ID
     *
     * @param Request $request
     *
     * @param Note $note
     * @return Response|RedirectResponse
     * @throws Exception
     */
    public function delete(Request $request, note $note): Response|RedirectResponse
    {
        $note->delete();

        if ($request->isJson() || $request->ajax()) {
            return response('', 204);
        } else {
            return redirect()->route('note.index');
        }
    }
}
