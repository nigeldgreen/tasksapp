<?php

namespace App\Http\Controllers\Auth;

use App\Classes\TokenManager;
use App\Exceptions\TokenNotValidException;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ValidateUserController extends Controller
{
    /**
     * @param Request $request
     * @param TokenManager $tm
     */
    public function index(Request $request, TokenManager $tm)
    {
        try {
            $token = $tm->validate_token_from_lookup($request->get('l'), $request->get('t'));

            User::where('email', $token->email)
                ->first()
                ->updateAndDeleteToken($token);

            return redirect()->route('client.index');
        } catch (TokenNotValidException) {
            return redirect()->route('home');
        } catch (\Exception $e) {
            //
        }
    }
}
