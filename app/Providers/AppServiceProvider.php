<?php

namespace App\Providers;

use App\Classes\HcaptchaValidationManager;
use App\Interfaces\CaptchaValidationManager;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        if ($this->app->environment() === 'production') {
//            $this->app->register(Rollbar\Laravel\RollbarServiceProvider::class);
//        }
        if ($this->app->isLocal()) {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
        $this->app->bind(CaptchaValidationManager::class, HcaptchaValidationManager::class);
    }
}
