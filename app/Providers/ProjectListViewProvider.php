<?php

namespace App\Providers;

use App\Http\ViewComposers\ProjectListViewComposer;
use Illuminate\Support\ServiceProvider;

class ProjectListViewProvider extends ServiceProvider
{
    /**
     * Define the views that the ProjectListViewComposer applies to
     *
     * This View Composer is only used in the form shown to create or edit
     * an action, so restricted to just those views.
     *
     */
    public function boot()
    {
        view()->composer([
            'action.create',
            'action.edit',
            'forms.action_create_form',
            'forms.action_update_form'
        ], ProjectListViewComposer::class);
    }
}
