<?php

namespace App\Providers;

use App\Http\ViewComposers\ClientListViewComposer;
use Illuminate\Support\ServiceProvider;

class ClientListViewProvider extends ServiceProvider
{
    /**
     * Define the views that the ClientListViewComposer applies to
     *
     * This View Composer is available to all views
     *
     */
    public function boot()
    {
        view()->composer([
            'partials.navbar',
            'partials.leftnav',
            'forms.action_create_form',
            'forms.action_update_form',
            'forms.project_create_form',
            'forms.project_update_form',
            'forms.client_create_form',
            'forms.client_update_form',
            'forms.note_create_form',
            'forms.note_update_form'
        ], ClientListViewComposer::class);
    }
}
