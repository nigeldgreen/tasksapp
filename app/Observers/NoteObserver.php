<?php

namespace App\Observers;

use App\Note;

class NoteObserver
{
    /**
     * Handle the Note "created" event.
     *
     * @param  \App\Note  $note
     * @return void
     */
    public function creating(Note $note)
    {
        if (! isset($note->user_id)) {
            $note->user_id = auth()->user()->id;
        }
        $note->makeOverview();
        $note->tags = $this->sanitise_tags($note->tags);
    }

    /**
     * Handle the Note "updated" event.
     *
     * @param  \App\Note  $note
     * @return void
     */
    public function updating(Note $note)
    {
        $note->makeOverview();
        $note->tags = $this->sanitise_tags($note->tags);
    }

    /**
     * Handle the Note "deleted" event.
     *
     * @param  \App\Note  $note
     * @return void
     */
    public function deleted(Note $note)
    {
        //
    }

    /**
     * Handle the Note "restored" event.
     *
     * @param  \App\Note  $note
     * @return void
     */
    public function restored(Note $note)
    {
        //
    }

    /**
     * Handle the Note "force deleted" event.
     *
     * @param  \App\Note  $note
     * @return void
     */
    public function forceDeleted(Note $note)
    {
        //
    }

    private function sanitise_tags($tags)
    {
        $tags = collect(preg_split("|,\s*|", $tags, -1, PREG_SPLIT_NO_EMPTY));
        return implode(',', $tags->unique()->toArray());
    }
}
