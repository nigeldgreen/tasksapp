<?php

namespace App\Observers;

use App\Client;
use Illuminate\Support\Facades\Auth;

class ClientObserver
{
    /**
     * Handle the Client "creating" event.
     *
     * @param Client $client
     * @return void
     */
    public function creating(Client $client)
    {
        $client->order = Client::getHighestOrder() + 1;
        if (! isset($client->user_id)) {
            $client->user_id = Auth::user()->id;
        }
    }

    /**
     * Handle the Client "updated" event.
     *
     * @param Client $client
     * @return void
     */
    public function updated(Client $client)
    {
        //
    }

    /**
     * Handle the Client "deleted" event.
     *
     * @param Client $client
     * @return void
     */
    public function deleted(Client $client)
    {
        //
    }

    /**
     * Handle the Client "restored" event.
     *
     * @param Client $client
     * @return void
     */
    public function restored(Client $client)
    {
        //
    }

    /**
     * Handle the Client "force deleted" event.
     *
     * @param Client $client
     * @return void
     */
    public function forceDeleted(Client $client)
    {
        //
    }
}
