<?php

namespace App\Observers;

use App\Action;
use Illuminate\Support\Facades\Auth;

class ActionObserver
{
    /**
     * Handle the Action "creating" event.
     *
     * Make sure that all new actions get their correct project order and are linked to the user
     * making the request.
     *
     * @param Action $action
     * @return void
     */
    public function creating(Action $action)
    {
        if (! isset($action->project_order)) {
            $action->project_order = $action->calculateProjectOrder();
        }

        if (! isset($action->user_id)) {
            $action->user_id = Auth::user()->id;
        }



        Auth::user()->logActivity();
    }

    /**
     * Handle the Action "updated" event.
     *
     * @param Action $action
     * @return void
     */
    public function updated(Action $action)
    {
        Auth::user()->logActivity();
    }

    /**
     * Handle the Action "deleted" event.
     *
     * @param Action $action
     * @return void
     */
    public function deleted(Action $action)
    {
        //
    }

    /**
     * Handle the Action "restored" event.
     *
     * @param Action $action
     * @return void
     */
    public function restored(Action $action)
    {
        //
    }

    /**
     * Handle the Action "force deleted" event.
     *
     * @param Action $action
     * @return void
     */
    public function forceDeleted(Action $action)
    {
        //
    }
}
