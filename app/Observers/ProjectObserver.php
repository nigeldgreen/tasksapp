<?php

namespace App\Observers;

use App\Project;
use Illuminate\Support\Facades\Auth;

class ProjectObserver
{
    /**
     * Handle the Project "creating" event.
     *
     * @param Project $project
     * @return void
     */
    public function creating(Project $project)
    {
        if ($project->client !== null) {
            $project->client_order = $project->calculateClientOrder();
        }
        if (! isset($project->user_id)) {
            $project->user_id = Auth::user()->id;
        }
    }

    /**
     * Handle the Project "updated" event.
     *
     * @param Project $project
     * @return void
     */
    public function updated(Project $project)
    {
        //
    }

    /**
     * Handle the Project "deleted" event.
     *
     * @param Project $project
     * @return void
     */
    public function deleted(Project $project)
    {
        //
    }

    /**
     * Handle the Project "restored" event.
     *
     * @param Project $project
     * @return void
     */
    public function restored(Project $project)
    {
        //
    }

    /**
     * Handle the Project "force deleted" event.
     *
     * @param Project $project
     * @return void
     */
    public function forceDeleted(Project $project)
    {
        //
    }
}
