<?php

namespace App\Classes;

use App\Interfaces\CaptchaValidationManager;

class HcaptchaValidationManager implements CaptchaValidationManager {
    /**
     * @param string $code
     * @return bool
     */
    public function validate_captcha(string $code): bool
    {
        $data = [
            'secret' => config('app.hcaptcha_secret'),
            'response' => $code,
        ];

        $verify = curl_init();
        curl_setopt($verify, CURLOPT_URL, "https://hcaptcha.com/siteverify");
        curl_setopt($verify, CURLOPT_POST, true);
        curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($verify);
        $responseData = json_decode($response);

        return $responseData->success;
    }
}
