<?php

namespace App\Classes;

use App\Client;
use App\Project;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class IndexManager {
    public Collection $clients;
    public Collection $projects;
    public Collection $inbox;

    public function __construct() {
        $this->clients = Client::with(['projects' => function ($query) {
            $query->orderBy('client_order');
        }])
            ->where('user_id', Auth::user()->id)
            ->get();
        $this->projects = Project::with(['actions' => function ($query) {
            $query->orderBy('project_order');
        }])
            ->where('user_id', Auth::user()->id)
            ->get();
        $this->inbox = Auth::user()->inboxActions()->get();
    }

    public function indexProjects() {
        $this->clients->each(function ($client) {
            $i = 1;
            $client->projects->each(function ($project) use (&$i) {
                $project->client_order = $i;
                $project->save();
                $i++;
            });
        });
    }

    public function indexActions() {
        // Re-index actions that are contained directly in clients
        $i = 1;
        $this->inbox
            ->each(function ($action) use (&$i) {
                $action->project_order = $i;
                $action->save();
                $i++;
            });

        // Re-index actions that are contained directly in clients
        $this->clients
            ->each(function ($client) {
                $i = 1;
                $client->actions->filter(function ($action) use ($client) {
                    return $action->project_id == 0;
                })->each(function ($action) use (&$i) {
                    $action->project_order = $i;
                    $action->save();
                    $i++;
                });
            });

        // Re-index actions that are within projects
        $this->projects->each(function ($project) {
            $i = 1;
            $project->actions->each(function ($action) use (&$i) {
                $action->project_order = $i;
                $action->save();
                $i++;
            });
        });
    }
}
