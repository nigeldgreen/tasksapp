<?php

namespace App\Interfaces;

interface CaptchaValidationManager
{
    /**
     * Method to implement that will validate a capture for form
     *
     * @param string $code
     * @return bool
     */
    public function validate_captcha(string $code): bool;
}