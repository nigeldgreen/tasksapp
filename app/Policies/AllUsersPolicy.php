<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AllUsersPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return whether a user is assigned super user status or not
     *
     * Used on super-admin functions to check that user has correct privilege level
     *
     * @param $user
     * @return mixed
     */
    public function viewAllUsers($user)
    {
        return $user->su;
    }
}
