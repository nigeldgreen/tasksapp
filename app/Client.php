<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Tests\Unit\ActionTest;

class Client extends Model
{
    use SoftDeletes, HasFactory;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    protected $fillable = [
        'title',
        'notes',
        'created_at',
    ];
    protected $casts = [
        'order' => 'integer',
        'user_id' => 'integer',
    ];

    // Other public methods

    /**
     * Retrieve all actions for the client that have no project set
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getNonProjectActions()
    {
        return $this->actions->where('project_id', 0);
    }

    /**
     * Check if the active client has any projects still live
     *
     * @return bool
     * @throws \Exception
     */
    public function hasBeenDeleted()
    {
        if ($this->projects()->count() || $this->actions()->count()) {
            return false;
        } else {
            if ($this->id == session('client_id')) {
                session(['client_id' => null]);
            }
            $this->order = null;
            $this->save();
            $this->delete();
            return true;
        }
    }

    /**
     * Find the current highest order
     *
     * @return int
     */
    public static function getHighestOrder()
    {
        $current_max = DB::table('clients')
            ->where('user_id', auth()->user()->id)
            ->max('order');

        return $current_max > 0 ? $current_max : 0;
    }



    // Query scopes
    public function scopeOneWeekOld($query)
    {
        $aWeekAgo = Carbon::today()->subDays(7);
        return $query->where('created_at', '>', $aWeekAgo);
    }

    public function scopeWhereClientIs($query, $client_id)
    {
        return $query->when($client_id != null, function ($query) use ($client_id) {
            $query = $query->where('id', $client_id);
        });
    }

    public function scopeFullyStackedForMainView($query)
    {
        return $query->with(['projects' => function ($query) {
            $query->with(['actions' => function ($query) {
                $query->HideDueIfFlagged()->orderBy('project_order');
            }])->orderBy('client_order');
        }])
            ->with(['actions' => function ($query) {
                $query->where('project_id', 0)
                    ->HideDueIfFlagged()
                    ->orderBy('project_order');
            }])
            ->WhereClientIs(session('client_id'))
            ->orderBy('order', 'asc')
            ->get();
    }



    // Define relationships
    /**
     * Get all of the projects for the client.
     */
    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    /**
     * Get all of the actions for the client.
     */
    public function actions()
    {
        return $this->hasMany(Action::class);
    }

    /**
     * Get all of the notes for the client.
     */
    public function notes()
    {
        return $this->hasMany(Note::class);
    }

    /**
     * Get the user that the client belongs to
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
