<?php

namespace Tests\Unit;

use App\User;
use App\Client;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ClientTest extends TestCase
{
    use DatabaseMigrations;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }

    /** @test */
    public function client_order_is_correct_as_projects_are_added()
    {
        $this->assertEquals(0, Client::getHighestOrder());

        Client::factory()->create();
        $this->assertEquals(1, Client::getHighestOrder());

        Client::factory(3)->create();

        $this->assertEquals(4, Client::getHighestOrder());
    }
}
