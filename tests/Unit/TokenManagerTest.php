<?php

namespace Tests\Unit;

use App\Classes\TokenManager;
use App\Exceptions\TokenNotValidException;
use App\RegistrationToken;
use App\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TokenManagerTest extends TestCase
{
    use DatabaseMigrations;

    private User $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create([
            'email' => 'test@token.test'
        ]);
    }

    /** @test */
    public function can_create_new_token()
    {
        $tm = new TokenManager();
        $token_creds = $tm->create_token($this->user);

        $this->assertEquals(24, strlen($token_creds['lookup']));
        $this->assertEquals(64, strlen($token_creds['token']));

        $stored_tm = RegistrationToken::first();

        $this->assertEquals('test@token.test', $stored_tm->email);
        $this->assertEquals(Carbon::now()->addDays(2)->startOfDay(), $stored_tm->expiry->startOfDay());
        $this->assertEquals(24, strlen($stored_tm->lookup));
        $this->assertEquals(60, strlen($stored_tm->token));
    }

    /** @test
     * @throws TokenNotValidException
     */
    public function can_validate_token_from_correct_lookup()
    {
        $tm = new TokenManager();
        $token_creds = $tm->create_token($this->user);

         $this->assertEquals(
             'test@token.test',
             $tm->validate_token_from_lookup($token_creds['lookup'], $token_creds['token'])->email
         );
    }

    /** @test */
    public function cannot_validate_token_from_invalid_lookup()
    {
        $tm = new TokenManager();
        $token_creds = $tm->create_token($this->user);

        $this->expectException(TokenNotValidException::class);
        $tm->validate_token_from_lookup('fake_lookup', $token_creds['token']);
    }

    /** @test */
    public function cannot_validate_token_from_invalid_plain_token()
    {
        $tm = new TokenManager();
        $token_creds = $tm->create_token($this->user);

        $this->expectException(TokenNotValidException::class);
        $tm->validate_token_from_lookup($token_creds['lookup'], 'fake_token');
    }

    /** @test */
    public function cannot_validate_expired_token()
    {
        $tm = new TokenManager();
        $token_creds = $tm->create_token($this->user);

        $token = RegistrationToken::first();
        $token->update([
            'expiry' => Carbon::now()->subDays(7),
        ]);

        $this->expectException(TokenNotValidException::class);
        $tm->validate_token_from_lookup($token_creds['lookup'], $token_creds['token']);
    }
}
