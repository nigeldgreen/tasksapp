<?php

namespace Tests\Unit;

use App\Note;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NoteTest extends TestCase
{
    use DatabaseMigrations;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }

    /** @test */
    public function can_see_correct_html_for_tags()
    {
        $tags = '<div class="tags-container"><p>Tags: </p><ul class="tags-list"><li class=\'tags-list-item\'>test 1</li><li class=\'tags-list-item\'>test 2</li><li class=\'tags-list-item\'>test 3</li></ul></div>';
        $note = Note::factory()->create([
            'tags' => "test 1, test 2, test 3",
        ]);
        $this->assertEquals($note->getTagsAsHTML(), $tags);
        
    }

    /** @test */
    public function correct_message_shown_if_no_tags_defined()
    {
        $note = Note::factory()->create([ ]);
        $this->assertEquals($note->getTagsAsHTML(), "<div class=\"tags-container\"><p>Tags: none defined</p></div>");
        
    }
}
