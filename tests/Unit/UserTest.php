<?php

namespace Tests\Unit;

use App\User;
use App\Action;
use Carbon\Carbon;
use Database\Seeds\UserTestSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp():void
    {
        parent::setUp();
        $this->be(User::factory()->create([ 'name' => 'Smew Watters' ]));
        $this->artisan('db:seed', [ '--class' => UserTestSeeder::class ]);
    }

    /** @test */
    public function can_log_activity_for_user()
    {
        auth()->user()->logActivity();

        $this->assertEquals(
            Carbon::today()->format('Y-m-d'),
            auth()->user()->lastActive->format('Y-m-d')
        );
    }

    /** @test */
    public function can_get_inbox_projects()
    {
        $projects = auth()->user()->inboxProjects()->get();

        $this->assertEquals(2, $projects->count());
        $this->assertArrayHasKey('title', $projects->toArray()[0]);
        $this->assertArrayHasKey('title', $projects->toArray()[1]);
//        $this->assertArraySubset(['title' => 'Test project 2'], $projects->toArray()[0]);
//        $this->assertArraySubset(['title' => 'Test project 3'], $projects->toArray()[1]);
    }

    /** @test */
    public function can_get_inbox_actions()
    {
        $actions = auth()->user()->inboxActions()->get();

        $this->assertEquals(2, $actions->count());
        $this->assertArrayHasKey('title', $actions->toArray()[0]);
        $this->assertArrayHasKey('title', $actions->toArray()[1]);
//        $this->assertArraySubset(['title' => 'Test action 2'], $actions->toArray()[0]);
//        $this->assertArraySubset(['title' => 'Test action 3'], $actions->toArray()[1]);
    }

    /** @test */
    public function can_get_first_name()
    {
        $this->assertEquals('Smew', auth()->user()->fresh()->getFirstName());
    }

    /** @test */
    public function can_get_actions_deleted_today()
    {
        $this->assertEquals(0, auth()->user()->actions()->completed()->deletedToday()->count());

        $action = Action::find(1);
        $action->delete();

        $actions = auth()->user()->actions()->completed()->deletedToday();

        $this->assertEquals(1, $actions->count());
        $this->assertEquals('Test action 1', $actions->get()[0]->title);
    }

    /** @test */
    public function can_get_actions_deleted_yesterday()
    {
        $this->assertEquals(0, auth()->user()->actions()->completed()->deletedYesterday()->count());

        $action = Action::find(2);
        $action->deleted_at = Carbon::now()->subDay()->format('Y-m-d H:i:s');
        $action->save();

        $actions = auth()->user()->actions()->completed()->deletedYesterday();

        $this->assertEquals(1, $actions->count());
        $this->assertEquals('Test action 2', $actions->get()[0]->title);
    }

    /** @test */
    public function can_get_actions_deleted_last_week()
    {
        $this->assertEquals(0, auth()->user()->actions()->completed()->deletedLastWeek()->count());

        $action = Action::find(3);
        $action->deleted_at = Carbon::now()->subDays(5)->format('Y-m-d H:i:s');
        $action->save();

        $actions = auth()->user()->actions()->completed()->deletedLastWeek();

        $this->assertEquals(1, $actions->count());
        $this->assertEquals('Test action 3', $actions->get()[0]->title);
    }

    /** @test */
    public function new_user_is_not_validated()
    {
        $user = User::factory()->create();
        $this->assertFalse($user->isValidated());
    }

    /** @test */
    public function validated_user_returns_as_validated()
    {
        $user = User::factory()->create([
            'validated_at' => Carbon::now(),
        ]);
        $this->assertTrue($user->isValidated());
    }

    /** @test */
    public function user_can_flag_to_hide_due_actions_in_planning_view()
    {
        $user = User::factory()->create();

        $user->hideDueActions();

        $this->assertTrue($user->fresh()->shouldHideDueActions());
    }
}
