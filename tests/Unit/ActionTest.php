<?php

namespace Tests\Unit;

use App\Client;
use App\User;
use App\Action;
use App\Project;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Mockery;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ActionTest extends TestCase
{
    use DatabaseMigrations;

    private Authenticatable|Model|Collection $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }

    /** @test */
    public function new_action_created_with_correct_attributes()
    {
        $action = Action::factory()->create();

        $this->assertTrue($action->title === 'Fake action');
        $this->assertTrue($action->user_id === $this->user->id);
        $this->assertTrue($action->project_order === 1);
    }
    /** @test */
    public function highest_project_order_is_one_if_no_actions_exist()
    {
        $action = Action::factory()->create();
        route('login');
        $this->assertEquals(1, $action->project_order);
    }
    /** @test */
    public function highest_project_order_is_returned_when_actions_exist()
    {
        $actions = Action::factory(3)->create();

        $this->assertEquals(3, $actions[2]->project_order);
    }
    /** @test */
    public function correct_project_order_is_set_for_multiple_actions()
    {
        // Arrange
        Action::factory(5)->create([
            'client_id' => 1,
            'project_id' => 1
        ]);

        // Act
        $actions_fromdb = Action::get();

        // Assert
        $this->assertEquals(1, $actions_fromdb[0]->project_order);
        $this->assertEquals(2, $actions_fromdb[1]->project_order);
        $this->assertEquals(3, $actions_fromdb[2]->project_order);
        $this->assertEquals(4, $actions_fromdb[3]->project_order);
        $this->assertEquals(5, $actions_fromdb[4]->project_order);
    }
    /** @test */
    public function can_get_project_title_for_action()
    {
        // Arrange
        $project = Project::factory()->create(['title' => 'This is not a project test']);
        $action = Action::factory()->create(['project_id' => $project->id]);

        // Assert
        $this->assertEquals('This is not a project test', $action->getProjectTitle());
    }

    /** @test */
    public function can_get_flagged_status_as_text()
    {
        // ASSEMBLE
        $action1 = Action::factory()->make([ 'flagged' => true ]);
        $action2 = Action::factory()->make([ 'flagged' => false ]);

        $this->assertEquals("set", $action1->getFlaggedStatus());
        $this->assertEquals("unset", $action2->getFlaggedStatus());
    }
    /** @test */
    public function delete_action_success_returns_true()
    {
        $action = Action::factory()->create();

        $this->assertTrue($action->deleteAndUpdateApplicationState());
    }
    /** @test */
    public function delete_action_failure_returns_false()
    {
        $this->action = Mockery::mock('App\Action')->makePartial();
        $this->action->shouldReceive('removeFromOrder')->once()->andThrow('\Exception');

        $this->assertFalse($this->action->deleteAndUpdateApplicationState());
    }
    /** @test */
    public function can_see_action_location()
    {
        Client::factory()->create([
            'title' => 'Test client',
        ]);
        Project::factory()->create([
            'title' => 'Test project',
        ]);
        $action = Action::factory()->create([
            'title' => 'Preview action test',
            'client_id' => 1,
            'project_id' => 1,
            'flagged' => 1,
        ]);

        $this->assertEquals($action->getLocation(), 'Test client / Test project');
    }
    /** @test */
    public function can_see_action_location_if_in_inbox()
    {
        $action = Action::factory()->create([
            'title' => 'Preview action test',
            'flagged' => 1,
        ]);

        $this->assertEquals($action->getLocation(), 'Inbox');
    }

    /** @test */
    public function can_scope_actions_to_only_those_with_no_due_date()
    {
        Action::factory(10)->create();
        $this->assertEquals(10, Action::count());

        Action::find(3)->update([ 'due' => Carbon::now() ]);
        Action::find(5)->update([ 'due' => Carbon::now() ]);
        Action::find(9)->update([ 'due' => Carbon::now() ]);

        $this->user->hideDueActions();
        $this->user->fresh();

        $this->assertEquals(7, Action::HideDueIfFlagged()->count());
    }

    /** @test */
    public function can_correctly_check_if_action_is_moving_on_update()
    {
        $action = Action::factory()->create([
            'client_id' => 1,
            'project_id' => 1,
            'project_order' => 1,
        ]);

        $this->assertTrue($action->shouldMoveAction([
            'client_id' => 3,
            'project_id' => 1,
            'project_order' => 1,
        ]));
        $this->assertTrue($action->shouldMoveAction([
            'client_id' => 1,
            'project_id' => 3,
            'project_order' => 1,
        ]));
        $this->assertTrue($action->shouldMoveAction([
            'client_id' => 1,
            'project_id' => 1,
            'project_order' => 3,
        ]));
        $this->assertTrue($action->shouldMoveAction([
            'client_id' => 3,
            'project_id' => 3,
            'project_order' => 3,
        ]));
    }
}
