<?php

namespace Tests\Unit;

use App\User;
use App\Action;
use App\Project;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CSRFRefreshTest extends TestCase
{
    use DatabaseMigrations;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->be(User::factory()->create());
    }

    /** @test */
    public function controller_can_generate_new_csrf_token()
    {
        $response = $this->call("GET", "/refreshcsrf");
        $response->assertStatus(200);
        $this->assertEquals(40, strlen($response->content()));
    }
}
