<?php

namespace Tests\Feature;

use App\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function new_user_has_no_successful_logins()
    {
        $user = User::factory()->create();

        $this->assertNotNull($user->fresh()->totalLogins);
        $this->assertEquals(0, $user->fresh()->totalLogins);
    }

    /** @test */
    public function invalid_user_details_get_login_failure()
    {
        $this->withExceptionHandling();
        $user = User::factory()->create([
            'email' => 'smew@watters.com'
        ]);

        $response = $this->call('POST', '/login', [
            'email' => 'smew@watters.com',
            'password' => 'thisisthewrongpassword'
        ]);

        $response->assertRedirect('/');
        $this->assertEquals(0, $user->fresh()->totalLogins);
        $this->assertNull($user->fresh()->lastLoggedInOn);
    }

    /** @test */
    public function user_last_login_is_updated_on_login()
    {
        $user = User::factory()->create([
            'email' => 'smew@watters.com'
        ]);

        $response = $this->call('POST', '/login', [
            'email' => 'smew@watters.com',
            'password' => 'secret'
        ]);

        $response->assertStatus(302);
        $this->assertEquals(1, $user->fresh()->totalLogins);
        $this->assertEquals(Carbon::now()->startOfDay(), Carbon::parse($user->fresh()->lastLoggedInOn)->startOfDay());
    }

    /** @test */
    public function user_redirected_to_login_page_if_not_logged_in()
    {
        $response = $this->call('GET', '/');

        $response->assertRedirect('/login');
    }

    /** @test */
    public function user_redirected_to_home_page_if_logged_in()
    {
        $this->be(User::factory()->create([
            'validated_at' => Carbon::now(),
        ]));
        $response = $this->call('GET', '/');

        $response->assertRedirect('/client');
    }
}
