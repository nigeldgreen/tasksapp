<?php

namespace Tests\Feature\Client;

use App\User;
use App\Action;
use App\Client;
use App\Project;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ShowTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }
    /** @test */
    public function main_view_shows_correctly_for_all_clients()
    {
        $client1 = Client::factory()->create([ 'title' => 'Test client 1' ]);
        $client2 = Client::factory()->create([ 'title' => 'Test client 2' ]);
        $project1 = Project::factory()->create([ 'title' => 'Test project 1', 'client_id' => $client1->id ]);
        $project2 = Project::factory()->create([ 'title' => 'Test project 2', 'client_id' => $client2->id ]);
        Action::factory()->create([
            'title' => 'This action is in the inbox'
        ]);
        Action::factory()->create([
            'title' => 'This action is in client 1 with no project',
            'client_id' => $client1->id
        ]);
        Action::factory()->create([
            'title' => 'This action is in client 1, project 1',
            'client_id' => $client1->id,
            'project_id' => $project1->id
        ]);
        Action::factory()->create([
            'title' => 'This action is in client 2 with no project',
            'client_id' => $client2->id
        ]);
        Action::factory()->create([
            'title' => 'This action is in client 2, project 2',
            'client_id' => $client2->id,
            'project_id' => $project2->id
        ]);

        $response = $this->json('Get', '/client');

        $response->assertSeeInOrder([
            '<span class="client-title is-open">Inbox</span>',
            'This action is in the inbox',
            '<span class="client-title">Test client 1    </span>',
            'This action is in client 1 with no project',
            '<span class="project-title is-open">Test project 1 </span>',
            'This action is in client 1, project 1',
            '<span class="client-title">Test client 2    </span>',
            'This action is in client 2 with no project',
            '<span class="project-title is-open">Test project 2 </span>',
            'This action is in client 2, project 2',
        ], false);
    }
    /** @test */
    public function main_view_filtered_correctly_if_client_set_in_session()
    {
        $client1 = Client::factory()->create([ 'title' => 'Test client 1' ]);
        $client2 = Client::factory()->create([ 'title' => 'Test client 2' ]);
        $project1 = Project::factory()->create([ 'title' => 'Test project 1', 'client_id' => $client1->id ]);
        $project2 = Project::factory()->create([ 'title' => 'Test project 2', 'client_id' => $client2->id ]);
        Action::factory()->create([
            'title' => 'This action is in the inbox'
        ]);
        Action::factory()->create([
            'title' => 'This action is in client 1 with no project',
            'client_id' => $client1->id
        ]);
        Action::factory()->create([
            'title' => 'This action is in client 1, project 1',
            'client_id' => $client1->id,
            'project_id' => $project1->id
        ]);
        Action::factory()->create([
            'title' => 'This action is in client 2 with no project',
            'client_id' => $client2->id
        ]);
        Action::factory()->create([
            'title' => 'This action is in client 2, project 2',
            'client_id' => $client2->id,
            'project_id' => $project2->id
        ]);

        session([ 'client_id' => $client2->id ]);
        $response = $this->json('Get', '/client');

        $response->assertSeeInOrder([
            '<span class="client-title">Test client 2    </span>',
            'This action is in client 2 with no project',
            '<span class="project-title is-open">Test project 2 </span>',
            'This action is in client 2, project 2',
        ], false);
        $response->assertDontSee('<span class="client-title">Inbox    </span>', false);
        $response->assertDontSee('<span class="client-title">Test client 1    </span>', false);
        $response->assertDontSee('This action is in client 1 with no project', false);
        $response->assertDontSee('This action is in client 1, project 1', false);
    }
    /** @test */
    public function can_get_correct_list_of_clients_for_user()
    {
        $user2 = User::factory()->create();
        $client1 = Client::factory()->create([ 'title' => 'Test client 1' ]);
        $client2 = Client::factory()->create([ 'title' => 'Test client 2' ]);
        $client3 = Client::factory()->create([ 'title' => 'Test client 3' ]);
        $client4 = Client::factory()->create([ 'title' => 'Test client 4' ]);

        $client3->user_id = $user2->id;
        $client3->save();
        $client4->user_id = $user2->id;
        $client4->save();

        $response = $this->json('GET', '/clientlist');

        $response->assertSeeInOrder([
            '<option value=0 selected>-- show all --</option>',
            '<option value="' . $client1->id . '">' . $client1->title . '</option>',
            '<option value="' . $client2->id . '">' . $client2->title . '</option>',
        ], false);
        $response->assertDontSee('Test client 3');
        $response->assertDontSee('Test client 4');
    }
    /** @test */
    public function can_get_correct_list_of_clients_with_session_client_selected()
    {
        $user2 = User::factory()->create();
        $client1 = Client::factory()->create([ 'title' => 'Test client 1' ]);
        $client2 = Client::factory()->create([ 'title' => 'Test client 2' ]);
        $client3 = Client::factory()->create([ 'title' => 'Test client 3' ]);
        $client4 = Client::factory()->create([ 'title' => 'Test client 4' ]);

        $client3->user_id = $user2->id;
        $client3->save();
        $client4->user_id = $user2->id;
        $client4->save();

        session(['client_id' => $client2->id]);

        $response = $this->json('GET', '/clientlist');

        $response->assertSeeInOrder([
            '<option value=0>-- show all --</option>',
            '<option value="' . $client1->id . '">' . $client1->title . '</option>',
            '<option value="' . $client2->id . '" selected>' . $client2->title . '</option>',
        ], false);
        $response->assertDontSee('Test client 3');
        $response->assertDontSee('Test client 4');
    }
    /** @test */
    public function can_get_correct_list_of_projects_for_client()
    {
        $this->be($this->user);
        $clients = Client::factory(2)->create();
        $project1 = Project::factory()->create([
            'client_id' => $clients[0]->id,
            'title' => 'project 1'
        ]);
        $project2 = Project::factory()->create([
            'client_id' => $clients[0]->id,
            'title' => 'project 2'
        ]);
        $project3 = Project::factory()->create([
            'client_id' => $clients[1]->id,
            'title' => 'project 3'
        ]);

        $response = $this->json('get', "/client/getProjects/" . $clients[0]->id);

        $response->assertSee("<option value=" . $project1->id . ">project 1</option>", false);
        $response->assertSee("<option value=" . $project2->id . ">project 2</option>", false);
        $response->assertDontSee("<option value=" . $project3->id . ">project 3</option>");

        $response = $this->json('get', "/client/getProjects/" . $clients[1]->id);

        $response->assertSee("<option value=" . $project3->id . ">project 3</option>", false);
        $response->assertDontSee("<option value=" . $project1->id . ">project 1</option>");
    }
}
