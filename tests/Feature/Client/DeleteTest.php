<?php

namespace Tests\Feature\Client;

use App\User;
use App\Action;
use App\Client;
use App\Project;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DeleteTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }
    /** @test */
    public function client_can_be_deleted_via_ajax()
    {
        $client = Client::factory()->create();
        session([ 'client_id' => $client->id ]);

        $this->assertEquals(1, Client::count());
        $json_response = $this->json('DELETE', '/client/delete/' . $client->id);

        $json_response->assertStatus(200);
        $this->assertEquals(0, Client::count());
        $this->assertNull(Client::withTrashed()->find($client->id)->order);
        $this->assertNull(session('client_id'));
    }
    /** @test */
    public function client_can_be_deleted_via_http()
    {
        // ASSEMBLE
        $client = Client::factory()->create();

        // ACT
        $this->assertEquals(1, Client::count());
        $json_response = $this->call('DELETE', '/client/delete/' . $client->id);

        // ASSERT
        $json_response->assertRedirect('/client');
        $this->assertEquals(0, Client::count());
        $this->assertNull(Client::withTrashed()->find($client->id)->order);
    }
    /** @test */
    public function error_received_on_delete_if_client_has_projects()
    {
        // ASSEMBLE
        $client = Client::factory()->create();
        Project::factory(4)->create([ 'client_id' => $client->id ]);

        // ACT
        $json_response = $this->json('DELETE', '/client/delete/' . $client->id);

        // ASSERT
        $json_response->assertStatus(403);
        $this->assertEquals(1, Client::count());
        $this->assertEquals(1, $client->order);
    }
}
