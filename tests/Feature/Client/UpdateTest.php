<?php

namespace Tests\Feature\Client;

use App\User;
use App\Action;
use App\Client;
use App\Project;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UpdateTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }
    /** @test */
    public function client_is_updated_via_ajax_request()
    {
        $client = Client::factory()->create();

        $response = $this->json('PATCH', '/client/' . $client->id, [
            'title' => 'This bad boy has been updated',
        ]);
        $response->assertStatus(200);
        $this->assertEquals('This bad boy has been updated', $client->fresh()->title);
    }
    /** @test */
    public function client_fails_ajax_update_if_no_title_is_present()
    {
        $this->withExceptionHandling();
        $client = Client::factory()->create();

        $response = $this->json('PATCH', '/client/' . $client->id);

        $response->assertStatus(422);
        $this->assertEquals(Client::find(1)->title, $client->fresh()->title);
    }
    /** @test */
    public function client_is_updated_via_http_request()
    {
        $client = Client::factory()->create();

        $response = $this->call('PATCH', '/client/' . $client->id, [
            'title' => 'This bad boy has been updated',
        ]);
        $response->assertRedirect('/client');
        $this->assertEquals('This bad boy has been updated', $client->fresh()->title);
    }
}
