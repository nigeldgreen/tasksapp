<?php

namespace Tests\Feature\Client;

use App\User;
use App\Action;
use App\Client;
use App\Project;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreateTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }
    /** @test */
    public function client_order_is_correct_for_first_client()
    {
        $this->json('POST', '/client', [
            'title' => 'Test test test',
            'notes' => 'Lots of notes',
        ]);
        $client = Client::orderBy('created_at', 'desc')->first();

        $this->assertEquals(1, $client->order);
    }
    /** @test */
    public function client_order_is_correct_for_subsequent_clients()
    {
        $clients = Client::factory(3)->create();
        $this->assertEquals(1, $clients[0]->order);
        $this->assertEquals(2, $clients[1]->order);
        $this->assertEquals(3, $clients[2]->order);

        $this->json('POST', '/client', [
            'title' => 'Test test test',
            'notes' => 'Lots of notes',
        ]);
        $client = Client::where('user_id', $this->user->id)->orderBy('id', 'desc')->first();

        $this->assertEquals(4, $client->order);
    }
    /** @test */
    public function correct_response_received_for_client_create()
    {
        $json_response = $this->json('POST', '/client', [
            'title' => 'test client',
            'notes' => '',
        ]);
        $static_response = $this->call('POST', '/client', [
            'title' => 'test client',
            'notes' => '',
        ]);

        $json_response->assertStatus(200);
        $static_response->assertRedirect('/client');
    }
    /** @test */
    public function client_create_fails_if_data_is_incorrect()
    {
        $this->withExceptionHandling();

        $response = $this->json('POST', '/client');

        $response->assertStatus(422);
    }
}
