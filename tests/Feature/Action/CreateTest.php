<?php

namespace Tests\Feature\Action;

use App\User;
use App\Action;
use App\Project;
use Database\Seeds\ActionCreateTestSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
    }

    /** @test */
    public function unauthenticated_user_gets_redirected_to_login_page()
    {
        $this->withExceptionHandling();
        $response = $this->get('/myday');
        $response->assertRedirect(config('app.url') . "/login");
    }
    /** @test */
    public function authenticated_user_sees_main_home_page()
    {
        $this->be($this->user);
        $response = $this->get('/client');
        $response->assertSeeText("Inbox");
    }
    /** @test */
    public function new_action_appears_on_home_page()
    {
        $this->be($this->user);
        Action::factory()->create([
            'user_id' => $this->user->id,
            'title' => 'Should see this on the home page'
        ]);
        $response = $this->get('/client');

        $response->assertSeeText('Should see this on the home page');
    }
    /** @test */
    public function can_add_action_with_post_request()
    {
        $this->be($this->user);
        $response = $this->post('/action', [
            'title' => 'test the action form',
            'user_id' => $this->user->id
        ]);

        $action = Action::first();

        $response->assertRedirect();
        $this->assertEquals('test the action form', $action->title);
    }
    /** @test */
    public function can_add_action_with_ajax_post_request()
    {
        $this->be($this->user);
        $response = $this->json('POST', '/action', [
            'title' => 'store a test action with AJAX form',
            'user_id' => $this->user->id,
        ]);
        $action = Action::first();

        $response
            ->assertStatus(201)
            ->assertJson([
                'title' => 'store a test action with AJAX form',
            ]);
        $this->assertEquals('store a test action with AJAX form', $action->title);
    }
    /** @test */
    public function no_drag_handles_shown_if_not_requested()
    {
        // Arrange
        $this->be($this->user);
        $project = Project::factory()->create();
        $action = Action::factory()->create([
            'title' => 'Testing the action.show action',
            'notes' => 'There are copious notes for this action',
            'client_id' => 1,
            'project_id' => $project->id
        ]);

        // Act
        $response = $this->json('GET', '/action/' . $action->id);

        $response
            ->assertSeeInOrder([
                '<span class="action-title-project">',
                '[ / ' . $action->getProjectTitle() . ']',
                $action->title,
            ], false);
    }
    /** @test */
    public function drag_handles_shown_if_requested()
    {
        // Arrange
        $this->be($this->user);
        Project::factory()->create();
        $action = Action::factory()->create([
            'title' => 'Testing the action.show action',
            'notes' => 'There are copious notes for this action',
            'client_id' => 1,
            'project_id' => 1
        ]);

        // Act
        $response = $this->json('GET', '/action/' . $action->id, [
            'show_handles' => 1
        ]);

        // Assert<span class="action-title-project">[Fake project]</span>
        $response
            ->assertSee('<div class="action-drag-handle">&nbsp;', false)
            ->assertSee('<span class="action-title-text"', false)
            ->assertSee($action->title, false);
    }
    /** @test */
    public function activity_logged_when_creating_an_action()
    {
        $this->be($this->user);
        $this->assertNull($this->user->lastActive);

        $response = $this->json('POST', '/action', [
            'title' => 'store a test action with AJAX form',
            'user_id' => $this->user->id,
        ]);


        $this->assertNotNull($this->user->lastActive);
    }
    /** @test */
    public function correct_project_order_for_new_action()
    {
        $this->be($this->user);
        $this->artisan('db:seed', [ '--class' => ActionCreateTestSeeder::class ]);

        // Create new action in project 2
        $action_1 = Action::factory()->create([
            'client_id' => 1,
            'project_id' => 2,
            'user_id' => $this->user->id,
        ]);

        // Create new action in inbox
        $action_2 = Action::factory()->create([
            'project_id' => 0,
            'client_id' => 0,
            'user_id' => $this->user->id,
        ]);

        $this->assertEquals(3, $action_1->project_order);
        $this->assertEquals(4, $action_2->project_order);
    }
    /** @test
     * This test is in to track a bug that was found on the original implementation
     * of the calculateProjectOrder, where the query to get the max project_order
     * was failing on the inbox where there were deleted actions
    */
    public function correct_project_order_for_new_action_with_deleted_actions()
    {
        $this->be($this->user);
        $this->artisan('db:seed', [ '--class' => ActionCreateTestSeeder::class ]);

        // Delete two actions from the inbox so we have a mix of
        Action::where('title', 'action 13')->delete();
        Action::where('title', 'action 14')->delete();
        // ...and re-index the remaining action
        Action::where('title', 'action 15')->update([
            'project_order' => 1,
        ]);

        // Create new action in project 2
        $action_1 = Action::factory()->create([
            'client_id' => 1,
            'project_id' => 2,
            'user_id' => $this->user->id,
        ]);

        // Create new action in inbox
        $action_2 = Action::factory()->create([
            'project_id' => 0,
            'client_id' => 0,
            'user_id' => $this->user->id,
        ]);

        $this->assertEquals(3, $action_1->fresh()->project_order);
        $this->assertEquals(2, $action_2->fresh()->project_order);
    }
}
