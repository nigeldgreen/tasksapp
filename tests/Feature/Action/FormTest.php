<?php

namespace Tests\Feature\Action;

use App\User;
use App\Action;
use App\Client;
use App\Project;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FormTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
    }

    /** @test */
    public function create_action_form_shows_correctly()
    {
        $this->be($this->user);
        $response = $this->json('GET', '/action/create');

        $response->assertSee('<h3>Create new action</h3>', false);
        $response->assertSeeInOrder([
            '<h3>Create new action</h3>',
            '<label for="client_id">Client</label>',
            '<option value=0  selected >',
            '<label for="project_id">Project</label>',
            '<option value=0  selected >',
        ], false);
    }
    /** @test */
    public function create_action_form_shows_client_from_session()
    {
        $this->be($this->user);
        $client = Client::factory()->create([ 'title' => 'My new client' ]);

        session([
            'client_id' => $client->id
        ]);

        $response = $this->call('GET', '/action/create');

        $response->assertSeeInOrder([
            '<h3>Create new action</h3>',
            '<label for="client_id">Client</label>',
            '<option value="' . $client->id . '"  selected >',
            'My new client',
            '<label for="project_id">Project</label>',
            '<option value=0  selected >',
        ], false);
    }
    /** @test */
    public function create_action_form_shows_client_and_project()
    {
        $this->be($this->user);
        $client = Client::factory()->create([ 'title' => 'My new client' ]);
        $project = Project::factory()->create([
            'client_id' => $client->id,
            'title' => 'My new project'
        ]);

        $response = $this->call('GET', '/action/create/' . $project->id);

        $response->assertSeeInOrder([
            '<h3>Create new action</h3>',
            '<label for="client_id">Client</label>',
            '<option value="' . $client->id . '"  selected >',
            'My new client',
            '<label for="project_id">Project</label>',
            '<option value="' . $project->id . '"  selected >',
            'My new project',
        ], false);
    }
    /** @test */
    public function create_form_is_shown_again_if_add_another_selected()
    {
        $this->be($this->user);
        $response = $this->post('/action', [
            'title' => 'test the action form',
            'user_id' => $this->user->id,
            'addAnotherAction' => 'on'
        ]);

        $action = Action::first();

        $response->assertRedirect(route('action.create'));
        $this->assertEquals('test the action form', $action->title);
    }
    /** @test */
    public function edit_action_form_shows_correctly()
    {
        $this->be($this->user);
        $action = Action::factory()->create();

        $response = $this->json('GET', '/action/' . $action->id . '/edit');

        $response->assertSee('<h3>Update action details</h3>', false);
    }
}
