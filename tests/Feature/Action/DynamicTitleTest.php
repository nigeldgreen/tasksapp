<?php

namespace Tests\Feature\Action;

use App\User;
use App\Action;
use App\Client;
use App\Project;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DynamicTitleTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
    }

    /** @test */
    public function create_action_form_shows_data_correctly()
    {
        $this->be($this->user);
        $action = Action::factory()->create([ 'title' => 'Test test test' ]);
        $response = $this->json(
            'GET',
            '/actiontitle/' . $action->id . '/edit'
        );

        $response->assertSee('value="' . "Test test test" . '" id="dynamic-title-field"', false);
    }

    /** @test */
    public function updating_title_gets_correct_action_data_back()
    {
        $this->be($this->user);
        $action = Action::factory()->create([ 'title' => 'Test test test' ]);
        $response = $this->json(
            'POST',
            '/actiontitle/' . $action->id,
            [ 'title' => "This has now changed"]
        );

        $response->assertSeeInOrder([
            '<div class="action-title" action_id="' . $action->id . '">',
            'This has now changed',
        ], false);
    }
}
