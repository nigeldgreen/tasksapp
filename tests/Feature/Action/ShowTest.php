<?php

namespace Tests\Feature\Action;

use App\User;
use App\Action;
use App\Client;
use App\Project;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ShowTest extends TestCase
{
    use RefreshDatabase;

    private $user;
    private $action_1;
    private $action_2;
    private $action_3;
    private $action_4;
    private $action_5;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);

        $client1 = Client::factory()->create([ 'title' => 'Test client 1' ]);
        $project1 = Project::factory()->create([ 'title' => 'Test project 1', 'client_id' => $client1->id ]);
        $project2 = Project::factory()->create([ 'title' => 'Test project 2', 'client_id' => $client1->id ]);
        $this->action_1 = Action::factory()->create([
            'title' => 'This action is in the inbox'
        ]);
        $this->action_2 = Action::factory()->create([
            'title' => 'This action is in client 1 with no project',
            'client_id' => $client1->id,
        ]);
        $this->action_3 = Action::factory()->create([
            'title' => 'This action is in client 1, project 1',
            'client_id' => $client1->id,
            'project_id' => $project1->id,
        ]);
        $this->action_4 = Action::factory()->create([
            'title' => 'This action is also in client 1, project 1',
            'client_id' => $client1->id,
            'project_id' => $project1->id,
        ]);
        $this->action_5 = Action::factory()->create([
            'title' => 'This action is in client 1, project 2',
            'client_id' => $client1->id,
            'project_id' => $project2->id,
        ]);
    }
    /** @test */
    public function can_see_all_actions_in_the_main_planning_view()
    {
        $response = $this->json('Get', '/client');

        $response->assertSeeInOrder([
            'This action is in the inbox',
            'This action is in client 1 with no project',
            'This action is in client 1, project 1',
            'This action is also in client 1, project 1',
            'This action is in client 1, project 2',
        ], false);
    }
    /** @test */
    public function cannot_see_actions_with_a_due_date_in_the_main_planning_view()
    {
        // set the flag for the user to hide due actions
        $this->user->hideDueActions();

        // set up a couple of actions to have a due date
        $this->action_1->update([ 'due' => Carbon::now() ]);
        $this->action_2->update([ 'due' => Carbon::now() ]);
        $this->action_3->update([ 'due' => Carbon::now() ]);

        $response = $this->json('Get', '/client');

        $response->assertSeeInOrder([
            'This action is also in client 1, project 1',
            'This action is in client 1, project 2',
        ], false);

        $response->assertDontSee([
            'This action is in the inbox',
            'This action is in client 1 with no project',
            'This action is in client 1, project 1',
        ], false);
    }
}
