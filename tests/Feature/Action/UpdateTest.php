<?php

namespace Tests\Feature\Action;

use App\User;
use App\Action;
use App\Client;
use App\Project;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateTest extends TestCase
{
    use RefreshDatabase;

    private Collection|Model $client;
    private Collection|Model $project;
    private Collection|Model $action;
    private Collection|Model|Authenticatable $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
        $this->client = Client::factory()->create();
        $this->project = Project::factory()->create(['client_id' => $this->client->id]);
        $this->action = Action::factory()->create([
            'client_id' => $this->client->id,
            'project_id' => $this->project->id,
            'title' => 'Fake action',
            'notes' => '',
            'user_id' => $this->user->id,
            'flagged' => 0
        ]);
    }

    // HAPPY PATH FOR UPDATING
    /** @test */
    public function can_update_core_action_info()
    {
        $this->assertEquals(1, $this->action->project_order);

        $this->call('PATCH', '/action/' . $this->action->id, [
            'title' => 'This has been changed',
            'notes' => 'Here are some notes',
            'flagged' => 1
        ]);
        $this->assertDatabaseHas('actions', [
            'id' => $this->action->id,
            'title' => 'This has been changed',
            'notes' => 'Here are some notes',
            'project_order' => 1,
            'flagged' => 1
        ]);
    }
    /** @test */
    public function updated_title_changes_title_on_home_page()
    {
        $this->call('PATCH', '/action/' . $this->action->id, [
            'title' => 'Has been changed!'
        ])->assertRedirect('/home');

        $this->assertEquals('Has been changed!', $this->action->fresh()->title);
    }
    /** @test */
    public function project_order_preserved_when_updating_with_multiple_actions()
    {
        $actions = Action::factory(4)->create([
            'client_id' => $this->client->id,
            'project_id' => $this->project->id,
        ]);

        $this->json('PATCH', '/action/' . $this->action->id, [
            'client_id' => "1",
            'project_id' => "1",
            'title' => 'This has been changed',
        ]);

        $this->assertDatabaseHas('actions', [ 'id' => $this->action->id, 'project_order' => 1, 'title' => 'This has been changed' ]);
        $this->assertDatabaseHas('actions', [ 'id' => $actions[0]->id, 'project_order' => 2 ]);
        $this->assertDatabaseHas('actions', [ 'id' => $actions[1]->id, 'project_order' => 3 ]);
        $this->assertDatabaseHas('actions', [ 'id' => $actions[2]->id, 'project_order' => 4 ]);
        $this->assertDatabaseHas('actions', [ 'id' => $actions[3]->id, 'project_order' => 5 ]);
    }
    /** @test */
    public function metadata_not_changed_when_no_params_sent()
    {
        $actions = Action::factory(2)->create([
            'client_id' => $this->client->id,
            'project_id' => $this->project->id,
        ]);

        // Update the first of the new actions inserted for this test
        $this->json('PATCH', '/action/' . $actions[0]->id, []);

        $this->assertDatabaseHas('actions', [
            'id' => $this->action->id,
            'title' => 'Fake action',
            'notes' => '',
            'project_order' => 1,
            'flagged' => 0
        ]);
    }

    // SAD PATH FOR UPDATING
    /** @test */
    public function not_found_error_received_if_action_missing()
    {
        $this->withExceptionHandling();

        $response = $this->json('PATCH', '/action/101');

        $response->assertStatus(404);
    }

    // MOVING ACTIONS AROUND
    /** @test */
    public function project_order_updated_when_moving_action_to_end()
    {
        $actions = Action::factory(4)->create([
            'client_id' => $this->client->id,
            'project_id' => $this->project->id
        ]);

        $this->json('PATCH', '/action/' . $this->action->id, [
            'client_id' => $this->client->id,
            'project_id' => $this->project->id,
            'project_order' => 5
        ]);
        $this->assertDatabaseHas('actions', [ 'id' => $this->action->id, 'project_order' => 5 ]);
        $this->assertDatabaseHas('actions', [ 'id' => $actions[0]->id, 'project_order' => 1 ]);
        $this->assertDatabaseHas('actions', [ 'id' => $actions[1]->id, 'project_order' => 2 ]);
        $this->assertDatabaseHas('actions', [ 'id' => $actions[2]->id, 'project_order' => 3 ]);
        $this->assertDatabaseHas('actions', [ 'id' => $actions[3]->id, 'project_order' => 4 ]);
    }
    /** @test */
    public function project_order_updated_when_moving_action_to_start()
    {
        $actions = Action::factory(4)->create([
            'client_id' => $this->client->id,
            'project_id' => $this->project->id
        ]);

        $this->json('PATCH', '/action/' . $actions[3]->id, [
            'client_id' => $this->client->id,
            'project_id' => $this->project->id,
            'project_order' => 1
        ]);
        $this->assertDatabaseHas('actions', [ 'id' => $this->action->id, 'project_order' => 2 ]);
        $this->assertDatabaseHas('actions', [ 'id' => $actions[0]->id, 'project_order' => 3 ]);
        $this->assertDatabaseHas('actions', [ 'id' => $actions[1]->id, 'project_order' => 4 ]);
        $this->assertDatabaseHas('actions', [ 'id' => $actions[2]->id, 'project_order' => 5 ]);
        $this->assertDatabaseHas('actions', [ 'id' => $actions[3]->id, 'project_order' => 1 ]);
    }
    /** @test */
    public function project_order_updated_when_moving_action_to_middle()
    {
        $actions = Action::factory(4)->create([
            'client_id' => $this->client->id,
            'project_id' => $this->project->id
        ]);

        $this->json('PATCH', '/action/' . $this->action->id, [
            'client_id' => $this->client->id,
            'project_id' => $this->project->id,
            'project_order' => 3
        ]);
        $this->assertDatabaseHas('actions', [ 'id' => $this->action->id, 'project_order' => 3 ]);
        $this->assertDatabaseHas('actions', [ 'id' => $actions[0]->id, 'project_order' => 1 ]);
        $this->assertDatabaseHas('actions', [ 'id' => $actions[1]->id, 'project_order' => 2 ]);
        $this->assertDatabaseHas('actions', [ 'id' => $actions[2]->id, 'project_order' => 4 ]);
        $this->assertDatabaseHas('actions', [ 'id' => $actions[3]->id, 'project_order' => 5 ]);
    }
    /** @test */
    public function project_order_updated_when_moving_between_clients()
    {
        Client::factory()->create();
        Project::factory()->create(['client_id' => 2]);
        $a = Action::factory(5)->create(['project_id' => 1]);
        $b = Action::factory(5)->create(['project_id' => 2]);

        $this->json('PATCH', '/action/' . $a[2]->id, [
            'client_id' => 2,
            'project_id' => 2,
            'project_order' => 6
        ]);
        $this->assertDatabaseHas('actions', [ 'id' => $a[0]->id, 'project_order' => 1 ]);
        $this->assertDatabaseHas('actions', [ 'id' => $a[2]->id, 'project_order' => 6 ]);
        $this->assertDatabaseHas('actions', [ 'id' => $a[3]->id, 'project_order' => 3 ]);
        $this->assertDatabaseHas('actions', [ 'id' => $b[4]->id, 'project_order' => 5 ]);
    }
    /** @test */
    public function activity_logged_when_updating_an_action()
    {
        $action = Action::factory()->create();
        $lastUpdate = $this->user->lastActive;

        $this->json('PATCH', '/action/' . $action->id, [ 'title' => 'store a test action with AJAX form' ]);

        $this->assertNotEquals($lastUpdate, $this->user->lastActive);
    }
}
