<?php

namespace Tests\Feature\Action;

use App\User;
use App\Action;
use App\Client;
use App\Project;
use Carbon\Carbon;
use Mockery;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompletedTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }

    /** @test */
    public function action_can_be_marked_as_deleted()
    {
        $action = Action::factory()->create();

        $response = $this->json('GET', '/completeaction/' . $action->id);

        $this->assertSoftDeleted('actions', [ 'id' => $action->fresh()->id ]) ;
    }
    /** @test */
    public function completed_action_view_shows_data_correctly()
    {
        Action::factory()->create([ 'title' => 'Completed today', 'deleted_at' => Carbon::now() ]);
        Action::factory()->create([ 'title' => 'Completed yesterday', 'deleted_at' => Carbon::now()->subDay() ]);
        Action::factory()->create([ 'title' => 'Completed last week', 'deleted_at' => Carbon::now()->subDays(4) ]);
        Action::factory()->create([ 'title' => 'Not completed' ]);


        $response = $this->json('GET', '/completedactions/');

        $response->assertSeeInOrder([
            '<span class="project-title project-toggle">Today</span>',
            'Completed today',
            '<span class="project-title project-toggle">Yesterday</span>',
            'Completed yesterday',
            '<span class="project-title project-toggle">Last week</span>',
            'Completed last week',
        ], false);
        $response->assertDontSee('Not completed');
    }

    /** @test */
    public function can_restore_action_to_original_project()
    {
        $client = Client::factory()->create();
        $project = Project::factory()->create();
        $action = Action::factory()->create([ 'client_id' => $client->id, 'project_id' => $project->id ]);

        $action->delete();
        $this->assertSoftDeleted('actions', [ 'id' => $action->fresh()->id ]);

        $this->json('DELETE', '/restoreaction/' . $action->id);

        $this->assertNull($action->fresh()->deleted_at);
        $this->assertEquals($client->id, $action->fresh()->client_id);
        $this->assertEquals($project->id, $action->fresh()->project_id);
    }

    /** @test */
    public function restored_action_goes_to_inbox_if_client_and_project_deleted()
    {
        $client = Client::factory()->create([  ]);
        $project = Project::factory()->create([  ]);
        $action = Action::factory()->create([ 'client_id' => $client->id, 'project_id' => $project->id ]);

        $action->delete();
        $this->assertSoftDeleted('actions', [ 'id' => $action->fresh()->id ]);

        $client->delete();
        $project->delete();
        $this->json('DELETE', '/restoreaction/' . $action->id);

        $this->assertNull($action->fresh()->deleted_at);
        $this->assertEquals(0, $action->fresh()->client_id);
        $this->assertEquals(0, $action->fresh()->project_id);
    }
}
