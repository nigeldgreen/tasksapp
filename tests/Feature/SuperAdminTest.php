<?php

namespace Tests\Feature;

use App\Action;
use App\Client;
use App\Project;
use App\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SuperAdminTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
    }

    /** @test */
    public function cannot_see_super_admin_page_if_not_superadmin()
    {
        $this->withExceptionHandling();
        $this->be($this->user);

        $response = $this->call('GET', '/superadmin');

        $response->assertStatus(403);
    }

    /** @test */
    public function can_see_super_admin_page_if_user_is_superadmin()
    {
        $this->set_as_superadmin();
        $response = $this->call('GET', '/superadmin');

        $response->assertStatus(200);
    }

    /** @test */
    public function user_data_displayed_correctly()
    {
        $newuser = User::factory()->create([
            'name' => 'Smew Watters',
            'email' => 'smew@watters.com',
        ]);

        // ACT
        $this->call('POST', '/login', [
            'email' => 'smew@watters.com',
            'password' => 'secret'
        ])->assertRedirect('/');
        $newuser->logActivity();

        // Need to set the main user as superadmin here or it will skip the call to /login
        $this->set_as_superadmin();

        Client::factory(2)->create([ 'user_id' => $newuser->id ]);
        Project::factory(5)->create([ 'user_id' => $newuser->id ]);
        Action::factory(12)->create([ 'user_id' => $newuser->id ]);

        $response = $this->call('GET', '/superadmin');

        // ASSERT
        $today = Carbon::now()->startOfDay()->format('Y-m-d');
        $response->assertSeeInOrder([
            'Smew Watters',
            $today,
            $today,
            '1',
            $today,
            '2',
            '5',
            '12'
        ]);

    }

    /** @test */
    public function can_see_unset_debug_view_status()
    {
        $this->set_as_superadmin();

        $response = $this->call('GET', '/superadmin');

        $response->assertStatus(200)
            ->assertSee([
                'Show debug info',
                '<input id="debug_view" type="checkbox" class="form-control" name="debug_view" >',
            ], false);
    }

    /** @test */
    public function can_see_set_debug_view_status()
    {
        $this->set_as_superadmin();
        $this->user->update([
            'debug_view' => 1,
        ]);
        $this->user->save();

        $response = $this->call('GET', '/superadmin');

        $response->assertStatus(200)
            ->assertSee([
                'Show debug info',
                '<input id="debug_view" type="checkbox" class="form-control" name="debug_view" checked >',
            ], false);
    }

    private function set_as_superadmin() {
        $this->user->update([ 'su' => 1 ]);
        $this->user->save();
        $this->be($this->user);
    }
}
