<?php

namespace Tests\Feature\Auth;

use App\Interfaces\CaptchaValidationManager;
use App\Mail\NewUserRegisters;
use App\RegistrationToken;
use App\User;
use Carbon\Carbon;
use Illuminate\Validation\ValidationException;
use Mockery\MockInterface;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    public function setUp():void
    {
        parent::setUp();
    }

    // HAPPY PATH
    /** @test */
    public function user_can_see_register_screen()
    {
        $this->call("GET", '/register')
            ->assertStatus(200)
            ->assertSeeInOrder([
                'name="name"',
                'name="email"',
                'name="password"',
                'name="password_confirmation"',
            ], false);
    }

    /** @test */
    public function registration_creates_unvalidated_user()
    {
        $this->mock(CaptchaValidationManager::class, function (MockInterface $mock) {
            $mock->shouldReceive('validate_captcha')
                ->once()
                ->andReturn(true);
        });

        $this->call('POST', '/register', [
            'name' => 'Test User',
            'email' => 'testingtheregistration@user.com',
            'password' => 'T0pSekrit',
            'password_confirmation' => 'T0pSekrit',
            'h-captcha-response' => 'test-code',
        ]);

        $new_user = User::find(1);
        $this->assertNull($new_user->validated_at);
    }

    /** @test */
    public function registration_creates_validation_token()
    {
        $mock = $this->mock(CaptchaValidationManager::class, function (MockInterface $mock) {
            $mock->shouldReceive('validate_captcha')
                ->once()
                ->andReturn(true);
        });

        $this->assertEquals(0, RegistrationToken::count());

        $this->call('POST', '/register', [
            'name' => 'Test User',
            'email' => 'testingtheregistration@user.com',
            'password' => 'T0pSekrit',
            'password_confirmation' => 'T0pSekrit',
            'h-captcha-response' => 'test-code',
        ]);

        $this->assertEquals(1, RegistrationToken::count());
    }

    /** @test */
    public function unvalidated_user_sees_validation_refresh_screen_on_access()
    {
        $this->be($this->user = User::factory()->create());
        $this->call("GET", '/')
            ->assertSee('<h1>Account not validated</h1>', false);
    }

    /** @test */
    public function validated_user_sees_client_index_screen_on_login()
    {
        $this->be($this->user = User::factory([ 'validated_at' => Carbon::now() ])->create());
        $this->call("GET", '/')
            ->assertRedirect('/client');
    }

    /** @test */
    public function registration_emails_user_to_validate_account()
    {
        Mail::fake();
        $this->mock(CaptchaValidationManager::class, function (MockInterface $mock) {
            $mock->shouldReceive('validate_captcha')
                ->once()
                ->andReturn(true);
        });

        $this->call('POST', '/register', [
            'name' => 'Test User',
            'email' => 'testingtheregistration@user.com',
            'password' => 'T0pSekrit',
            'password_confirmation' => 'T0pSekrit',
            'h-captcha-response' => 'test-code',
        ]);

        // Make sure user receives email
        $user = User::first();
        Mail::assertSent(
            NewUserRegisters::class, function ($mail) use ($user) {
                return $mail->hasTo($user->email);
            }
        );
    }



    //SAD PATH
    /** @test */
    public function registration_fails_on_empty_form()
    {
        $this->withoutExceptionHandling();
        $this->expectException(ValidationException::class);
        $this->call('POST', '/register', []);
    }
    /** @test */
    public function registration_fails_if_captcha_validation_fails()
    {
        $this->mock(CaptchaValidationManager::class, function (MockInterface $mock) {
            $mock->shouldReceive('validate_captcha')
                ->once()
                ->andReturn(false);
        });

        $this->withoutExceptionHandling();
        $this->expectException(ValidationException::class);
        $this->call('POST', '/register', [
            'name' => 'Test User',
            'email' => 'testingtheregistration@user.com',
            'password' => 'T0pSekrit',
            'password_confirmation' => 'T0pSekrit',
            'h-captcha-response' => 'test-code',
        ]);
    }

}
