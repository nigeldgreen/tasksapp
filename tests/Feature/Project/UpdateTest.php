<?php

namespace Tests\Feature\Project;

use App\User;
use App\Action;
use App\Client;
use App\Project;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class  UpdateTest extends TestCase
{
    use RefreshDatabase;

    private Collection|Model|Authenticatable $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }

    // UPDATE PROJECT DATA
    /** @test */
    public function can_update_project_metadata()
    {
        Client::factory()->create([ 'id' => 1 ]);
        Project::factory(3)->create([ 'client_id' => 1 ]);
        $project = Project::factory()->create([
            'client_id' => 1,
            'title' => 'Test client',
            'notes' => 'Some test notes',
        ]);
        $this->assertEquals(1, $project->client_id);
        $this->assertEquals('Test client', $project->title);
        $this->assertEquals('Some test notes', $project->notes);
        $this->assertEquals(4, $project->client_order);

        $this->call('PATCH', '/project/' . $project->id, [
            'title' => 'Wait, this has changed!',
            'notes' => 'And there\'s a new note as well!',
        ]);
        $this->assertEquals(1, $project->fresh()->client_id);
        $this->assertEquals('Wait, this has changed!', $project->fresh()->title);
        $this->assertEquals('And there\'s a new note as well!', $project->fresh()->notes);
        $this->assertEquals(4, $project->fresh()->client_order);
    }
    /** @test */
    public function metadata_unchanged_if_no_data_sent()
    {
        Client::factory()->create([ 'id' => 1 ]);
        Project::factory(3)->create([ 'client_id' => 1 ]);
        $project = Project::factory()->create([
            'client_id' => 1,
            'title' => 'Test client',
            'notes' => 'Some test notes',
        ]);
        $this->assertEquals(1, $project->client_id);
        $this->assertEquals('Test client', $project->title);
        $this->assertEquals('Some test notes', $project->notes);
        $this->assertEquals(4, $project->client_order);

        $this->call('PATCH', '/project/' . $project->id, []);
        $this->assertEquals(1, $project->fresh()->client_id);
        $this->assertEquals('Test client', $project->fresh()->title);
        $this->assertEquals('Some test notes', $project->fresh()->notes);
        $this->assertEquals(4, $project->fresh()->client_order);
    }

    // MOVE PROJECT
    /** @test */
    public function client_order_updated_when_moving_project_to_end()
    {
        $client = Client::factory()->create();
        $projects = Project::factory(5)->create(['client_id' => $client->id]);

        $this->call('PATCH', '/project/' . $projects[2]->id, [
            'client_id' => $client->id,
            'client_order' => 5
        ]);
        $this->assertDatabaseHas('projects', ['id' => $projects[0]->id, 'client_order' => 1]);
        $this->assertDatabaseHas('projects', ['id' => $projects[1]->id, 'client_order' => 2]);
        $this->assertDatabaseHas('projects', ['id' => $projects[2]->id, 'client_order' => 5]);
        $this->assertDatabaseHas('projects', ['id' => $projects[3]->id, 'client_order' => 3]);
        $this->assertDatabaseHas('projects', ['id' => $projects[4]->id, 'client_order' => 4]);
    }
    /** @test */
    public function client_order_updated_when_moving_project_to_start()
    {
        $client = Client::factory()->create();
        $projects = Project::factory(5)->create(['client_id' => $client->id]);

        $this->call('PATCH', '/project/' . $projects[2]->id, [
            'client_id' => $client->id,
            'client_order' => 1
        ]);
        $this->assertDatabaseHas('projects', ['id' => $projects[0]->id, 'client_order' => 2]);
        $this->assertDatabaseHas('projects', ['id' => $projects[1]->id, 'client_order' => 3]);
        $this->assertDatabaseHas('projects', ['id' => $projects[2]->id, 'client_order' => 1]);
        $this->assertDatabaseHas('projects', ['id' => $projects[3]->id, 'client_order' => 4]);
        $this->assertDatabaseHas('projects', ['id' => $projects[4]->id, 'client_order' => 5]);
    }
    /** @test */
    public function client_order_updated_when_moving_project_to_middle()
    {
        $client = Client::factory()->create();
        $projects = Project::factory(5)->create(['client_id' => $client->id]);

        $this->call('PATCH', '/project/' . $projects[4]->id, [
            'client_id' => $client->id,
            'client_order' => 3
        ]);
        $this->assertDatabaseHas('projects', ['id' => $projects[0]->id, 'client_order' => 1]);
        $this->assertDatabaseHas('projects', ['id' => $projects[1]->id, 'client_order' => 2]);
        $this->assertDatabaseHas('projects', ['id' => $projects[2]->id, 'client_order' => 4]);
        $this->assertDatabaseHas('projects', ['id' => $projects[3]->id, 'client_order' => 5]);
        $this->assertDatabaseHas('projects', ['id' => $projects[4]->id, 'client_order' => 3]);
    }
    /** @test */
    public function client_order_updated_when_moving_between_clients()
    {
        // Arrange
        $clients = Client::factory(3)->create();
        $p1 = Project::factory()->create(['client_id' => $clients[0]->id]);
        $p2 = Project::factory()->create(['client_id' => $clients[1]->id]);
        $p3 = Project::factory(2)->create(['client_id' => $clients[2]->id]);

        $this->json('PATCH', '/project/' . $p2->id, [
            'client_id' => $clients[2]->id,
            'client_order' => 3
        ]);

        $this->assertEquals(1, $p1->fresh()->client_order);
        $this->assertEquals(3, $p2->fresh()->client_order);
        $this->assertEquals(2, $p3[1]->fresh()->client_order);
    }
    /** @test */
    public function client_id_updated_for_actions_when_moving_project()
    {
        // Arrange
        $clients = Client::factory(3)->create();
        $projects = Project::factory(3)->create(['client_id' => $clients[0]->id]);
        $projects2 = Project::factory(3)->create(['client_id' => $clients[1]->id]);
        $actions = Action::factory(5)->create(['client_id' => $clients[0]->id, 'project_id' => $projects[1]]);

        $this->assertEquals($clients[0]->id, $actions[0]->client_id);

        $this->json('PATCH', '/project/' . $projects[1]->id, [
            'client_id' => $clients[1]->id,
            'client_order' => 2
        ]);

        $this->assertEquals($clients[1]->id, $actions[0]->fresh()->client_id);
    }

    // BUG FIX TESTS
    /** @test
     * This came from bug where the client_order values were setting correctly, but the projects
     * were showing up on screen incorrectly after reordering
     */
    public function projects_shown_in_correct_order_on_main_view()
    {
        Client::factory(1)->create();
        $p1 = Project::factory()->create([ 'client_id' => 1, 'title' => 'Test project 1' ]);
        $p2 = Project::factory()->create([ 'client_id' => 1, 'title' => 'Test project 2' ]);
        $p3 = Project::factory()->create([ 'client_id' => 1, 'title' => 'Test project 3' ]);
        $p4 = Project::factory()->create([ 'client_id' => 1, 'title' => 'Test project 4' ]);

        $this->user->update([
            'debug_view' => 1,
        ]);

        $this->call('GET', '/client')
            ->assertSeeInOrder([
                'Test project 1 [1]',
                'Test project 2 [2]',
                'Test project 3 [3]',
                'Test project 4 [4]',
            ]);

        $this->json('PATCH', '/project/' . $p2->id, [
            'client_id' => 1,
            'client_order' => 1
        ]);
        $this->json('PATCH', '/project/' . $p4->id, [
            'client_id' => 1,
            'client_order' => 2
        ]);

        $this->call('GET', '/client')
            ->assertSeeInOrder([
                'Test project 2 [1]',
                'Test project 4 [2]',
                'Test project 1 [3]',
                'Test project 3 [4]',
            ]);

    }
}
