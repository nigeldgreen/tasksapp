<?php

namespace Tests\Feature\Project;

use App\User;
use App\Action;
use App\Client;
use App\Project;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CreateTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }

    /** @test */
    public function client_order_is_set_for_first_project_in_client()
    {
        $client = Client::factory()->create();

        $response = $this->call('POST', '/project', [
            'title' => 'Test test test',
            'notes' => 'Lots of notes',
            'client_id' => $client->id,
        ]);
        $project = Project::orderBy('id', 'desc')->first();

        $this->assertEquals(1, $project->client_order);
        $this->assertEquals($this->user->id, $project->user_id);
        $response->assertRedirect();
    }
    /** @test */
    public function client_order_is_set_for_subsequent_project_in_client()
    {
        $client = Client::factory()->create();
        $projects = Project::factory(12)->create([
            'client_id' => $client->id,
            'user_id' => $this->user->id
        ]);

        $response = $this->call('POST', '/project', [
            'user_id' => $this->user->id,
            'title' => 'Test test test',
            'notes' => 'Lots of notes',
            'client_id' => $client->id
        ]);
        $project = $projects[11];

        $this->assertEquals(12, $project->client_order);
        $this->assertEquals($this->user->id, $project->user_id);
        $response->assertRedirect('/');
    }
    /** @test */
    public function project_can_be_created_with_ajax_request()
    {
        $client = Client::factory()->create();

        $response = $this->json('POST', '/project', [
            'title' => 'Test test test',
            'notes' => 'Lots of notes',
            'client_id' => $client->id,
        ]);
        $project = Project::orderBy('id', 'desc')->first();

        $this->assertEquals(1, $project->client_order);
        $this->assertEquals($this->user->id, $project->user_id);
        $this->assertEquals('Lots of notes', $project->notes);
        $response->assertStatus(201);
    }
}
