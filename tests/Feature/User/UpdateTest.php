<?php

namespace Tests\Feature\User;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateTest extends TestCase
{
    use RefreshDatabase;

    protected User $user;

    public function setUp():void
    {
        parent::setUp();

        $this->user = User::factory()->create();
        $this->be($this->user);
    }

    /** @test */
    public function cannot_set_debug_view_for_normal_user()
    {
        $response = $this->call('PUT', '/user', [
            'debug_view' => 1,
        ]);

        $response->assertStatus(403);
        $this->assertNull($this->user->debug_view);
    }

    /** @test */
    public function can_set_debug_view_for_superuser()
    {
        $this->user->update([
            'su' => 1,
        ]);
        $this->user->save();

        $response = $this->call('PUT', '/user', [
            'debug_view' => 1,
        ]);

        $response->assertStatus(200);
        $this->assertNotNull($this->user->refresh()->debug_view);
    }

    /** @test */
    public function can_unset_debug_view_for_superuser()
    {
        $this->user->update([
            'su' => 1,
        ]);
        $this->user->save();

        $this->assertNull($this->user->debug_view);

        $this->user->update([
            'debug_view' => 1,
        ]);
        $this->user->save();

        $this->assertNotNull($this->user->refresh()->debug_view);

        $response = $this->call('PUT', '/user', [
            'debug_view' => null,
        ]);

        $response->assertStatus(200);
        $this->assertNull($this->user->refresh()->debug_view);
    }
}
