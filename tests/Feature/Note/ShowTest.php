<?php

namespace Tests\Feature\Note;

use App\Note;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShowTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }
    /** @test */
    public function main_notes_view_shows_correctly()
    {
        Note::factory()->create([ 'title' => 'Test note 1' ]);
        Note::factory()->create([ 'title' => 'Test note 2' ]);
        Note::factory()->create([ 'title' => 'Test note 3' ]);
        $wrongnote = Note::factory()->create([ 'title' => 'Test note 4' ]);

        $wrongnote->user_id = User::factory()->create()->id;
        $wrongnote->save();

        $response = $this->json('GET', '/notes');

        $response->assertStatus(200);
        $response->assertSeeInOrder([
            '<div class="note-title">Test note 1</div>',
            '<div class="note-title">Test note 2</div>',
            '<div class="note-title">Test note 3</div>',
        ], false);
        $response->assertDontSee('Test note 4');
    }
    /** @test */
    public function can_get_html_back_for_single_note()
    {
        $body = <<<EOT
## heading 2
This is a test paragraph

- list 1
- list 2
- list 3

EOT;

        $note = Note::factory()->create([
            'title' => 'Test note 1',
            'body' => $body
        ]);

        $response = $this->json('GET', '/note/' . $note->id);

        $response->assertStatus(200);
        $response->assertSeeInOrder([
            '<div class=\'display-note\'><h1>Test note 1',
            '<div class="tags-container"><p>Tags: none defined',
            '<h2>heading 2</h2>',
            '<p>This is a test paragraph</p>',
            '<ul>',
            '<li>list 1</li>',
            '<li>list 2</li>',
            '<li>list 3</li>',
        ], false);
    }
}
