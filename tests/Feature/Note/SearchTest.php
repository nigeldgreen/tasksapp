<?php

namespace Tests\Feature\Note;

use App\Note;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SearchTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);

        Note::factory()->create([
            'title' => 'Test note 1',
            'body' => 'This is a test note about tractors and farms and carrier pigeons.',
        ]);
        Note::factory()->create([
            'title' => 'Test note 2',
            'body' => 'This is a test note about office chairs, whiteboards and carpets.',
        ]);
        Note::factory()->create([
            'title' => 'Test note 3',
            'body' => 'This is a test note about shop shelves, carrier bags and elves.',
        ]);
    }

    /** @test */
    public function no_actions_returned_if_search_term_not_matched()
    {
        $response = $this->call('POST', '/notes', [
            'searchterm' => 'this phrase is not found'
        ]);

        $this->assertEquals(0, preg_match_all('/div class="note-title"/', $response->content()));
    }

    /** @test */
    public function two_actions_returned_if_search_term_is_carrier()
    {
        $response = $this->call('POST', '/notes', [
            'searchterm' => 'carrier'
        ]);

        $this->assertEquals(2, preg_match_all('/div class="note-title"/', $response->content()));
    }

    /** @test */
    public function two_actions_returned_if_search_term_is_in_all_notes()
    {
        $response = $this->call('POST', '/notes', [
            'searchterm' => 'This is a test note about'
        ]);

        $this->assertEquals(3, preg_match_all('/div class="note-title"/', $response->content()));
    }

    /** @test */
    public function search_term_and_clear_search_are_displayed_when_results_shown()
    {
        $response = $this->withServerVariables([
            'HTTP_REFERER' => '/notes'
        ])
            ->followingRedirects()
            ->call('POST', '/notes', [ 'searchterm' => 'carrier' ])
            ->assertSee('<a href="' . route('note.index') . '" class="clear-search">clear</a>', false)
            ->assertSee('<input class="form-control" id="note-search-input" name="searchterm" type="text" placeholder="search"value="carrier">', false);
    }

    /** @test */
    public function clear_search_not_displayed_if_no_search_term_set()
    {
        $response = $this->call('GET', '/notes')
            ->assertDontSee('<a href="' . route('note.index') . '" class="clear-search">clear</a>');

        $this->assertEquals(3, preg_match_all('/div class="note-title"/', $response->content()));
    }
}
