<?php

namespace Tests\Feature\Note;

use App\Note;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }
    /** @test */
    public function correct_response_received_for_json_note_create()
    {
        $json_response = $this->json('POST', '/note', [
            'title' => 'test note',
            'body' => 'test test test test',
        ]);

        $note = Note::orderBy('id', 'desc')->first();

        $json_response->assertStatus(200);
        $this->assertEquals('test test test test', $note->body);
    }
    /** @test */
    public function correct_response_received_for_http_note_create()
    {
        $static_response = $this->call('POST', '/note', [
            'title' => 'test note',
            'body' => 'test test testitty test test',
        ]);

        $note = Note::orderBy('id', 'desc')->first();

        $static_response->assertRedirect('/notes');
        $this->assertEquals('test test testitty test test', $note->body);
    }
    /** @test */
    public function overview_created_correctly_for_different_length_body_text()
    {
        $note_1 = Note::factory()->create([
            'body' => 'test test test test',
        ]);
        $note_2 = Note::factory()->create([
            'body' => "This is a fake note. It has fake text in it's fake little body and it is longer",
        ]);
        $note_3 = Note::factory()->create([
            'body' => "This is a fake note. It has fake text in it's fake little body and it is even longer",
        ]);

        $this->assertEquals(
            'test test test test',
            $note_1->overview
        );
        $this->assertEquals(
            "This is a fake note. It has fake text in it's fake little body and it is longer",
            $note_2->overview
        );
        $this->assertEquals(
            "This is a fake note. It has fake text in it's fake little body and it is even l...",
            $note_3->overview
        );
    }
    /** @test */
    public function note_create_fails_if_title_is_missing()
    {
        $this->withExceptionHandling();

        $response = $this->json('POST', '/note', [
            'body' => 'body is here but there is no title!',
        ]);

        $response->assertStatus(422);
    }
    /** @test */
    public function note_create_fails_if_body_is_missing()
    {
        $this->withExceptionHandling();

        $response = $this->json('POST', '/note', [
            'title' => 'title is here but there is no body!',
        ]);

        $response->assertStatus(422);
    }
}
