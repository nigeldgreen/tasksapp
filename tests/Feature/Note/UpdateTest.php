<?php

namespace Tests\Feature\Note;

use App\Note;
use App\User;
use App\Action;
use App\Client;
use App\Project;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UpdateTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }
    /** @test */
    public function note_is_updated_via_ajax_request()
    {
        $note = Note::factory()->create([
            'body' => "This is a fake note. It has fake text in it's fake little body and it is longer than 80 characters."
        ]);
        $this->assertEquals(
            'Fake note',
            $note->title
        );
        $this->assertEquals(
            "This is a fake note. It has fake text in it's fake little body and it is longer than 80 characters.",
            $note->body
        );
        $this->assertEquals(
            "This is a fake note. It has fake text in it's fake little body and it is longer...",
            $note->overview
        );

        $response = $this->json('PATCH', '/note/' . $note->id, [
            'title' => 'This bad boy has been updated',
            'body' => "The note has been updated and now has a different body so the overview should also have been updated."
        ]);
        $response->assertStatus(200);
        $this->assertEquals(
            'This bad boy has been updated',
            $note->fresh()->title
        );
        $this->assertEquals(
            "The note has been updated and now has a different body so the overview should also have been updated.",
            $note->fresh()->body
        );
        $this->assertEquals(
            "The note has been updated and now has a different body so the overview should a...",
            $note->fresh()->overview
        );
    }
    /** @test */
    public function can_update_tags_for_note()
    {
        $note = Note::factory()->create();
        $this->assertEquals($note->refresh()->tags, '');

        $response = $this->json('PATCH', '/note/' . $note->id, [
            'title' => $note->title,
            'body' => $note->body,
            'tags' => "test, test 1,  test 2",
        ]);

        $response->assertStatus(200);
        $this->assertEquals($note->refresh()->tags, 'test,test 1,test 2');
    }
    /** @test */
    public function duplicate_tags_removed_for_update()
    {
        $note = Note::factory()->create();
        $this->assertEquals($note->refresh()->tags, '');

        $response = $this->json('PATCH', '/note/' . $note->id, [
            'title' => $note->title,
            'body' => $note->body,
            'tags' => "test, test 1,  test 2, test 1, test, testt",
        ]);

        $response->assertStatus(200);
        $this->assertEquals($note->refresh()->tags, 'test,test 1,test 2,testt');
    }
    /** @test */
    public function note_fails_ajax_update_if_no_title_is_present()
    {
        $note = Note::factory()->create([
            'body' => "This is a fake note. It has fake text in it's fake little body and it is longer than 80 characters."
        ]);

        $response = $this->json('PATCH', '/note/' . $note->id, [
            'body' => "test test test",
        ]);

        $response->assertStatus(422);
        $this->assertEquals(
            'Fake note',
            $note->fresh()->title
        );
        $this->assertEquals(
            "This is a fake note. It has fake text in it's fake little body and it is longer than 80 characters.",
            $note->fresh()->body
        );
    }
    /** @test */
    public function note_fails_ajax_update_if_no_body_is_present()
    {
        $this->withExceptionHandling();
        $note = Note::factory()->create([
            'body' => 'This is a fake note. It has fake text in it\'s fake little body and it is longer than 80 characters.'
        ]);

        $response = $this->json('PATCH', '/note/' . $note->id, [
            'title' => "test test test",
        ]);

        $response->assertStatus(422);
        $this->assertEquals(
            'Fake note',
            $note->fresh()->title
        );
        $this->assertEquals(
            'This is a fake note. It has fake text in it\'s fake little body and it is longer than 80 characters.',
            $note->fresh()->body
        );
    }
    /** @test */
    public function note_is_updated_via_http_request()
    {
        $note = Note::factory()->create();
        $this->assertEquals('Fake note', $note->title);

        $response = $this->call('PATCH', '/note/' . $note->id, [
            'title' => 'This bad boy has been updated',
            'body' => 'new body'
        ]);
        $response->assertRedirect('/notes');
        $this->assertEquals('This bad boy has been updated', $note->fresh()->title);
        $this->assertEquals('new body', $note->fresh()->body);
        $this->assertEquals('new body', $note->fresh()->overview);
    }
}
