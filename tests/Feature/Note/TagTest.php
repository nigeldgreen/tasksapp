<?php

namespace Tests\Feature\Note;

use App\Note;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TagTest extends TestCase
{
    use DatabaseMigrations;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }
    /** @test */
    public function tags_are_stored_without_additional_spaces()
    {
        $note1 = Note::factory()->create([
            'tags' => 'tag1, tag2, tag3',
        ]);
        $note2 = Note::factory()->create([
            'tags' => 'tag1, tag 2,    this is not a tag',
        ]);

        $this->assertEquals($note1->refresh()->tags, 'tag1,tag2,tag3');
        $this->assertEquals($note2->refresh()->tags, 'tag1,tag 2,this is not a tag');
    }

    /** @test */
    public function duplicate_tags_are_removed_when_a_note_is_saved()
    {
        $note1 = Note::factory()->create([
            'tags' => 'tag1, tag2, tag3, tag1, tag2',
        ]);

        $this->assertEquals($note1->refresh()->tags, 'tag1,tag2,tag3');
    }

    /** @test */
    public function can_split_list_of_tags_into_collection()
    {
        $note1 = Note::factory()->create([
            'tags' => 'tag1, tag2, tag3',
        ]);
        $note2 = Note::factory()->create([
            'tags' => 'tag1, tag 2,    this is not a tag',
        ]);

        $this->assertEquals($note1->createTagCollection(), collect([
            'tag1',
            'tag2',
            'tag3'
        ]));
        $this->assertEquals($note2->createTagCollection(), collect([
            'tag1',
            'tag 2',
            'this is not a tag'
        ]));
    }

    /** @test */
    public function empty_tags_field_stores_as_empty_string()
    {
        $note1 = Note::factory()->create([
            'tags' => '',
        ]);

        $this->assertEquals("", $note1->refresh()->tags);
    }

    /** @test */
    public function missing_tags_field_stores_as_empty_string()
    {
        $note1 = Note::factory()->create();

        $this->assertEquals("", $note1->refresh()->tags);
    }

    /** @test */
    public function can_create_a_note_with_empty_tag_field()
    {
        $note1 = Note::factory()->create([
            'tags' => 'tag1, , tag3',
        ]);
        $note2 = Note::factory()->create([
            'tags' => ', tag 2,    this is not a tag',
        ]);

        $this->assertEquals($note1->createTagCollection(), collect([
            'tag1',
            'tag3'
        ]));
        $this->assertEquals($note2->createTagCollection(), collect([
            'tag 2',
            'this is not a tag'
        ]));
    }
}
