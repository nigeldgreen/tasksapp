<?php

namespace Tests\Feature\Note;

use App\Note;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }
    /** @test */
    public function correct_response_received_for_json_note_delete()
    {
        $note = Note::factory()->create();

        $this->json('DELETE', '/note/' . $note->id)
            ->assertStatus(204);

        $this->assertSoftDeleted('notes', [ 'id' => $note->id ]);
    }
    /** @test */
    public function correct_response_received_for_http_note_delete()
    {
        $note = Note::factory()->create();

        $this->call('DELETE', '/note/' . $note->id)
            ->assertRedirect('/notes');

        $this->assertSoftDeleted('notes', [ 'id' => $note->id ]);
    }
    /** @test */
    public function error_if_note_does_not_exist()
    {
        $this->withExceptionHandling();

        $this->call('DELETE', '/note/999')
            ->assertStatus(404);
    }
}
