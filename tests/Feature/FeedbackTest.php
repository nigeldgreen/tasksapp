<?php

namespace Tests\Feature;

use App\Feedback;
use App\Mail\FeedbackFormCompleted;
use App\User;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FeedbackTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }

    /** @test */
    public function can_see_feedback_page()
    {
        $response = $this->call('GET', '/feedback');
        $response->assertSeeInOrder([
            'Let me know what you think',
            'textarea',
            'id="message"',
            'id="submit-button"'
        ], false);
    }

    /** @test */
    public function feedback_from_form_is_stored_in_database()
    {
        Mail::fake();
        $this->call('POST', '/feedback', [ 'message' => 'Testing the feedback form' ]);

        $feedback = Feedback::orderBy('id', 'desc')->first();

        $this->assertNotNull($feedback);
        $this->assertEquals('Testing the feedback form', $feedback->message);

        Mail::assertSent(FeedbackFormCompleted::class, function ($mail) {
            return $mail->hasTo('hello@tasksapp.co.uk');
        });
    }
}
