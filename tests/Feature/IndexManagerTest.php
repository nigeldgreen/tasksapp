<?php

namespace Tests\Feature;

use App\Classes\IndexManager;
use App\Client;
use App\User;
use Tests\TestCase;
use Database\Seeds\IndexManagerTestSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class IndexManagerTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
        $this->artisan('db:seed', [ '--class' => IndexManagerTestSeeder::class ]);
    }

    /** @test */
    public function non_superadmin_gets_unauthorised_error()
    {
        $this->call('GET', '/reindex')
            ->assertStatus(403);
    }

    /** @test */
    public function can_get_correct_order_after_sorting()
    {
        $this->user->update([ 'su' => 1 ]);

        $this->call('GET', '/reindex')
            ->assertStatus(204);

        $clients = Client::with('projects.actions')->get();

        $this->assertEquals(1, $clients[0]->projects[0]->client_order);
        $this->assertEquals(2, $clients[0]->projects[1]->client_order);

        $this->assertEquals(1, $clients[0]->actions[0]->project_order);
        $this->assertEquals(2, $clients[0]->actions[1]->project_order);
        $this->assertEquals(3, $clients[0]->actions[2]->project_order);

        $this->assertEquals(1, $clients[0]->projects[0]->actions[0]->project_order);
        $this->assertEquals(2, $clients[0]->projects[0]->actions[1]->project_order);
        $this->assertEquals(3, $clients[0]->projects[0]->actions[2]->project_order);

        $this->assertEquals(1, $clients[0]->projects[1]->actions[0]->project_order);
        $this->assertEquals(2, $clients[0]->projects[1]->actions[1]->project_order);
    }

}
