<?php

namespace Tests\Feature;

use App\Action;
use App\Client;
use App\Project;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SearchTest extends TestCase
{
    use RefreshDatabase;

    private $user;

    public function setUp():void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->be($this->user);
    }

    /** @test */
    public function search_page_shows_all_results_on_get_request()
    {
        Action::factory()->create([ 'title' => 'Search test action 1' ]);
        Action::factory()->create([ 'title' => 'Search test action 2' ]);
        Action::factory()->create([ 'title' => 'Search test action 3' ]);

        $response = $this->call('GET', '/search');

        $response->assertSee('Search test action 1', false);
        $response->assertSee('Search test action 2', false);
        $response->assertSee('Search test action 3', false);
    }

    /** @test */
    public function search_page_show_results_for_valid_search_term()
    {
        Action::factory()->create([ 'title' => 'Search test action 1' ]);
        Action::factory()->create([ 'title' => 'Search test action 2' ]);
        Action::factory()->create([ 'title' => 'Search test action 3' ]);

        $response = $this->call('POST', '/search', [
            'searchterm' => '1'
        ]);

        $response->assertSee('Search test action 1', false);
        $response->assertDontSee('Search test action 2');
        $response->assertDontSee('Search test action 3');
    }

    /** @test */
    public function no_results_message_shows_for_invalid_search_term()
    {
        Action::factory()->create([ 'title' => 'Search test action 1' ]);
        Action::factory()->create([ 'title' => 'Search test action 2' ]);
        Action::factory()->create([ 'title' => 'Search test action 3' ]);

        $response = $this->call('POST', '/search', [
            'searchterm' => 'fake'
        ]);

        $response->assertSee('There are no live actions containing "fake"', false);
        $response->assertSee('There are no completed actions containing "fake"', false);
        $response->assertDontSee('Search test action 1');
    }

    /** @test */
    public function search_page_shows_correct_location_stub()
    {
        Client::factory()->create([
            'title' => 'Test client',
        ]);
        Project::factory()->create([
            'title' => 'Test project',
        ]);
        Action::factory()->create([
            'title' => 'Preview action test',
            'client_id' => 1,
            'project_id' => 1,
            'flagged' => 1,
        ]);

        $response = $this->call('POST', '/search', [
            'searchterm' => 'Preview'
        ]);

        $response->assertSeeInOrder([
            '[Test client / Test project]',
            'Preview action test',
        ]);
    }
}
