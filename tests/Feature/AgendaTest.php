<?php

namespace Tests\Feature;

use App\Client;
use App\Project;
use App\User;
use App\Action;
use Carbon\Carbon;
use Database\Seeds\AgendaTestSeeder;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AgendaTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp():void
    {
        parent::setUp();
        $this->be(User::factory()->create());
        $this->artisan('db:seed', [ '--class' => AgendaTestSeeder::class ]);
    }

    /** @test */
    public function agenda_shows_flagged_actions()
    {
        $response = $this->call("GET", "/myday");

        $response->assertSeeInOrder([
            'Tasks due today',
            'Flagged tasks',
            'First flagged action',
            'Second flagged action',
            'Tasks due in the future'
        ], false);
        $response->assertDontSee("Action not flagged");
    }

    /** @test */
    public function agenda_shows_only_flagged_actions_from_active_client()
    {
        session([ 'client_id' => 2 ]);
        $response = $this->call("GET", "/myday");

        $response->assertSeeInOrder([
            'Tasks due today',
            'Flagged tasks',
            'Second flagged action',
            'Tasks due in the future'
        ], false);
        $response->assertDontSee("Action not flagged");
        $response->assertDontSee('First flagged action');
    }

    /** @test */
    public function agenda_shows_actions_past_due()
    {
        // ASSEMBLE
        Action::factory()->create(
            [
                'title' => 'Action past due',
                'due' => Carbon::parse('yesterday')
            ]
        );

        // ACT
        $response = $this->call("GET", "/myday");

        // ASSERT
        $response->assertSee("Action past due", false);
        $response->assertSeeInOrder(
            [ 'Tasks due today', 'Action past due', 'Flagged tasks' ], false
        );
    }

    /** @test */
    public function agenda_shows_actions_nearly_due()
    {
        // ASSEMBLE
        Action::factory()->create(
            [ 'title' => 'Action due tomorrow', 'due' => Carbon::now()->addDay(), 'project_id' => 1 ]
        );
        Action::factory()->create(
            [ 'title' => 'Action due in a week', 'due' => Carbon::now()->addDays(6), 'project_id' => 1 ]
        );
        Action::factory()->create(
            [ 'title' => 'Action due in just over a week', 'due' => Carbon::now()->addDays(7), 'project_id' => 1]
        );

        // ACT
        $response = $this->call("GET", "/myday");

        // ASSERT
        $response->assertSeeInOrder(
            [ 'Tasks due today', 'Flagged tasks', 'Tasks due in the future', 'Action due tomorrow', 'Action due in a week' ], false
        );
        $response->assertDontSee('Action due in just over a week');
    }

    /** @test */
    public function agenda_shows_inbox()
    {
        // ASSEMBLE
        Action::factory()->create(
            [ 'title' => 'Inbox action 1' ]
        );
        Action::factory()->create(
            [ 'title' => 'Inbox action 2' ]
        );

        // ACT
        $response = $this->call("GET", "/myday");

        // ASSERT
        $response->assertSeeInOrder(
            [
                'Inbox',
                'Inbox action 1',
                'Inbox action 2',
                'Tasks due today',
                'Flagged tasks',
                'Tasks due in the future'
            ], false
        );
        $response->assertDontSee('Action due in just over a week');
    }

    /** @test */
    public function agenda_shows_correct_client_project_names()
    {
        // ASSEMBLE
        Client::factory()->create([
            'title' => 'Test client',
        ]);
        Project::factory()->create([
            'title' => 'Test project',
        ]);
        Action::factory()->create([
            'title' => 'Preview action test',
            'client_id' => 3,
            'project_id' => 1,
            'flagged' => 1,
        ]);
        Action::factory()->create([
            'title' => 'No preview action test',
            'flagged' => 1,
        ]);

        // ACT
        $response = $this->call("GET", "/myday");

        // ASSERT
        $response->assertSeeInOrder([
                'Test client / Test project',
                'Preview action test',
                'Inbox',
                'No preview action test',
            ],
            false
        );
    }
}
