FROM nginx:stable

## Remove the installed config file
RUN rm /etc/nginx/conf.d/default.conf

# Copy over our site config files
COPY .docker/config/nginx/nginx.conf /etc/nginx/nginx.conf
COPY .docker/config/nginx/conf.d/*.conf /etc/nginx/conf.d/

# Copy over the main include files for the site
COPY .docker/config/nginx/includes/*.conf etc/nginx/conf.d/includes/

# Uncomment lines below to copy any manually imported folders into our new containers
COPY public/ /srv/app/public
#RUN chown -R nginx /srv/app

EXPOSE 80