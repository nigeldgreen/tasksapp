<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotesTable extends Migration
{
    /**
     * Create the core columns for the Notes table
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('body');
            $table->string('overview')->nullable();
            $table->integer('client_id')->nullable()->unsigned()->index()->default(0);
            $table->integer('project_id')->nullable()->unsigned()->index()->default(0);
            $table->integer('action_id')->nullable()->unsigned()->index()->default(0);
            $table->integer('user_id')->nullable()->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notes');
    }
}
