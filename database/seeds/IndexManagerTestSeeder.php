<?php

namespace Database\Seeds;

use App\Action;
use App\Client;
use App\Project;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class IndexManagerTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->truncate();
        DB::table('projects')->truncate();

        // Clients
        Client::factory()->create([ 'title' => 'client 1' ]);
        Client::factory()->create([ 'title' => 'client 2' ]);

        // Projects
        $project1 = Project::factory()->create([ 'title' => 'project 1', 'client_id' => 1 ]);
        $project1->update([ 'client_order' => 5 ]);
        $project2 = Project::factory()->create([ 'title' => 'project 2', 'client_id' => 1 ]);
        $project2->update([ 'client_order' => 9 ]);

        // Inbox actions
        $action13 = Action::factory()->create([ 'title' => 'action 13' ]);
        $action14 = Action::factory()->create([ 'title' => 'action 14' ]);
        $action15 = Action::factory()->create([ 'title' => 'action 15' ]);
        $action13->update([ 'project_order' => 42 ]);
        $action14->update([ 'project_order' => 44 ]);
        $action15->update([ 'project_order' => 57 ]);


        // Actions with no project
        $action1 = Action::factory()->create([ 'title' => 'action 1', 'client_id' => 1 ]);
        $action2 = Action::factory()->create([ 'title' => 'action 2', 'client_id' => 1 ]);
        $action3 = Action::factory()->create([ 'title' => 'action 3', 'client_id' => 1 ]);
        $action1->update([ 'project_order' => 2 ]);
        $action2->update([ 'project_order' => 9 ]);
        $action3->update([ 'project_order' => 101 ]);

        $action9 = Action::factory()->create([ 'title' => 'action 9', 'client_id' => 2 ]);
        $action10 = Action::factory()->create([ 'title' => 'action 10', 'client_id' => 2 ]);
        $action11 = Action::factory()->create([ 'title' => 'action 11', 'client_id' => 2 ]);
        $action12 = Action::factory()->create([ 'title' => 'action 12', 'client_id' => 2 ]);
        $action9->update([ 'project_order' => 72 ]);
        $action10->update([ 'project_order' => 73 ]);
        $action11->update([ 'project_order' => 74 ]);
        $action12->update([ 'project_order' => 75 ]);

        // Actions with associated projects
        $action4 = Action::factory()->create([ 'title' => 'action 4', 'client_id' => 1, 'project_id' => 1 ]);
        $action5 = Action::factory()->create([ 'title' => 'action 5', 'client_id' => 1, 'project_id' => 1 ]);
        $action6 = Action::factory()->create([ 'title' => 'action 6', 'client_id' => 1, 'project_id' => 1 ]);
        $action4->update([ 'project_order' => 0 ]);
        $action5->update([ 'project_order' => 2 ]);
        $action6->update([ 'project_order' => 10 ]);


        $action7 = Action::factory()->create([ 'title' => 'action 4', 'client_id' => 1, 'project_id' => 2]);
        $action8 = Action::factory()->create([ 'title' => 'action 5', 'client_id' => 1, 'project_id' => 2 ]);
        $action7->update([ 'project_order' => 3 ]);
        $action8->update([ 'project_order' => 3 ]);
    }
}
