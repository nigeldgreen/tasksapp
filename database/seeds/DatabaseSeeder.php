<?php

namespace Database\Seeds;

use App\Action;
use App\Client;
use App\Note;
use App\Project;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Client::factory(3)->create([
            'user_id' => 1,
        ]);

        Project::factory(2)->create([
            'user_id' => 1,
            'client_id' => 1
        ]);
        Project::factory()->create([
            'user_id' => 1,
            'client_id' => 2
        ]);
        Project::factory()->create([
            'user_id' => 1,
            'client_id' => 3
        ]);

        Action::factory(5)->create([
            'user_id' => 1,
            'client_id' => 1,
            'project_id' => 1,
        ]);
        Action::factory(2)->create([
            'user_id' => 1,
            'client_id' => 1,
            'project_id' => 2,
        ]);
        Action::factory(5)->create([
            'user_id' => 1,
            'client_id' => 3,
            'project_id' => 0,
        ]);

        Note::factory(75)->create([
            'user_id' => 1,
        ]);
    }
}
