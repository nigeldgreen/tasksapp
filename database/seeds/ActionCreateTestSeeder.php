<?php

namespace Database\Seeds;

use App\Action;
use App\Client;
use App\Project;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActionCreateTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->truncate();
        DB::table('projects')->truncate();

        // Clients
        Client::factory()->create([ 'title' => 'client 1' ]);
        Client::factory()->create([ 'title' => 'client 2' ]);

        // Projects
        $project1 = Project::factory()->create([ 'title' => 'project 1', 'client_id' => 1 ]);
        $project2 = Project::factory()->create([ 'title' => 'project 2', 'client_id' => 1 ]);

        // Inbox actions
        Action::factory()->create([ 'title' => 'action 13' ]);
        Action::factory()->create([ 'title' => 'action 14' ]);
        Action::factory()->create([ 'title' => 'action 15' ]);


        // Actions with no project
        Action::factory()->create([ 'title' => 'action 1', 'client_id' => 1 ]);
        Action::factory()->create([ 'title' => 'action 2', 'client_id' => 1 ]);
        Action::factory()->create([ 'title' => 'action 3', 'client_id' => 1 ]);

        Action::factory()->create([ 'title' => 'action 9', 'client_id' => 2 ]);
        Action::factory()->create([ 'title' => 'action 10', 'client_id' => 2 ]);
        Action::factory()->create([ 'title' => 'action 11', 'client_id' => 2 ]);
        Action::factory()->create([ 'title' => 'action 12', 'client_id' => 2 ]);

        // Actions with associated projects
        Action::factory()->create([ 'title' => 'action 4', 'client_id' => 1, 'project_id' => 1 ]);
        Action::factory()->create([ 'title' => 'action 5', 'client_id' => 1, 'project_id' => 1 ]);
        Action::factory()->create([ 'title' => 'action 6', 'client_id' => 1, 'project_id' => 1 ]);

        Action::factory()->create([ 'title' => 'action 4', 'client_id' => 1, 'project_id' => 2 ]);
        Action::factory()->create([ 'title' => 'action 5', 'client_id' => 1, 'project_id' => 2 ]);

    }
}
