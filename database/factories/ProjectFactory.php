<?php

namespace Database\Factories;

use App\Action;
use App\Project;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProjectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Project::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->words(5, true),
            'notes' => '',
            'user_id' => 1,
            'client_id' => 0,
            'client_order' => 0
        ];
    }
}
