<?php

namespace Database\Factories;

use App\Action;
use Illuminate\Database\Eloquent\Factories\Factory;

class ActionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Action::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => 'Fake action',
            'notes' => '',
            'user_id' => 1,
            'client_id' => 0,
            'project_id' => 0,
            'flagged' => 0
        ];
    }
}

