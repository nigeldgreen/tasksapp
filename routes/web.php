<?php

// Sets up the correct routes for login/registration/logout etc
//Route::get('/register', [
//    'as' => 'register',
//    'uses' => 'Auth\RegisterController@showRegistrationForm',
//]);
//
//Route::post('/register', [
//    'as' => 'register.store',
//    'uses' => 'Auth\RegisterController@register',
//]);
//
//Route::get('/validate-user', [
//    'as' => 'user.validate',
//    'uses' => 'Auth\ValidateUserController@index',
//]);

Route::get('/login', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm',
]);
Route::post('/login', [
    'uses' => 'Auth\LoginController@login',
]);

Route::post('/password/email', [
    'as' => 'password.email',
    'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail',
]);
Route::get('/password/reset', [
    'as' => 'password.request',
    'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm',
]);
Route::post('/password/reset', [
    'as' => 'password.update',
    'uses' => 'Auth\ResetPasswordController@reset',
]);
Route::get('/password/reset/{token}', [
    'as' => 'password.reset',
    'uses' => 'Auth\ResetPasswordController@showResetForm',
]);


Route::get('/', [
    'as'=>'home',
    'uses'=>'View\IndexController@index'
]);
Route::get('/home', 'View\IndexController@index');
Route::get('/remotelogin', [
    'uses' => 'Helper\RemoteLoginController@create'
]);


Route::group(['middleware' => ['auth']], function () {
    Route::get('/logout', [
        'uses' => 'Auth\LoginController@logout'
    ]);
    Route::post('/logout', [
        'as' => 'logout',
        'uses' => 'Auth\LoginController@logout',
    ]);


    // User management routes
    Route::put('/user', [
        'as' => 'user.update',
        'uses' => 'UserController@update'
    ]);


    // Admin screen routes
    Route::get('/admin', [
        'as' => 'admin.index',
        'uses' => 'View\AdminController@index'
    ]);
    Route::post('/admin/saveclientorder', [
        'as' => 'admin.client.save',
        'uses' => 'View\AdminController@saveclientorder'
    ]);
    Route::post('/admin/agendapref', [
        'as' => 'admin.agenda.optin',
        'uses' => 'View\AdminController@updateagendaoptin'
    ]);


    // SuperAdmin routes
    Route::get('/superadmin', [
        'as' => 'admin.super',
        'uses' => 'View\SuperAdminController@index'
    ]);
    Route::get('/reindex', [
        'as' => 'admin.reindex',
        'uses' => 'Helper\ReindexController@index'
    ]);


    // Show actions by filtered view
    Route::get('/myday', [
        'as' => 'view.agenda',
        'uses' => 'View\AgendaController@index'
    ]);
    Route::get('/help', [
        'as' => 'help',
        'uses' => 'View\HelpController@index'
    ]);


    // Feedback
    Route::get('/feedback', [
        'as' => 'feedback',
        'uses' => 'View\FeedbackController@show'
    ]);
    Route::post('/feedback', [
        'as' => 'feedback.store',
        'uses' => 'View\FeedbackController@store'
    ]);


    // Search routes
    Route::get('/search', [
        'as' => 'search.form',
        'uses' => 'View\SearchController@index'
    ]);
    Route::post('/search', [
        'as' => 'search',
        'uses' => 'View\SearchController@index'
    ]);


    // Routes to manage front-end specific issues
    Route::get('/refreshcsrf', [
        'as' => 'refreshcsrf',
        'uses' => 'Admin\CSRFRefreshTokenController@index'
    ]);


    // Routes to deal with clients
    Route::get('/client', [
        'as' => 'client.index',
        'uses' => 'Client\ClientController@index'
    ]);
    Route::get('/client/create', [
        'as' => 'client.create',
        'uses' => 'Client\ClientController@create'
    ]);
    Route::get('/client/{client}/edit', [
        'as' => 'client.edit',
        'uses' => 'Client\ClientController@edit'
    ])->where('client', '[0-9]+');
    Route::post('/client', [
        'as' => 'client.store',
        'uses' => 'Client\ClientController@store'
    ]);
    Route::patch('/client/{client}', [
        'as' => 'client.update',
        'uses' => 'Client\ClientController@update'
    ])->where('client', '[0-9]+');
    Route::match(['get', 'delete'], '/client/delete/{client}', [
        'as' => 'client.destroy',
        'uses' => 'Client\ClientController@destroy'
    ])->where('client', '[0-9]+');
    Route::get('/client/switch/{client_id}', [
        'as' => 'client.switch',
        'uses' => 'Client\SwitchController@index'
    ]);
    // General ajax endpoints related to clients
    Route::get('/clientlist', [
        'uses' => 'Client\ListController@index'
    ]);

    // Route to get latest list of projects for a client
    Route::get('/client/getProjects/{client_id}', [
        'uses'=>'Client\ProjectListController@index'
    ]);





    // Routes to deal with projects
    Route::get('/project/{project}/show', [
        'uses'=>'Project\ProjectController@show'
    ]);
    Route::get('/project/create/{client_id?}', [
        'as' => 'project.create',
        'uses' => 'Project\ProjectController@create'
    ]);
    Route::get('/project/{project}/edit', [
        'as' => 'project.edit',
        'uses' => 'Project\ProjectController@edit'
    ]);
    Route::post('/project', [
        'as' => 'project.store',
        'uses' => 'Project\ProjectController@store'
    ]);
    Route::patch('/project/{project}', [
        'as' => 'project.update',
        'uses' => 'Project\ProjectController@update'
    ]);
    Route::match(['get', 'delete'], '/project/{project}/delete', [
        'as' => 'project.destroy',
        'uses' => 'Project\ProjectController@destroy'
    ]);





    // Routes to deal with actions
    Route::get('/action', [
        'as' => 'action.create',
        'uses' => 'Action\ActionController@create'
    ]);
    Route::get('/action/{action}', [
        'as' => 'action.show',
        'uses' => 'Action\ActionController@show'
    ])->where('action', '[0-9]+');
    Route::get('/action/create/{project?}', [
        'as' => 'action.create.project',
        'uses' => 'Action\ActionController@create'
    ]);
    Route::post('/action', [
        'as' => 'action.store',
        'uses' => 'Action\ActionController@store'
    ]);
    Route::get('/action/{action}/edit', [
        'as' => 'action.edit',
        'uses' => 'Action\ActionController@edit'
    ])->where('action', '[0-9]+');
    Route::patch('/action/{action}', [
        'as' => 'action.update',
        'uses' => 'Action\ActionController@update'
    ])->where('action', '[0-9]+');

    // Routes to deal with completed actions (treating as a separate resource to live actions)
    Route::get('/completedactions', [
        'as' => 'view.completedactions',
        'uses' => 'Action\CompletedController@index'
    ]);
    Route::delete('/restoreaction/{action_id}', [
        'as' => 'completedaction.delete',
        'uses' => 'Action\CompletedController@delete'
    ]);
    Route::match(['get', 'post'], '/completeaction/{action}', [
        'as' => 'completedaction.store',
        'uses'=>'Action\CompletedController@store'
    ]);
    // Routes to deal with dynamic action title field
    Route::get('/actiontitle/{action}/edit', [
        'uses' => 'Action\TitleController@edit'
    ]);
    Route::post('/actiontitle/{action}', [
        'uses' => 'Action\TitleController@update'
    ]);





    // Routes to deal with notes
    Route::match(['get', 'post'], '/notes', [
        'as' => 'note.index',
        'uses' => 'Note\NoteController@index'
    ]);
    Route::get('/note/{note}', [
        'as' => 'note.show',
        'uses'=>'Note\NoteController@show'
    ])->where('note', '[0-9]+');

    Route::get('/note/create', [
        'as' => 'note.create',
        'uses' => 'Note\NoteController@create'
    ]);
    Route::post('/note', [
        'as' => 'note.store',
        'uses' => 'Note\NoteController@store'
    ]);
    Route::get('/note/{note}/edit', [
        'as' => 'note.edit',
        'uses' => 'Note\NoteController@edit'
    ]);
    Route::patch('/note/{note}', [
        'as' => 'note.update',
        'uses' => 'Note\NoteController@update'
    ]);
    Route::delete('/note/{note}', [
        'as' => 'note.delete',
        'uses' => 'Note\NoteController@delete'
    ]);





    //Route::get('/migrate', ['uses' => 'MigrationController@index']);
//    Route::get('/email', [
//        'uses' => 'View\AdminController@sendAgendaEmail'
//    ]);
//
//    Route::get('/migrate', [
//        'uses' => 'Helper\MigrationController@index'
//    ]);

    //Route::get('/export-notes', [
    //    'uses' => 'ExportController@index'
    //]);
});